/**
 * ==========================================
 * Name:           Request
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    Request操作
 * Log
 * 2015-06-08    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";
	let API;

	API = {
		/**
		 * 解析url
		 *
		 * @public
		 * @param {string} url
		 * @returns {object}
		 */
		Parse: url => {
			const result = {
					  href:        "",
					  hashssearch: "",
					  hash:        "",
					  protocol:    "",
					  host:        "",
					  port:        80,
					  path:        "",
					  page:        "",
					  searchs:     [],
					  hashs:       []
				  },
				  rxp    = /^(?:(\w+):\/\/)?([^/\s\:]+)(?:\:(\d+))?([^\?\#\s]*\/)*([^\?\#\/\s]+)*([\?|\#]?\S*)*/i,
				  param  = (arr, val) => {
					  if (val.length < 1) return;

					  let rxp = /([\w-]+)(?:\=([^\s&]+)+)?&?/ig,
						  tmp, kv, status;

					  while (tmp = rxp.exec(val)) {
						  if (tmp.length != 3 && tmp[1] === void(0)) continue;

						  kv = {key: decodeURI(tmp[1]), value: tmp[2] === void(0) ? "" : tmp[2].trim("&")};
						  status = false;

						  jBD.each(arr, d => {
							  if (d.key != kv.key) return;
							  d.value += (d.value.length > 0 ? "," : "") + kv.value;
							  status = true;
						  });

						  if (!status) arr.push(kv);
					  }
				  };

			jBD.each(rxp.exec(url), (d, i, c) => {
				if (d === void(0)) return;

				switch (i) {
					case 1:
						result.protocol = d;
						break;
					case 2:
						result.host = d;
						break;
					case 3:
						result.port = Number(d);
						break;
					case 4:
						result.path = d.replace(/\/\//g, "/").replace(/^(\/)|(\/)$/g, "");
						break;
					case 5:
						result.page = d;
						break;
					case 6:
						if (d[0] == "?") {
							c = d.indexOf("#");

							param(result.searchs, d.substr(1, c > 1 ? c - 1 : d.length));
							if (c > 0) d = d.substr(c);
						}

						if (d[0] == "#") param(result.hashs, d.substr(1));
						break;
				}
			}, 1);

			API.Build(result);

			return result;
		},
		/**
		 * 构建url
		 *
		 * @public
		 * @param {object} [data=null]
		 * @returns {string}
		 */
		Build: data => {
			if (!jBD.isObject(data)) return "";

			data.search = "";
			data.hash = "";
			data.href = "";

			if (data.protocol.length > 0) data.href += data.protocol + "://";
			if (data.host.length > 0) data.href += data.host + ((data.port != 80 && data.port > 0) ? ":" + data.port + "/" : "/");
			if (data.path.length > 0) data.href += data.path + "/";
			if (data.page.length > 0) data.href += data.page;
			if (data.searchs.length > 0) {
				jBD.each(data.searchs, d => {
					data.search += encodeURI(d.key);
					if (d.value.length > 0) {
						data.search += "=";
						data.search += encodeURI(d.value);
					}
					data.search += "&";
				});
				data.search = data.search.replace(/^(&)|(&)$/g, "");

				data.href += "?" + data.search;
			}
			if (data.hashs.length > 0) {
				jBD.each(data.hashs, d => {
					data.hash += encodeURI(d.key);
					if (d.value.length > 0) {
						data.hash += "=";
						data.hash += encodeURI(d.value);
					}
					data.hash += "&";
				});
				data.hash = data.hash.replace(/^(&)|(&)$/g, "");

				data.href += "#" + data.hash;
			}

			return data.href;
		},
		/**
		 * 查询参数
		 *
		 * @public
		 * @param {string} [url=null]
		 * @param {number|string} [sh=1] 查询search/hash模式
		 * @returns {Array}
		 */
		Query: (url, sh) => {
			if (sh === void(0)) sh = url;
			url = API.Parse(url || window.location.href);

			switch (String(sh)) {
				default:
				case "search":
				case "1":
					return url.searchs;
				case "hash":
				case "2":
					return url.hashs;
			}
		},
		/**
		 * 获取参数
		 *
		 * @public
		 * @param {string} key
		 * @param {string|object} [data=null] url对象
		 * @param {number|string} [sh=1] 查询search/hash模式
		 * @returns {string}
		 */
		Get:   (key, data, sh) => {
			if (sh === void(0)) sh = data;
			if (!jBD.isString(key)) return "";

			if (jBD.isString(data)) data = API.Parse(data);
			else if (!jBD.isObject(data)) data = API.Parse(window.location.href);

			let result = [];

			switch (String(sh)) {
				default:
				case "search":
				case "1":
					sh = data.searchs || [];
					break;
				case "hash":
				case "2":
					sh = data.hashs || [];
					break;
			}

			jBD.each(sh, d => {
				if (d.key === key) result.push(d.value);
			});

			return result.join();
		},
		/**
		 * 设置参数
		 *
		 * @public
		 * @param {string} key
		 * @param {string} value
		 * @param {string} [data=null] url对象
		 * @param {object} [opt]
		 *    @param {number|string} [opt.sh=1] 查询search/hash模式
		 *    @param {boolean} [opt.operate=true] 是否改变url
		 *    @param {boolean} [opt.history=false] 是否改变历史记录，opr=true时，为true
		 *    @param {string} [opt.title=null] 历史记录标题
		 * @returns {string}
		 */
		Set:   (key, value, data, opt) => {
			if (opt === void(0)) opt = data;
			if (!jBD.isString(key)) return "";

			let state = false,
				sh, operate, history, title;

			switch (typeof (opt)) {
				case "object":
					if (opt) {
						sh = opt.sh;
						operate = opt.operate;
						history = opt.history;
						title = opt.title;
					}
					break;
				case "string":
				case "number":
					sh = String(opt);
					break;
				case "boolean":
					operate = opt;
					break;
			}
			sh = jBD.isString(sh) ? sh : "1";
			operate = operate !== false;
			title = jBD.isString(title) ? title : null;
			history = history === true;
			if (operate) history = false;

			if (jBD.isString(data)) data = API.Parse(data);
			else if (!jBD.isObject(data)) data = API.Parse(window.location.href);

			switch (sh) {
				default:
				case "search":
				case "1":
					sh = data.searchs || [];
					break;
				case "hash":
				case "2":
					sh = data.hashs || [];
					break;
			}

			value = String(value);

			jBD.each(sh, d => {
				if (d.key === key) {
					d.value = value;
					state = true;
				}
			});

			if (!state) sh.push({key: key, value: value});

			API.Build(data);

			if (operate) window.location.href = data.href;
			if (history) window.history.replaceState(null, title, data.href);

			return data.href;
		},
		/**
		 * 删除参数
		 *
		 * @public
		 * @param {string} key
		 * @param {string} [data=null] url对象
		 * @param {object} [opt]
		 *    @param {number|string} [opt.sh=1] 查询search/hash模式
		 *    @param {boolean} [opt.operate=true] 是否改变url
		 *    @param {boolean} [opt.history=false] 是否改变历史记录
		 *    @param {string} [opt.title=null] 历史记录标题
		 * @returns {string}
		 */
		Del:   (key, data, opt) => {
			if (opt === void(0)) opt = data;
			if (!jBD.isString(key)) return "";

			let sh, operate, history, title;

			switch (typeof (opt)) {
				case "object":
					if (opt) {
						sh = opt.sh;
						operate = opt.operate;
						history = opt.history;
						title = opt.title;
					}
					break;
				case "string":
				case "number":
					sh = String(opt);
					break;
				case "boolean":
					operate = opt;
					break;
			}
			sh = jBD.isString(sh) ? sh : "1";
			operate = operate !== false;
			title = jBD.isString(title) ? title : null;
			history = history === true;
			if (operate) history = false;

			if (jBD.isString(data)) data = API.Parse(data);
			else if (!jBD.isObject(data)) data = API.Parse(window.location.href);

			switch (sh) {
				default:
				case "search":
				case "1":
					sh = data.searchs || [];
					break;
				case "hash":
				case "2":
					sh = data.hashs || [];
					break;
			}

			for (let i = 0; i < sh.length;) {
				if (sh[i].key == key) sh.splice(i, 1);
				else i++;
			}

			API.Build(data);

			if (operate) window.location.href = data.href;
			if (history) window.history.replaceState(null, title, data.href);

			return data.href;
		},
		/**
		 * 清除全部
		 *
		 * @public
		 * @param {string} [data=null] url对象
		 * @param {object} [opt]
		 *    @param {boolean} [opt.operate=true] 是否改变url
		 *    @param {boolean} [opt.history=false] 是否改变历史记录，opr=true时，为true
		 *    @param {string} [opt.title=null] 历史记录标题
		 * @returns {string}
		 */
		Clear: (data, opt) => {
			if (opt === void(0)) opt = data;

			let operate, history, title;

			switch (typeof (opt)) {
				case "object":
					if (opt) {
						operate = opt.operate;
						history = opt.history;
						title = opt.title;
					}
					break;
				case "string":
					title = opt;
					break;
				case "boolean":
					operate = opt;
					break;
			}
			operate = operate !== false;
			title = jBD.isString(title) ? title : null;
			history = history === true;
			if (operate) history = false;

			if (jBD.isString(data)) data = API.Parse(data);
			else if (!jBD.isObject(data)) data = API.Parse(window.location.href);

			data.searchs.splice(0, data.searchs.length);
			data.hashs.splice(0, data.hashs.length);

			API.Build(data);

			if (operate) window.location.href = data.href;
			if (history) window.history.replaceState(null, title, data.href);

			return data.href;
		},
		/**
		 * 附加随机内容
		 *
		 * @public
		 * @param {string} [data=null] url对象
		 * @param {object} [opt]
		 *    @param {boolean} [opt.operate=true] 是否改变url
		 *    @param {boolean} [opt.history=false] 是否改变历史记录，opr=true时，为true
		 *    @param {string} [opt.title=null] 历史记录标题
		 * @returns {string}
		 */
		Rmd:   (data, opt) => {
			if (opt === void(0)) opt = data;

			let operate, history, title;

			switch (typeof (opt)) {
				case "object":
					if (opt) {
						operate = opt.operate;
						history = opt.history;
						title = opt.title;
					}
					break;
				case "string":
					title = opt;
					break;
				case "boolean":
					operate = opt;
					break;
			}
			operate = operate !== false;
			title = jBD.isString(title) ? title : null;
			history = history === true;
			if (operate) history = false;

			return API.Set(
				"rmd", parseInt(Math.random() * 100),
				data,
				{
					operate: operate,
					history: history,
					title:   title
				}
			);
		}
	};

	return API;
}, {module: module, exports: this}, [], "Request");