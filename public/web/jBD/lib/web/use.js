/**
 * ==========================================
 * Name:           USE
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    Web CMD默认加载模块 便于加载所有模块
 * Log
 * 2015-06-08    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
}, {module:  module,
	exports: this
}, ["RegExp", "Security", "Conver", "String", "Date", "Check", "DOM", "Request", "Cookie", "Storage", "History"], "USE");