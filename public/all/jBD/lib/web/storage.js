/**
 * ==========================================
 * Name:           Storage
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    本地数据操作
 * Log
 * 2015-06-08    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";
	let API;

	API = {
		/**
		 * 获取Storage值数组
		 *
		 * @public
		 * @returns {Array}
		 */
		Query: () => {
			let data   = window.localStorage,
				result = [];

			if (data) {
				for (let i = 0, k; i < data.length; i++) {
					k = data.key(i);

					result.push({key: k, value: data.getItem(k)});
				}
			}

			return result;
		},
		/**
		 * 获取值
		 *
		 * @public
		 * @param {string} name
		 * @returns {string}
		 */
		Get:   name => {
			return window.localStorage && jBD.isString(name) ? window.localStorage.getItem(name) : "";
		},
		/**
		 * 设置值
		 *
		 * @public
		 * @param {string} name
		 * @param {string} value
		 * @returns {string}
		 */
		Set:   (name, value) => {
			let data = window.localStorage;

			if(!data) return "";

			switch (jBD.type(name, true)){
				case "string":
				case "number":
					if (value = String(value)) data.setItem(name, value);
					else data.removeItem(name);
					break;
				case "object":
					for(let k in name) API.Set(k, name[k]);
					break;
			}

			return value;
		},
		/**
		 * 删除值
		 *
		 * @public
		 * @param {string} name
		 */
		Del:   name => {
			if (window.localStorage && jBD.isString(name)) window.localStorage.removeItem(name);
		},
		/**
		 * 清除全部
		 *
		 * @public
		 */
		Clear: () => {
			if (window.localStorage) window.localStorage.clear();
		}
	};

	return API;
}, {module: module, exports: this}, [], "Storage");