/**
 * ==========================================
 * Name:           History
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    History操作
 * Log
 * 2015-06-08    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";
	let API;

	API = {
		/**
		 * 历史页面个数
		 *
		 * @public
		 * @returns {number}
		 */
		Length:  () => {
			return window.history.length;
		},
		/**
		 * 回退
		 *
		 * @public
		 */
		Back:    () => {
			window.history.back();
		},
		/**
		 * 前进
		 *
		 * @public
		 */
		Forward: () => {
			window.history.forward();
		},
		/**
		 * 跳转到制定历史
		 *
		 * @public
		 * @param {number} [num=-1]
		 */
		Go:      num => {
			window.history.go(jBD.isNumber(num) ? num : -1);
		},
		/**
		 * 增加历史点
		 *
		 * @public
		 * @param {string} url
		 * @param {string} [data=null]
		 * @param {string} [title=""]
		 */
		Add:     (url, data, title) => {
			title = jBD.isString(title, true) ? title : "";
			data = jBD.isNull(data, true) ? null : data;
			url = jBD.isString(url, true) ? url : jBD.Request.Query().href;

			window.history.pushState(data, title, url);
		},
		/**
		 * 修改当前点
		 *
		 * @public
		 * @param {string} url
		 * @param {string} [data=null]
		 * @param {string} [title=""]
		 * @param {boolean} [opr=true] 是否更改当前页面
		 */
		Set:     (url, data, title, opr) => {
			title = jBD.isString(title, true) ? title : "";
			data = jBD.isNull(data, true) ? null : data;
			url = jBD.isString(url, true) ? url : jBD.Request.Query().href;
			opr = jBD.isBool(opr) ? opr : true;

			window.history.replaceState(data, title, url);

			if (opr) window.location.href = url;
		}
	};

	return API;
}, {module: module, exports: this}, ["Request"], "History");