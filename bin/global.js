"use strict";

module.exports = exports = function () {
	const fs   = require("fs"),
		  path = require("path");

	const minify = (src, dest, type, callback)=> {
			  let e = path.extname(src),
				  o = {
					  sync:     true,
					  fileIn:   src,
					  fileOut:  dest,
					  callback: err => {
						  if (err) console.log(err);
						  callback(err);
					  }
				  };

			  e = e.toLowerCase();
			  if (e == ".css") o.type = "yui-css";
			  else {
				  o.type = "gcc";
				  o.language = type || "ECMASCRIPT5";
			  }

			  new require("node-minify").minify(o);
		  },
		  uglify = (src, dest, type, callback) => {
			  let e = path.extname(src),
				  o = {
					  outSourceMap: path.basename(src) + ".map"
				  };

			  require("uglify-js").minify(src, o);
		  };

	const deep = (src, dest)=> {
			  let result = dest || {};

			  each(src, (d, k)=> {
				  if (typeof(d) == "object") {
					  if (d === null) result[k] = null;
					  else if (d === void(0)) result[k] = void(0);
					  else if (d instanceof Array) result[k] = result[k] === void(0) ? d.concat() : result[k].concat(d);
					  else result[k] = deep(d);
				  }
				  else result[k] = d;
			  });

			  return result;
		  },
		  each = (data, callback)=> {
			  if (data instanceof Array) {
				  for (let i = 0, d; i < data.length && (d = data[i]); i++) callback(d, i);
			  }
			  else {
				  for (let k in data) callback(data[k], k);
			  }
		  };

	const saveFile  = (fn, data)=> writeFile(fn, JSON.stringify(data, null, 4)),
		  miniFile  = (src, dest, type)=> {
			  if (!fs.existsSync(src)) return;

			  makeDir(dest);

			  copyFile(src, dest);

			  // minify(src, dest, type || "", err=> {
			  //   if (err) {
			  // 	  throw err;
			  //   }
			  // });
		  },
		  copyFile  = (src, dest) => {
			  if (!fs.existsSync(src)) return false;

			  makeDir(dest);

			  switch (path.extname(src).toLowerCase()) {
				  case ".js":
				  case ".css":
				  case ".json":
					  fs.writeFileSync(dest, fs.readFileSync(src, "utf-8"));
					  break;
				  default:
					  fs.createReadStream(src).pipe(fs.createWriteStream(dest));
					  break;
			  }

			  return true;
		  },
		  delFile   = src=> {
			  if (fs.existsSync(src)) fs.unlinkSync(src);
		  },
		  readFile  = (src, code) => fs.readFileSync(src, code || "utf-8"),
		  writeFile = (fn, data) =>fs.writeFileSync(fn, data),
		  makeDir   = (dest, callback)=> {
			  dest = dest.replace(/\\/g, "/");

			  if (!fs.existsSync(dest)) {
				  for (let i = 0, o = "", l = dest.split("/"), d; i < l.length - 1; i++) {
					  if (d = l[i]) {
						  if (i == 0 && d[d.length - 1] == ":") o = d;
						  else if (!fs.existsSync(o = path.join(o, d))) fs.mkdirSync(o);
					  }
					  else if (i == 0) o = "/";
				  }
			  }

			  if (callback) callback(dest);

			  return dest;
		  },
		  delDir    = (dest, self)=> {
			  self = self !== false;
			  dest = dest.replace(/\\/g, "/");

			  if (!fs.existsSync(dest)) return;

			  fs.readdirSync(dest).forEach((f, o)=> {
				  if (fs.statSync(o = path.join(dest, f)).isDirectory()) delDir(o);
				  else fs.unlinkSync(o);
			  });

			  if (self) fs.rmdirSync(dest);
		  },
		  copyDir   = (src, dest)=> {
			  if (!fs.existsSync(src)) return;

			  fs.readdirSync(src).forEach((f, o)=> {
				  if (fs.statSync(o = path.join(src, f)).isDirectory()) copyDir(o, path.join(dest, f));
				  else copyFile(o, path.join(dest, f));
			  });
		  },
		  listDir   = (code, p, list)=> {
			  let _list = list || {},
				  _src  = path.join(code, p);

			  if (!fs.existsSync(_src)) return _list;

			  fs.readdirSync(_src).forEach((f, o)=> {
				  if (fs.statSync(o = path.join(_src, f)).isDirectory()) listDir(p + "/" + f, _list);
				  else _list[p + "/" + f] = path.extname(o).toLowerCase() == ".css" || path.extname(o).toLowerCase() == ".js";
			  });

			  return _list;
		  };

	return {
		save:      saveFile,
		each:      each,
		deep:      deep,
		miniFile:  miniFile,
		copyFile:  copyFile,
		delFile:   delFile,
		readFile:  readFile,
		writeFile: writeFile,
		makeDir:   makeDir,
		delDir:    delDir,
		copyDir:   copyDir,
		listDir:   listDir
	};
};