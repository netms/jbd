"use strict";

module.exports = exports = function (global, info, code, out) {
	const path  = require("path"),
		  cfg   = require(path.join(code, "config.json")),
		  cache = path.join(out, "cache");

	const init_jBD   = (core, loading, fn) => {
			  fn = [path.join(code, "all_jBD.js"), path.join(code, "web_jBD.js"), path.join(code, "node_jBD.js")];
			  let c = ["", "", ""],
				  f = ["", "", ""];

			  for (let i = 0; i < core.length; i++) {
				  if (core[i][0] == "%") {
					  core[i] = core[i].substr(1);
					  c[0] += global.readFile(path.join(code, "web_" + core[i])) + "\n";
					  c[0] += global.readFile(path.join(code, "node_" + core[i])) + "\n";
					  c[1] += global.readFile(path.join(code, "web_" + core[i])) + "\n";
					  c[2] += global.readFile(path.join(code, "node_" + core[i])) + "\n";

				  }
				  else {
					  c[0] += global.readFile(path.join(code, core[i])) + "\n";
					  c[1] += global.readFile(path.join(code, core[i])) + "\n";
					  c[2] += global.readFile(path.join(code, core[i])) + "\n";
				  }
			  }

			  for (let i = 0; i < loading.length; i++) {
				  if (loading[i][0] == "%") {
					  loading[i] = loading[i].substr(1);
					  f[0] += global.readFile(path.join(code, "web_" + loading[i])) + "\n";
					  f[0] += global.readFile(path.join(code, "node_" + loading[i])) + "\n";
					  f[1] += global.readFile(path.join(code, "web_" + loading[i])) + "\n";
					  f[2] += global.readFile(path.join(code, "node_" + loading[i])) + "\n";

				  }
				  else {
					  f[0] += global.readFile(path.join(code, loading[i])) + "\n";
					  f[1] += global.readFile(path.join(code, loading[i])) + "\n";
					  f[2] += global.readFile(path.join(code, loading[i])) + "\n";
				  }
			  }

			  let fw = global.readFile(path.join(code, "jBD.js"));

			  c[0] = fw.replace("\"@jBD_Code\";", c[0]);
			  c[0] = c[0].replace("@version", cfg.info.version);

			  c[1] = fw.replace("\"@jBD_Code\";", c[1]);
			  c[1] = c[1].replace("@version", cfg.info.version);

			  c[2] = fw.replace("\"@jBD_Code\";", c[2]);
			  c[2] = c[2].replace("@version", cfg.info.version);

			  c[0] = c[0].replace("\"@jBD_Loading\";", f[0]);

			  c[1] = c[1].replace("\"@jBD_Loading\";", f[1]);

			  c[2] = c[2].replace("\"@jBD_Loading\";", f[2]);

			  global.writeFile(fn[0], c[0]);
			  global.writeFile(fn[1], c[1]);
			  global.writeFile(fn[2], c[2]);
		  },
		  init_Test  = (core, fn) => {
			  fn = path.join(code, "test_jBD.js");

			  let c = "",
				  f = global.readFile(path.join(code, "jBD.js"));

			  for (let i = 0; i < core.length; i++) {
				  if (core[i][0] == "%") {
					  core[i] = core[i].substr(1);
					  c += global.readFile(path.join(code, "node_" + core[i])) + "\n";
				  }
				  else {
					  c += global.readFile(path.join(code, core[i])) + "\n";
				  }
			  }

			  c = f.replace("\"@jBD_Code\";", c);
			  c = c.replace("@version", cfg.info.version);

			  global.writeFile(fn, c);
		  },
		  init_Cache = (code, out, cache) => {
			  let list = {web: {}, node: {}, plug: {}};

			  global.delDir(out, false);
			  //global.delFile(path.join(code, "jBD.js"));

			  console.log("─┬─ cache");

			  console.log(" ├─ jBD.js");
			  init_jBD(cfg.core, cfg.loading);
			  global.miniFile(path.join(code, "all_jBD.js"), path.join(cache, "jBD", "all_jBD.js"));
			  global.miniFile(path.join(code, "web_jBD.js"), path.join(cache, "jBD", "web_jBD.js"));
			  global.miniFile(path.join(code, "node_jBD.js"), path.join(cache, "jBD", "node_jBD.js"));
			  init_Test(cfg.test);
			  global.miniFile(path.join(code, "test_jBD.js"), path.join(cache, "jBD", "test_jBD.js"));

			  console.log(" ├┬ base");
			  global.each(cfg.base, (d, o) => {
				  o = "lib/" + d;
				  list.web[o] = list.node[o] = "./jBD/" + o;
				  global.miniFile(path.join(code, o), path.join(cache, "jBD", o));
				  console.log(" │├─ " + o);
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ web");
			  global.each(cfg.web.files, (d, o) => {
				  o = "lib/web/" + d;
				  list.web[o] = "./jBD/" + o;
				  global.miniFile(path.join(code, o), path.join(cache, "jBD", o));
				  console.log(" │├─ " + o);
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ css");
			  global.each(cfg.css.files, (d, o) => {
				  o = path.join(code, d);
				  d = path.basename(o);
				  list.web["src/" + d] = "./src/" + d;
				  global.miniFile(o, path.join(cache, "src", d));
				  console.log(" │├─ " + o);
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ plugsin");
			  global.each(cfg.plug.files, (d, o, l) => {
				  console.log(" │├┬ " + d);
				  global.each(l = global.listDir(code, "plugsin/" + d, list.plug), (d, k) => {
					  if (d) {
						  global.miniFile(path.join(code, k), l[k] = path.join(cache, "src/plugsin", k));
						  console.log(" ││├─ " + k);
					  }
					  else l[k] = path.join(code, k);
				  });
				  console.log(" ││└─ done");
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ src");
			  global.each(cfg.src.files, (d, o) => {
				  o = path.join(code, d);
				  d = path.basename(o);
				  list.web["src/" + d] = "./src/" + d;
				  global.miniFile(o, path.join(cache, "src", d));
				  console.log(" │├─ " + o);
			  });
			  console.log(" │└─ done\n │");

			  console.log(" ├┬ node");
			  global.each(cfg.node.files, (d, o) => {
				  o = "lib/node/" + d;
				  list.node[o] = "./jBD/" + o;
				  // global.miniFile(path.join(code, o), path.join(cache, "jBD", o), "ECMASCRIPT6");
				  global.copyFile(path.join(code, o), path.join(cache, "jBD", o));
				  console.log(" │├─ " + o);
			  });
			  console.log(" │└─ done\n │");

			  console.log(" └─ done\n");

			  return list;
		  };

	cfg.cache = init_Cache(code, out, cache);

	console.log("─┬─ test");
	console.log(" ├─ ./jBD.js");
	global.miniFile(path.join(cache, "./jBD/test_jBD.js"), path.join(out, "test", "jBD.js"));
	console.log(" └─ done\n │");

	console.log("─┬─ web");
	global.each(cfg.cache.web, (d, k) => {
		global.copyFile(path.join(cache, d), path.join(out, "web", d));
		console.log(" ├─ " + d);
	});
	global.each(cfg.cache.plug, (d, k) => {
		global.copyFile(path.join(cache, d), path.join(out, "web/src", k));
		console.log(" ├─ " + d);
	});
	global.copyFile(path.join(code, "web_jBD.js"), path.join(out, "web/jBD", "jBD.js"));
	console.log(" └─ done\n");

	console.log("─┬─ node");
	cfg.pack = global.deep(cfg.info);
	cfg.pack = global.deep(info, cfg.pack);
	cfg.pack.main = "jBD.js";
	cfg.pack.files = [];
	global.each(cfg.cache.node, (d, k) => {
		cfg.pack.files.push(k);
		global.copyFile(path.join(cache, d), path.join(out, "node", d));
		console.log(" ├─ " + d);
	});
	global.save(path.join(out, "node/jBD/package.json"), cfg.pack);
	global.copyFile(path.join(code, "node_jBD.js"), path.join(out, "node/jBD", "jBD.js"));
	console.log(" └─ done\n");

	console.log("─┬─ all");
	global.copyDir(path.join(out, "web"), path.join(out, "all"));
	global.copyDir(path.join(out, "node"), path.join(out, "all"));
	global.copyFile(path.join(code, "all_jBD.js"), path.join(out, "all/jBD", "jBD.js"));
	console.log(" └─ done\n");

	global.delDir(cache, true);
	global.delFile(path.join(code, "all_jBD.js"));
	global.delFile(path.join(code, "web_jBD.js"));
	global.delFile(path.join(code, "node_jBD.js"));
	global.delFile(path.join(code, "test_jBD.js"));
};