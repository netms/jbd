![jBD](jBD-logo.png)
## jBD是什么?
针对ES6的JS工具库，部分功能兼容ES5


## jBD有哪些功能？

* 工具库
	* `核心` —— 常用的is类型判断
	* `check` —— 文本内容校验
	* `conver` —— 数据转换
	* `date` —— 时间计算
	* `security` —— 安全性算法（MD5、CRC、SHA）
	* `string` —— 字符串工具

* Web端
	* `CMD模块加载`，参考了seajs代码，并进行了模块参数的优化
	* `cookie`、`history`、`storage`、`request`对象简化操作
	* `dom操作`，参考了jQuery代码，并进行了一定的优化和事件扩展
	* 针对Mobile端的`手势识别`，进行了一定的扩展

* Node端
	* `fs` —— 针对json、log、file文本类文件进行读写操作封装
	* `db` —— mongodb、mysql、redis数据库操作封装
	* `log` —— log文件生成、记录、检索操作封装
	* `net` —— 网络操作封装
	* `zip` —— zip、tar压缩文件封装


## 问题反馈
在使用中有任何问题，欢迎反馈给我


## 感激
感谢以下的项目,排名不分先后。

* [jQuery](http://jquery.com)
* [seajs](https://github.com/seajs/seajs)