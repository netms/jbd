/**
 * ==========================================
 * Name:           jBD‘s Loading
 * Author:         Buddy-Deus
 * CreTime:        2016-05-10
 * Description:    jBD Loading Core
 * Log
 * 2016-05-10    初始化加载列表
 * 2016-11-24    基于ES6改写
 * ==========================================
 */
(jBD => {
	jBD.__require("./lib/regexp", "RegExp");
	jBD.__require("./lib/security", "Security");
	jBD.__require("./lib/conver", "Conver");
	jBD.__require("./lib/string", "String");
	jBD.__require("./lib/date", "Date");
	jBD.__require("./lib/check", "Check");
	// jBD.__require("./lib/task", "Task");
})(this);