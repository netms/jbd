/**
 * ==========================================
 * Name:           jBD‘s Asyn for ES6
 * Author:         Buddy-Deus
 * CreTime:        2016-04-06
 * Description:    BD‘s Callback/Deferred for ES6
 * Log:
 * 2016-04-06    重写代码
 * 2017-02-10    去除函数默认参数,增加适配
 * 2017-02-22    增加ES6 Promise支持
 * ==========================================
 */
(own => {
	let TCallback, TDeferred, TPromise;

	/*
	 ====================================
	 = 类名: TCallback
	 = 功  能: 回调类
	 = 对象函数：
	 =   lock        = 锁定状态
	 =   unlock      = 去除锁定
	 =   add         = 添加函数
	 =   del         = 删除指定函数
	 =   fire        = 触发调用
	 =   fireWith    = 触发调用,改变this指向
	 =   empty       = 清空回调列表
	 =   unique      = 去除重复函数
	 =   has         = 检测是否存在指定函数
	 ====================================
	 */
	TCallback = (TObject => {
		const getFuncs  = (list, callback) => {
				  jBD.each(list, (d, k, t) => {
					  switch (t) {
						  case "function":
							  callback(d);
							  break;
						  case "array":
							  if (d.length > 0) getFuncs(d, callback);
							  break;
					  }
				  }, true);
			  },
			  eachFuncs = (list, callback) => {
				  let i = 0;

				  while (i < list.length) {
					  if (callback(i, list[i]) !== false) i++;
				  }
			  },
			  fireFuncs = (list, data, once, done, prog) => {
				  if (list.length < 1) return done;

				  eachFuncs(list, (i, f) => {
					  f.apply(data[0], data[1]);

					  if (once) list.splice(i, 1);
					  if (prog) prog();
					  if (once) return false;
				  });

				  if (done) done();

				  return null
			  };

		return TObject.extend({
			className: "TCallback",
			create:    function (opt, limit, callback) {
				this.locked = false;
				this.length = 0;

				this.__func = [];
				this.__arg = null;
				this.__opt = opt;
				this.__limit = limit;
				this.__callback = callback || null;
			},
			free:      function () {
				this.__opt = null;
				this.__callback = null;
				this.__func = null;
				this.__arg = null;
			},
			lock:      function () { return this.locked = true; },
			unlock:    function () { return this.locked = false; },
			on:        function () {
				if (!this.locked) {
					let opt  = this.__opt,
						func = this.__func,
						arg  = this.__arg;

					getFuncs(arguments, d => {
						func.push(d);
						if (func.length > this.limit) func.pop();
					});
					this.length = func.length;

					if (opt.unique) this.unique();
					if (opt.memory && opt.fire && arg) fireFuncs(func, arg, opt.once, this.__callback, d => this.length = func.length);
				}

				return this;
			},
			off:       function () {
				if (!this.locked) {
					let func = this.__func;

					if (arguments.length == 0) func = this.__func = [];
					else {
						getFuncs(arguments, d => {
							eachFuncs(func, (i, f) => {
								if (Object.is(d, f)) {
									func.splice(i, 1);
									return false;
								}
							})
						});
					}

					this.length = func.length;
				}

				return this;
			},
			fire:      function () { return this.fireWith(this, arguments); },
			fireWith:  function (own, args, done) {
				if (arguments.length < 2) args = [];
				if (arguments.length < 1) own = this;

				let opt = this.__opt,
					arg;

				this.__arg = arg = [own, args.slice ? args.slice() : args];
				if (opt.memory && !opt.fire) {
					opt.fire = true;
					if (opt.limit) this.__arg = null;
				}
				else {
					let func = this.__func;

					fireFuncs(func, arg, opt.once, done, d => this.length = func.length);
					if (this.__callback) this.__callback.apply(arg[0], arg[1]);
				}

				return this;
			},
			unique:    function () {
				if (!this.locked) this.length = (this.__func = Array.from(new Set(this.__func))).length;

				return this;
			},
			has:       function (func) { return this.__func.indexOf(func) != -1; }
		});
	})(jBD.TSeal);
	/*
	 ====================================
	 = 类名: TDeferred
	 = 功  能: 异步类(观察者模式)
	 = 对象函数：
	 =   empty       = 清空回调列表
	 =   promise     = 去除重复函数
	 =   resolve     = 触发成功
	 =   reject      = 触发失败
	 =   alarm       = 触发警告
	 =   notify      = 触发进度
	 =   on          = 绑定事件
	 =   off         = 解除绑定
	 = 对象事件:
	 =   done        = 成功
	 =   fail        = 失败
	 =   warn        = 警告
	 =   progress    = 进度
	 =   always      = 结束
	 ====================================
	 */
	TDeferred = (TObject => {
		let STATE = {
				inited:   0,
				pending:  1,
				resolved: "resolve",
				rejected: "reject"
			},
			Deferred;

		Deferred = TObject.extend({
			className: "TDeferred",
			/**
			 * 初始化函数
			 *
			 * @param {number} [limit=50] 回调上限
			 * @param {function} [callback] 最终回调,在触发always最后调用
			 */
			create:    function (limit, callback) {
				let _this = this,
					event = _this.__event = {
						done:     jBD.Callback(limit).fire(),
						fail:     jBD.Callback(limit).fire(),
						warn:     jBD.Callback(limit).fire(),
						progress: jBD.Callback(limit).fire(),
						always:   jBD.Callback(limit, callback).fire()
					},
					action = _this.__action = {
						resolve: event.done,
						reject:  event.fail,
						alarm:   event.warn,
						notify:  event.progress
					},
					promise = _this.__promise = {
						state:   STATE.inited,
						on:      function () {
							_this.on.apply(_this, arguments);
							return this;
						},
						off:     function () {
							_this.off.apply(_this, arguments);
							return this;
						},
						promise: function () { return _this.promise.apply(_this, arguments); }
					};

				_this.state = STATE.inited;
				_this.__owner = void(0);
				_this.__surp = 0;

				event.done.on(e => {
					_this.__promise.state = _this.state = STATE.resolved;
					event.fail.lock();
					event.warn.lock();
					event.progress.lock();
				});
				event.fail.on(e => {
					_this.__promise.state = _this.state = STATE.rejected;
					event.done.lock();
					event.warn.lock();
					event.progress.lock();
				});

				jBD.each(event, (d, k) => {
					promise[k] = _this[k] = function () {
						d.on(Array.from(arguments));
						return this;
					};
				});

				jBD.each(action, (d, k) => {
					_this[k] = function () {
						_this[k + "With"](_this, arguments);

						return this;
					};
					_this[k + "With"] = function (own, arg) {
						own = own || _this.__owner;
						d.fireWith(own, arg);

						if (_this.state == k) event.always.fireWith(own, arg);
						else if (k == "notify" && --_this.__surp < 1) _this.resolveWith(own, arg);

						return this;
					};
				});
			},
			/**
			 * 释放函数
			 */
			free:      function () {
				this.__promise = null;
				this.__event = null;
				this.__action = null;
			},
			/**
			 * 绑定事件
			 *
			 * @param {string} tag
			 * @param {function|Array} callback
			 * @returns {Window.TDeferred}
			 */
			on:        function (tag, callback) {
				if (this.__event[tag]) this.__event[tag].on(callback);
				return this;
			},
			/**
			 * 解除绑定
			 *
			 * @param {string} [tag]
			 * @returns {Window.TDeferred}
			 */
			off:       function (tag) {
				if (arguments.length == 0) jBD.each(this.__event, d => d.off());
				else if (this.__event[tag]) this.__event[tag].off();

				return this;
			},
			/**
			 * 返回操作者模式
			 *
			 * @param {object} [own] 所有者
			 * @param {number} [max] 最大done次数,用于适配剩余模式
			 * @returns {null|*}
			 */
			promise:   function (own, max) {
				if (this.state == STATE.inited || this.state == STATE.pending) {
					this.__promise.state = this.state = STATE.pending;

					if (arguments.length == 1 && jBD.isNumber(own)) max = own;
					max = parseInt(jBD.isNumber(max) && max >= 0 ? max : this.__surp);

					this.__owner = own === null ? void(0) : own;
					this.__surp = max > 0 ? max : 0;
				}

				return this.__promise;
			}
		});

		Deferred.create = function (limit, callback) {
			return new Deferred(limit, callback);
		};

		return Deferred;
	})(jBD.TSeal);
	/**
	 * 异步链式对象,扩展Promise对象
	 *
	 */
	TPromise = (Promise => {
		let prop = Promise.prototype;

		prop.end = function (onFulfilled, onRejected) {
			this.then(onFulfilled, onRejected)
				.catch(function (reason) {
					// 抛出一个全局错误
					setTimeout(() => { throw reason }, 0);
				});
		};
		prop.on = function (tag, callback) {
			switch (tag) {
				case "done":
				default:
					return this.then(callback);
				case "fail":
					return this.catch(callback);
				case "always":
					let P = this.constructor;
					return this.then(
						value => P.resolve(callback()).then(() => value),
						reason => P.resolve(callback()).then(() => { throw reason })
					);
			}
		};
		prop.done = function (callback) { return this.on("done", callback); };
		prop.fail = function (callback) { return this.on("fail", callback); };
		prop.always = prop.finally = function (callback) { return this.on("always", callback); };
		prop.resolve = function () {
			let arg = [];
			Array.prototype.push.apply(arg, arguments);
			this.__resolve.call(this, arg.length == 1 ? arg[0] : arg);

			return this;
		};
		prop.reject = function () {
			let arg = [];
			Array.prototype.push.apply(arg, arguments);
			this.__reject.call(this, arg.length == 1 ? arg[0] : arg);

			return this;
		};
		prop.promise = function () { return this; };

		Promise.create = function (callback) {
			let obj, done, fail;

			obj = new Promise(function (resolve, reject) {
				done = resolve;
				fail = reject;
			});
			obj.__resolve = done;
			obj.__reject = fail;

			if (callback) callback(obj);

			return obj;
		};

		return Promise;
	})(Promise);

	/**
	 * 回调对象
	 *
	 * @param {string} [flag=once memory limit] 工作模式
	 * @param {number} [limit=50] 回调列表数量
	 * @param {function} callback 结束回调
	 * @returns {object}
	 */
	own.Callback = function (flag, limit, callback) {
		switch (arguments.length) {
			case 1:
				switch (typeof(flag)) {
					case "number":
						limit = flag;
						break;
					case "function":
						callback = flag;
						break;
				}
				break;
			case 2:
				if (jBD.isFunction(limit)) callback = limit;
				if (jBD.isNumber(flag)) limit = flag;
				break;
		}
		if (!jBD.isString(flag)) flag = "once memory limit";
		limit = jBD.isNumber(limit) && limit >= 1 ? limit : 50;
		if (!jBD.isFunction(callback)) callback = null;

		let opt = {};

		flag.split(" ").forEach(d => opt[d] = true);

		if (opt.memory && !opt.limit) opt.fire = true;

		return new TCallback(
			{
				once:   opt.once === true,
				memory: opt.memory === true,
				limit:  opt.limit === true,
				unique: opt.unique === true,
				fire:   opt.fire === true
			},
			opt.simple === true ? 1 : parseInt(limit),
			callback);
	};
	/**
	 * 异步观察者对象
	 *
	 * @param {boolean} [simple=false] 是否用简单模式
	 * @param {function} [callback] 结束回调
	 * @returns {object}
	 */
	own.Deferred = function (simple, callback) {
		if (arguments.length == 1) callback = simple;

		if (!jBD.isFunction(callback)) callback = null;

		if (simple === true) return TPromise.create(callback);
		else return TDeferred.create(jBD.isNumber(simple, {int: false}) && simple > 0 ? simple : 50, callback);
	};
	/**
	 * 异步链式对象,扩展Promise对象
	 *
	 */
	own.Promise = callback => (TPromise.create(callback));
	/**
	 * Generator函数转Promise对象
	 *
	 * @param {Generator} gen 函数主体，必须是Generator函数
	 * @param {*} [data] 带入参数
	 * @param {*} [owner] 带入所有者
	 * @returns {object}
	 */
	own.Async = function (gen, data, owner) {
		if (!jBD.isFunction(gen)) throw new Error("callback is not Generator");

		if (jBD.isNull(data)) data = [];
		else if (!jBD.isArray(data)) data = [data];
		if (!jBD.isObject(owner)) owner = null;

		const isFunc    = jBD.isFunction,
			  isPromise = jBD.isPromise,
			  isGen     = jBD.isGenerator,
			  toPromise = function (gen, data, owner, first) {
				  if (isPromise(gen)) return gen;

				  return new Promise(function (resolve, reject) {
					  const step = err => {
								let method = err ? "throw" : "next";

								return (arg, rev) => {
									try {
										rev = gen[method](arg);
									}
									catch (e) {
										return reject(e);
									}

									next(rev);
								};
							},
							then = (own, ok, fail) => own.then(ok || step(), fail || step(1)),
							next = rev => {
								if (rev.done) return resolve(rev.value);
								rev = rev.value;

								then(toPromise(rev, [], owner));
							};

					  if (isGen(gen) || (first && isFunc(gen))) gen = gen.apply(owner, data);

					  if (isPromise(gen)) gen.then(resolve, reject);
					  else if (!isGen(gen, true)) resolve(gen);
					  else step()();
				  });
			  };

		return toPromise(gen, data, owner, true);
	};
	/**
	 * 异步函数转换
	 * @param {function} func 普通函数
	 * @returns {Promise<*>}
	 * @constructor
	 */
	own.Done = async function (func) {
		const dtd = TPromise.create(true);

		if (jBD.isFunction(func)) func = TPromise.create(func);

		func.always(() => dtd.resolve());

		return dtd.promise();
	};
})(this);