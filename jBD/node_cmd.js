/**
 * ==========================================
 * Name:           jBD‘s CMD
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    Node CMD
 * Log
 * 2016-11-24    基于ES6改写
 * ==========================================
 */
(own => {
	if (jBD.mode != jBD.MODE.Node) return;
	own.__require = jBD.require = (url, tag) => {
		if (tag === void(0)) tag = url;
		this[tag] = require(url);
	};
	own.define = jBD.define = (factory, own, deps, alias, exec) => {
		if (jBD.isFunction(factory) && exec !== false) factory = factory(own.module, own.exports, require);
		return own.module.exports = factory;
	}
})(this);