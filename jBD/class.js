/**
 * ==========================================
 * Name:           jBD‘s Class
 * Author:         Buddy-Deus
 * CreTime:        2016-04-06
 * Description:    BD‘s Class
 * Log:
 * 2016-04-06    重写代码
 * 2016-04-20    重构List，以函数形式存在，合并Queue和Stack以配对函数区分
 * 2016-04-21    重构ChinaDate，以函数形式存在，内部引用TChinaDate类
 * 2016-11-24    基于ES6改写
 * ==========================================
 */
(own => {
	const initClassError = function () {
			  if (this.getError || this.setError || this.lastError) return;
			  this.__error__ = [];
			  this.errorMax = 5;
			  this.getError = function (index) {
				  index = parseInt(jBD.isNumber(index) && index >= 0 ? index : -1);

				  let list = this.__error__;

				  return list.length < 1 ? null : list[index < 0 ? list.length - 1 : index];
			  };
			  this.setError = function (level, code, msg) {
				  if (arguments.length < 2) code = level;
				  if (arguments.length < 3) msg = code;

				  if (!jBD.has(level, ["log", "info", "error"])) level = "log";
				  if (!jBD.isNumber(code)) code = -1;
				  if (!jBD.isString(msg)) msg = "";

				  let list = this.__error__,
					  err  = {level, code, msg};

				  while (list.length >= this.errorMax) list.shift();
				  list.push(err);

				  this.call("error", [err]);

				  return list[list.length - 1];
			  };
			  this.lastError = function () {
				  let list = this.__error__;

				  return list.length > 0 ? list[list.length - 1] : null;
			  };
		  },
		  initClassEvent = function () {
			  if (this.on || this.off || this.call) return;
			  this.__event__ = {};
			  this.on = function (key, func) {
				  let each = (o, l, n) => {
						  for (let i = n, f; i < l.length; i++) {
							  f = l[i];

							  switch (jBD.type(f, true)) {
								  case "function":
									  o.push(f);
									  break;
								  case "array":
									  each(o, f, 0);
									  break;
							  }
						  }
					  },
					  list = this.__event__[key] || (this.__event__[key] = []);

				  each(list, arguments, 1);

				  return this;
			  };
			  this.off = function (key, func) {
				  if (arguments.length == 0) this.__event__ = {};
				  else if (typeof(func) != "function") this.__event__[key] = [];
				  else {
					  let list = this.__event__[key];

					  if (list && list.length > 0) {
						  for (let i = 0; i < list.length; i++) {
							  if (func !== list[i]) i++;
							  else list.splice(i, 1);
						  }
					  }
				  }

				  return this;
			  };
			  this.call = function (key, arg) {
				  let list = this.__event__[key];

				  if (list && list.length > 0) {
					  if (arguments.length < 2) arg = [];
					  else if (!jBD.isArray(arg)) arg = [arg];

					  for (let i = 0; i < list.length; i++) list[i].apply(this, arg);
				  }

				  return this;
			  };
		  };

	/*
	 ====================================
	 = 类名: TSeal
	 = 功  能: 密封类生成
	 = 对象属性：
	 =   className = 函数名
	 = 对象函数：
	 =   clone     = 复制本体
	 =   Create    = 初始化
	 ====================================
	 */
	own.TSeal = (() => {
		function TSeal () {}

		TSeal.extend = function (childAPI) {
			if (!jBD.isObject(childAPI)) throw new Error(console.error("[TSeal] object is null"));

			const className   = childAPI.className || "TSeal",
				  parentClass = TSeal,
				  initChild   = function () {
					  initClassError.call(this);
					  initClassEvent.call(this);

					  this["create"].apply(this, arguments);

					  return this;
				  };

			let childClass = null;

			eval("childClass=function " + className + "(){return initChild.apply(this,arguments);}");

			childClass.prototype = Object.create(parentClass.prototype);
			childClass.className = className;

			(CLASS => {
				jBD.each(childAPI, (d, k) => {
					if (Object.is(CLASS[k], d)) return;

					CLASS[k] = jBD.clone(d, true);
				});
			})(childClass.prototype);

			return childClass;
		};
		TSeal.className = "TSeal";
		TSeal.prototype = {
			className: "TSeal",
			create:    function () { return this; },
			free:      function () {},
			clone:     function () { return jBD.clone(this, true); }
		};

		return TSeal;
	})();
	/*
	 ====================================
	 = 类名: TObject
	 = 功  能: 基类
	 = 类函数：
	 =   extend    = 继承函数
	 = 对象属性：
	 =   className = 函数名
	 = 对象函数：
	 =   clone     = 复制本体
	 =   create    = 初始化
	 =   free      = 注销
	 =   super     = 继承
	 ====================================
	 */
	own.TObject = (() => {
		function TObject () {}

		TObject.extend = function (childAPI, parentClass) {
			const getClassName = target => {
					  if (target) {
						  if (typeof(target) == "function" && /^function ([a-zA-Z][a-zA-Z0-9_-]*)/.test(target.toString())) return RegExp.$1;
						  if (target.className) return target.className;
					  }

					  return "TObject";
				  },
				  initChild    = function (parentClass, childAPI, arg) {
					  if (typeof(childAPI) == "function") {
						  childAPI = childAPI();
						  for (let k in childAPI) if (!Object.is(this[k], childAPI[k])) this[k] = childAPI[k];
					  }

					  if (!childAPI.hasOwnProperty("create")) parentClass.apply(this, arg);
					  else {
						  initClassError.call(this);
						  initClassEvent.call(this);

						  childAPI["create"].apply(this, arg);
					  }

					  return this;
				  };

			parentClass = parentClass || this;
			childAPI = childAPI || {};

			let className  = [getClassName(parentClass), getClassName(childAPI)],
				childClass = null;

			if (className[0] == className[1] && className[1] == "TObject") className[1] = "Child";

			eval("childClass=function " + className[1] + "(){return initChild.call(this,parentClass,childAPI,arguments);}");

			childClass.__super__ = parentClass;
			childClass.prototype = Object.create(parentClass.prototype);
			childClass.prototype.__super__ = parentClass.prototype;
			childClass.prototype.className = className[1];
			childClass.className = className[1];

			jBD.each(parentClass, (d, k) => {
				if (k == "className" || Object.is(childClass[k], d)) return;
				if (k == "__super__") childClass["__super__"][k] = d;
				else childClass[k] = d;
			});

			(CLASS => {
				jBD.each(childAPI, (d, k, t) => {
					if (k == "__super__" || Object.is(CLASS[k], d)) return;

					CLASS[k] = jBD.clone(d, true);
				});
			})(typeof (childAPI) == "function" ? childClass : childClass.prototype, true);

			return childClass;
		};
		TObject.className = "TObject";
		TObject.prototype = {
			className: "TObject",
			create:    function () { return this; },
			free:      function () {},
			super:     function (method, arg, cls) {
				if (typeof(method) != "string") return;
				if (!cls) cls = this.__super__;
				else {
					if (typeof(cls) == "string" && cls) {
						let _this = this.__super__;
						do {
							if (_this.className != cls) _this = _this.__super__;
							else {
								cls = _this;
								break;
							}
						}
						while (_this.__super__);
					}

					if (!cls.className) return;
				}

				method = cls[method];

				if (!method) return;
				else if (!jBD.isFunction(method)) return method;
				else if (jBD.isNull(arg, true)) return method.call(this);
				else return method.apply(this, jBD.isArray(arg, true) ? arg : [arg]);
			},
			clone:     function () { return jBD.clone(this, true); }
		};

		return TObject;
	})();
	own.TCache = (TObject => {
		return TObject.extend({
			className: "TCache",
			create:    function (size) {
				this.max = size > 0 ? size : 128;
				this.index = 0;
				this.length = 0;

				this.cache = Buffer.alloc(this.max);
			},
			free:      function () {
				this.cache = null;
			},
			surplus:   function () {
				return this.max - this.offset();
			},
			offset:    function () {
				return this.index + this.length;
			},
			resize:    function (force) {
				if (force === true) this.clear();
				else {
					if (this.length > 0) this.cache.copy(this.cache, 0, this.index, this.offset());

					this.index = 0;
				}

				return this.surplus();
			},
			clear:     function () {
				this.index = 0;
				this.length = 0;

				return this;
			},
			push:      function (buf) {
				let offset = buf.byteOffset,
					count  = buf.byteLength,
					write;

				while (count > 0) {
					write = Math.min(count, (write = this.surplus()) > 0 ? write : this.resize());
					if (write < 1) write = this.resize(true);

					buf.copy(this.cache, this.offset(), offset, offset + write);

					this.length += write;
					offset += write;
					count -= write;

					if (this.parse) this.parse();
				}

				return this;
			},
			remove:    function (size) {
				size = Math.min(size, this.length);

				this.index += size;
				this.length -= size;

				return this;
			}
		});
	})(own.TObject);
	/*
		 ====================================
		 = 类名: TList
		 = 功  能: 队列类
		 = 对象属性：
		 =   length    = 数据长度
		 =   type      = 数据限定类型
		 =   first     = 首个数据
		 =   last      = 尾端数据
		 = 对象函数：
		 =   clear     = 清空数据
		 =   sort      = 数据排序，接受自定义排序函数
		 =   each      = 数据检索，遍历函数为true时，删除遍历项
		 =   set       = 设置数据，校验数据类型
		 =   del       = 删除数据
		 =   add       = 添加数据，校验数据类型
		 =   addList   = 批量添加数据
		 =   push      = 先进后出，添加数据
		 =   pop       = 先进后出，获取数据
		 =   put       = 先进先出，添加数据
		 =   poll      = 先进先出，获取数据
		 ====================================
		 */
	own.TList = (TObject => {
		const isType   = (obj, type) => {
				  let objType;

				  if (!type || type == "*" || (type == (objType = jBD.type(obj, true)))) return true;
				  if (type == "object" && (objType == "null" || objType == "undefined")) return false;
				  if (objType != "object") return false;
				  if (type == "date" && obj.className == "TChinaDate") return true;

				  return false;
			  },
			  fmtIndex = (index, def, max) => {
				  index = jBD.isNumber(index) ? index : def;

				  if (index < 0) index = -1;
				  else if (index >= max) index = max;

				  return index;
			  },
			  setData  = (own, data) => {
				  let i;

				  for (i = 0; i < own.length; i++) delete own[i];
				  for (i = 0; i < data.length; i++) own[i] = data[i];

				  if ((own.length = data.length) > 0) {
					  own.first = own[0];
					  own.last = own[own.length - 1];
				  }
				  else {
					  own.first = null;
					  own.last = null;
				  }
			  };

		return TObject.extend({
			className: "TList",
			create:    function (type) {
				this.data = [];
				this.type = jBD.isString(type) ? type : "*";
				this.length = 0;
				this.onChange = null;
			},
			free:      function () { this.clear(); },
			valueOf:   function () { return this.data; },
			clear:     function () {
				this.data = [];
				this.length = 0;

				setData(this, this.data);

				if (this.onChange) this.onChange("clear");

				return this;
			},
			sort:      function (func) {
				if (func = jBD.isFunction(func) ? func : null) this.data.sort(func);
				else this.data.sort();

				setData(this, this.data);

				if (this.onChange) this.onChange("sort");

				return this;
			},
			each:      function (func) {
				if (jBD.isFunction(func)) {
					for (let i = 0, r; i < this.length;) {
						if ((r = func(this.data[i])) === true) this.del(i);
						else if (r === false) break;
						else i++;
					}
				}

				return this;
			},
			"set":     function (index, data) {
				index = fmtIndex(index, -1, this.length);

				if (!isType(data, this.type)) return -1;

				this.data[index] = data;

				if (this.onChange) this.onChange("set", {index: index, data: data});

				return index;
			},
			del:       function (index) {
				let result;

				if (this.length == 0) return null;

				index = fmtIndex(index, 0, this.length - 1);

				if (index <= 0) result = this.data.shift();
				else if (index >= this.length - 1) result = this.data.pop();
				else {
					result = this.data[index];

					this.data.splice(index, 1);
				}

				setData(this, this.data);

				if (this.onChange) this.onChange("del", {index: index});

				return result;
			},
			add:       function (data, index) {
				index = fmtIndex(index, this.length, this.length);

				if (!isType(data, this.type)) return -1;

				if (index <= 0) {
					this.data.unshift(data);

					index = 0;
				}
				else if (index >= this.length) this.data.push(data);
				else this.data.splice(index, 0, data);

				setData(this, this.data);

				if (this.onChange) this.onChange("add", {index: index, data: data});

				return index;
			},
			addList:   function (data, index) {
				if (jBD.isArray(data) && data.length > 0) {
					index = fmtIndex(index, this.length, this.length);

					jBD.each(data, function (d, i) {
						this.add(d, index + i);
					});
				}

				return this;
			},
			push:      function (data) { return this.add(data, this.length); },
			pop:       function () { return this.del(this.length); },
			put:       function (data) { return this.add(data, 0); },
			poll:      function () { return this.del(0); }
		});
	})(own.TSeal);
	/*
	 ====================================
	 = 类名: TChinaDate
	 = 功  能: 中文日期类
	 = 对象函数：
	 =   getTime   = 获取当前毫秒数
	 =   setTime   = 设置当前时间
	 =   toString  = 转换为字符串
	 ====================================
	 */
	own.TChinaDate = (TObject => {
		const ct          = {
				  monthInfoDt: new Date(1900, 0, 31),
				  monthInfo:   [
					  0x04bd8, 0x04ae0, 0x0a570, 0x054d5, 0x0d260, 0x0d950, 0x16554, 0x056a0, 0x09ad0, 0x055d2,
					  0x04ae0, 0x0a5b6, 0x0a4d0, 0x0d250, 0x1d255, 0x0b540, 0x0d6a0, 0x0ada2, 0x095b0, 0x14977,
					  0x04970, 0x0a4b0, 0x0b4b5, 0x06a50, 0x06d40, 0x1ab54, 0x02b60, 0x09570, 0x052f2, 0x04970,
					  0x06566, 0x0d4a0, 0x0ea50, 0x06e95, 0x05ad0, 0x02b60, 0x186e3, 0x092e0, 0x1c8d7, 0x0c950,
					  0x0d4a0, 0x1d8a6, 0x0b550, 0x056a0, 0x1a5b4, 0x025d0, 0x092d0, 0x0d2b2, 0x0a950, 0x0b557,
					  0x06ca0, 0x0b550, 0x15355, 0x04da0, 0x0a5d0, 0x14573, 0x052d0, 0x0a9a8, 0x0e950, 0x06aa0,
					  0x0aea6, 0x0ab50, 0x04b60, 0x0aae4, 0x0a570, 0x05260, 0x0f263, 0x0d950, 0x05b57, 0x056a0,
					  0x096d0, 0x04dd5, 0x04ad0, 0x0a4d0, 0x0d4d4, 0x0d250, 0x0d558, 0x0b540, 0x0b5a0, 0x195a6,
					  0x095b0, 0x049b0, 0x0a974, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0af46, 0x0ab60, 0x09570,
					  0x04af5, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06b58, 0x055c0, 0x0ab60, 0x096d5, 0x092e0,
					  0x0c960, 0x0d954, 0x0d4a0, 0x0da50, 0x07552, 0x056a0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5,
					  0x0a950, 0x0b4a0, 0x0baa4, 0x0ad50, 0x055d9, 0x04ba0, 0x0a5b0, 0x15176, 0x052b0, 0x0a930,
					  0x07954, 0x06aa0, 0x0ad50, 0x05b52, 0x04b60, 0x0a6e6, 0x0a4e0, 0x0d260, 0x0ea65, 0x0d530,
					  0x05aa0, 0x076a3, 0x096d0, 0x04bd7, 0x04ad0, 0x0a4d0, 0x1d0b6, 0x0d250, 0x0d520, 0x0dd45,
					  0x0b5a0, 0x056d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0aa50, 0x1b255, 0x06d20, 0x0ada0
				  ],
				  termInfo:    [
					  0, 21208, 42467, 63836, 85337, 107014, 128867, 150921, 173149, 195551, 218072, 240693,
					  263343, 285989, 308563, 331033, 353350, 375494, 397447, 419210, 440795, 462224, 483532, 504758
				  ],
				  tianGan:     ["甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"],
				  diZhi:       ["子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"],
				  monthStr:    ["月", "正", "二", "三", "四", "五", "六", "七", "八", "九", "十", "冬", "腊"],
				  dayStr:      [
					  ["日", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十"],
					  ["初", "十", "廿", "卅", "　"]
				  ],
				  termStr:     [
					  "小寒", "大寒", "立春", "雨水", "惊蛰", "春分", "清明", "谷雨", "立夏", "小满", "芒种", "夏至",
					  "小暑", "大暑", "立秋", "处暑", "白露", "秋分", "寒露", "霜降", "立冬", "小雪", "大雪", "冬至"
				  ],
				  zodiacStr:   ["鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪"]
			  },
			  getTGDZ     = num => {
				  num = jBD.isNumber(num) ? num : 0;

				  return ct.tianGan[num % 10] + ct.diZhi[num % 12];
			  },
			  getZodiac   = num => ct.zodiacStr[(num - 4) % 12],
			  getTermDate = (year, month) => {
				  return (new Date((31556925974.7 * (year - 1900) + ct.termInfo[month % ct.termInfo.length] * 60000) + Date.UTC(1900, 0, 6, 2, 5))).getUTCDate();
			  },
			  getTerm     = (year, month, date) => {
				  let term = [getTermDate(year, month * 2), getTermDate(year, month * 2 + 1)];

				  for (let i = 0; i < term.length; i++) {
					  if (term[i] == date) return ct.termStr[(month * 2 + i) % ct.termStr.length];
				  }

				  return "";
			  },
			  getYear     = num => getTGDZ(num - 1864),
			  getMonth    = (num, leap) => {
				  return (leap ? "闰" : "") + ct.monthStr[num % ct.monthStr.length];
			  },
			  getDay      = num => {
				  return ct.dayStr[1][(num - num % 10) / 10] + ct.dayStr[0][num % 10 + (num % 10 ? 0 : 10)];
			  };

		let TChinaDate = TObject.extend({
			className: "TChinaDate",
			create:    function (dt) {
				this.nt = {year: 0, month: 0, day: 0};
				this.dt = new Date();

				this.Year = "";
				this.Month = "";
				this.Day = "";
				this.Term = "";
				this.Zodiac = "";
				this.cMonth = "";
				this.cDay = "";
				this.IsLeap = false;

				this.setTime(dt);
			},
			valueOf:   function () { return this.getTime(); },
			getTime:   function () { return this.dt.getTime(); },
			/**
			 * 设置时间
			 *
			 * @public
			 * @param {date} dt
			 * @returns {object} this
			 */
			setTime:   function (dt) {
				const getLeapYearMonth = year => {
						  return ct.monthInfo[year - ct.monthInfoDt.getFullYear()] & 0xf;
					  },
					  getLeapYearDay   = year => {
						  return getLeapYearMonth(year) ? ((ct.monthInfo[year - ct.monthInfoDt.getFullYear()] & 0x10000) ? 30 : 29) : 0;
					  },
					  getTotalYearDay  = year => {
						  let sum   = 348,
							  minfo = ct.monthInfo[year - ct.monthInfoDt.getFullYear()];

						  for (let i = 0x8000; i > 0x8; i >>= 1) sum += (minfo & i) ? 1 : 0;

						  return sum + getLeapYearDay(year);
					  },
					  getTotalMonthDay = (year, month) => {
						  return (ct.monthInfo[year - ct.monthInfoDt.getFullYear()] & (0x10000 >> month)) ? 30 : 29;
					  };

				if (jBD.isDate(dt)) this.dt.setTime(dt.getTime());

				let temp   = 0,
					leap   = 0,
					offset = parseInt((this.dt - ct.monthInfoDt) / 86400000),
					i;

				this.nt.year = 0;
				this.nt.month = 14;
				this.nt.day = offset + 40;

				for (i = ct.monthInfoDt.getFullYear(); i < ct.monthInfoDt.getFullYear() + ct.monthInfo.length && offset > 0; i++) {
					temp = getTotalYearDay(i);

					offset -= temp;
					this.nt.month += 12;
				}

				if (offset < 0) {
					offset += temp;
					i--;
					this.nt.month -= 12;
				}

				this.nt.year = i;
				this.IsLeap = false;

				leap = getLeapYearMonth(i);

				for (i = 1; i < 13 && offset > 0; i++) {
					if (leap > 0 && i == (leap + 1) && !this.IsLeap) {
						i--;
						this.IsLeap = true;
						temp = getLeapYearDay(this.nt.year);
					}
					else
						temp = getTotalMonthDay(this.nt.year, i);

					if (this.IsLeap && i == (leap + 1)) this.IsLeap = false;

					offset -= temp;

					if (!this.IsLeap) this.nt.month++;
				}

				if (offset < 0) {
					offset += temp;

					i--;
					this.nt.month--;
				}
				else if (offset == 0 && leap > 0 && i == leap + 1) {
					if (!this.IsLeap) {
						i--;
						this.nt.month--;
					}

					this.IsLeap = !this.IsLeap;
				}

				this.cYear = getTGDZ(this.nt.year - 1864);
				this.cMonth = getTGDZ(this.nt.month);
				this.cDay = getTGDZ(this.nt.day);

				this.nt.month = i;
				this.nt.day = offset + 1;

				this.Year = getYear(this.nt.year);
				this.Month = getMonth(this.nt.month, this.IsLeap);
				this.Day = getDay(this.nt.day);
				this.Term = getTerm(this.nt.year, this.nt.month, this.dt.getDate());
				this.Zodiac = getZodiac(this.dt.getFullYear());

				return this;
			},
			/**
			 * 转换成字符串
			 *
			 * @public
			 * @param {string} [fmt=Y年(Z) M月 D T]
			 * @returns {string}
			 */
			toString:  function (fmt) {
				let result = jBD.isString(fmt) ? fmt : "Y年(Z) M月 D T";

				if (/((CY|CM|Y|M|D|T|Z)+)/.test(result)) {
					fmt = {
						"CY+": this.cYear,
						"CM+": this.cMonth,
						"CD+": this.cDay,
						"Y+":  this.Year,
						"M+":  this.Month,
						"D+":  this.Day,
						"T+":  this.Term,
						"Z+":  this.Zodiac
					};

					jBD.each(fmt, function (d, k) {
						let rxp = new RegExp("(" + k + ")", "g");

						if (rxp.test(result)) result = result.replace(rxp, d + "");
					});
				}

				return result.trim();
			}
		});
		TChinaDate.toString = function (fmt, date) {
			if (date == void(0) && jBD.isDate(fmt)) {
				date = fmt;
				fmt = "";
			}

			return (new TChinaDate(date)).toString(fmt);
		};

		return TChinaDate;
	})(own.TSeal);
	/*
	 ====================================
	 = 类名: TChinaDate
	 = 功  能: 中文日期类
	 = 对象函数：
	 =   New          = 获取新Guid
	 =   toString     = 获取字符串
	 =   toByteArray  = 获取字节数据
	 ====================================
	 */
	own.TGuid = (TObject => {
		const rnd = l => Math.round(Math.random() * (Math.pow(2, 8 * l) - 1)),
			  com = (s, l) => {
				  while (s.length < l * 2) s = "0" + s;

				  return s;
			  },
			  des = (r, d) => {
				  d.reverse();
				  for (let i = 0; i < d.length; i++) if (d[i].length > 0) r.push(parseInt("0x" + d[i]));
			  },
			  gd  = [4, 2, 2, 2, 6];

		let TGuid = TObject.extend({
			className:   "TGuid",
			create:      function (data) {
				const fmt = (d, i, t, list) => {
					switch (t) {
						case "string":
							d = "0x" + d;
							break;
						case "number":
							if (!isNaN(d = parseInt(d))) break;
						default:
							return false;
					}

					if (d < 0 || d >= Math.pow(2, 8 * gd[i])) return false;

					list[i] = d;
				};

				this.value = [0, 0, 0, 0, 0];

				switch (jBD.type(data, true)) {
					case "string":
						data = data.replace(/^(\{)|(\})$/g, "").split("-");
						break;
					case "array":
						if (data.length == gd.length) break;
					default:
						data = [];
						break;
				}

				if (data.length == gd.length) {
					if (jBD.each(data, fmt)) this.value = data;
				}
			},
			valueOf:     function () { return this.toString(); },
			toString:    function () {
				let value  = this.value,
					result = "";

				for (let i = 0; i < value.length; i++)
					result += com(value[i].toString(16), gd[i]) + (i < value.length - 1 ? "-" : "");

				return result;
			},
			toByteArray: function () {
				let value  = this.value,
					result = [];

				for (let i = 0; i < value.length; i++)
					des(result, value[i].toString(16).split(/(\w{2})/g));

				return result;
			},
			New:         function () {
				this.value = [rnd(4), rnd(2), rnd(2), rnd(2), rnd(6)];

				return this;
			}
		});
		TGuid.toString = function () { return (new TGuid()).New().toString(); };
		TGuid.toByteArray = function () { return (new TGuid()).New().toByteArray(); };
		TGuid.New = function () { return (new TGuid()).New(); };

		return TGuid;
	})(own.TSeal);

	/**
	 * 队列函数
	 *
	 * @param {string} [type=*] 限定类型，对输入内容进行过滤
	 * @returns {object}
	 */
	own.List = type => (new own.TList(type));
	/**
	 * 中文日期函数
	 *
	 * @param {date} [dt=now]
	 * @returns {object}
	 */
	own.ChinaDate = dt => (new own.TChinaDate(dt));
	own.NewGuid = () => (new own.TGuid()).New();
	own.EmptyGuid = () => (new own.TGuid());
})(this);