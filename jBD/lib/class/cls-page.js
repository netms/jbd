jBD.define(function () {
	/*
	 ====================================
	 = 类名: TPage
	 = 功  能: Page类
	 = 子函数：
	 =   Init    = 虚函数
	 ====================================
	 */
	var TPage = jBD.TObject.extend({
		opt:    null, url: null, site: null,
		tag:    "", tpl: "", css: null, js: null,
		Create: function (opt, url) {
			var _this = this;

			_this.opt = jBD.isNull(opt, true) ? null : opt;
			_this.url = jBD.isNull(url, true) ? jBD.Request.Query() : url;

			return _this;
		},
		Init:   function () {
			var _this = this,
				_head = $("head"),
				i;

			_head.find("link[own=\"bd\"]").remove();
			if (_this.css) {
				for (i = 0; i < _this.css.length; i++)
					_head.append($("<link own=\"bd\" rel=\"stylesheet\" type=\"text/css\">​").attr("href", jBD.Request.Rmd(false, _this.css[i])));
			}

			_head.find("script[own=\"bd\"]").remove();
			if (_this.js) {
				for (i = 0; i < _this.js.length; i++)
					_head.append($("<script own=\"bd\" type=\"text/javascript\">").attr("src", jBD.Request.Rmd(false, _this.js[i])));
			}

			$("body").empty();
			$("body")
				.append("<header></header>")
				.append("<div id=\"bodyer\"></div>")
				.append("<footer></footer>");
		},
		UnInit: function () {
			var _this = this,
				_head = $("head"),
				i;

			if (_this.css) {
				for (i = 0; i < _this.css.length; i++) {
					_head.find("link[own=\"bd\"][type=\"text/css\"][href*=\"" + _this.css[i] + "\"]").remove();
				}
			}

			if (_this.js) {
				for (i = 0; i < _this.js.length; i++) {
					_head.find("link[own=\"bd\"][type=\"text/javascript\"][src*=\"" + _this.js[i] + "\"]").remove();
				}
			}
		},
		Href:   function (href) {
			href = jBD.isString(href, true) ? href : "";

			if (this.site && href.length > 0) {
				if (this.IsHref(href)) {
					this.site.Go("", eval("(" + href + ")"));
				}
				else {
					window.location.href = href;
				}

				return true;
			}

			return false;
		},
		IsHref: function (href) {
			href = jBD.isString(href, true) ? href : "";

			return jBD.String.Trim(href, ["{", "}"]) != href;
		}
	});

	return TPage;
});