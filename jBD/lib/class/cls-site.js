jBD.define(function () {
	/*
	 ====================================
	 = 类名: TSite
	 = 功  能: Site类
	 = 子函数：
	 =   Init    = 虚函数
	 ====================================
	 */
	var TSite = jBD.TObject.extend({
		opt:        null, cfg: null, page: null, url: null, now: {tag: "", page: null, param: null},
		Create:     function (opt, cfg) {
			var _this = this;

			_this.opt = jBD.isNull(opt, true) ? null : opt;
			_this.cfg = jBD.isNull(cfg, true) ? null : cfg;
			_this.page = new jBD.TDictionary("string", "object");
			_this.page.OnChange = _this.PageChange;
			_this.url = jBD.Request.Query();

			_this.Init();

			return _this;
		},
		PageChange: function (data, type) {
			type = jBD.isString(type, true) ? type : "";

			switch (type) {
				case "del":
					data.value.UnInit();
					break;
			}
		},
		Init:       function () {
			var _this = this;

			if (!_this.page.Exist("index") && _this.page.Count() > 0)
				_this.page.Add("index", _this.page.First().value);

			_this.Go(jBD.Request.Get("action", 2, _this.url));
		},
		UnInit:     function () {
			var _this = this;

			_this.now.tag = "";
			_this.now.page = null;
			_this.now.param = null;

			_this.page.Each(function (d) { return false; });
		},
		Add:        function (page, tag) {
			var _this = this;

			tag = jBD.isString(tag, true) ? tag : page.tag;

			page.url = _this.url;
			page.site = _this;

			_this.page.Add(tag, page);

			return _this;
		},
		Del:        function (tag) {
			var _this = this;

			_this.page.Del(tag);

			return _this;
		},
		Go:         function (tag, param) {
			param = jBD.isObject(param, true) ? param : null;
			tag = jBD.isString(tag, true) ? tag : (param ? param.action : "");

			var _this = this,
				_page = this.page.Item(tag);

			if (_page) {
				if (_this.now.page) _this.now.page.UnInit();

				_this.now.tag = tag;
				_this.now.page = _page;
				_this.now.param = param;

				if (param) {
					jBD.Request.Clear(false, _this.url);
					jBD.Request.Set("action", tag, 2, true, _this.url);

					for (var key in param) {
						if (key != "action")
							jBD.Request.Set(key, param[key], 2, true, _this.url);
					}
				}

				return _page.Init();
			}
			else if (tag != "index")
				return _this.Go("index");
			else
				return null;
		},
		Page:       function (tag, page) {
			page = jBD.isNull(page, true) ? null : page;

			if (page) {
				this.page.Set(tag, page);

				return page;
			}
			else
				return this.page.Item(tag);
		},
		Index:      function (page) {
			page = jBD.isNull(page, true) ? null : page;

			if (page) {
				this.page.Set("index", page);

				return page;
			}
			else
				this.Go("index");
		}
	});

	return TSite;
});