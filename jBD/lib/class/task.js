/**
 * ==========================================
 * Name:            TTask
 * Author:            Buddy-Deus
 * CreTime:            2014-11-20
 * Description:        任务队列类
 * Log
 * 2015-06-09    初始化
 * ==========================================
 */
jBD.define(function () {
	var taskType = {
			Other: 1,
			Once:  2,
			Ever:  3
		},
		taskMode = {
			Other: 1,
			Wait:  2
		},
		getKey   = function () {
			return "K" + jBD.String.Random(jBD.String.ULLN(), 3);
		};

	var Task = function (scope, time) {
		var _state = false,
			_scope = scope,
			_time  = jBD.isNumber(time) && time > 0 ? time : 0,
			_queue = [
				[],
				{}
			],
			api;

		function callEvent () {
			var i = 0;

			function setSure (k, t) {
				return function () {
					switch (t) {
						default:
							_queue[1][k].cnt--;
							break;
						case taskType.Ever:
							_queue[1][k].cnt = 5;
						case taskType.Once:
							_this.Del(k);
							i--;
							break;
					}

					i++;
					call();
				}
			}

			function call () {
				var k, d;

				if (!_state) return;

				if (i < _queue[0].length) {
					k = _queue[0][i];
					d = _queue[1][k];

					if (d && d.cnt > 0) {
						switch (d.mode) {
							default:
								if (!d.run) d.task.call(_scope, d.param);
								setSure(d.key, d.type)();
								break;
							case taskMode.Wait:
								if (!d.run) d.task.apply(_scope, d.param ? [d.param, setSure(d.key, d.type)] : setSure(d.key, d.type));
								break;
						}
					}
					else setSure(k, taskType.Once)();
				}
				else timeEvent(true);
			}

			call();
		}

		function timeEvent (s) {
			if (!s) callEvent();
			else if (_queue[0].length > 0 && _time > 0) setTimeout(timeEvent, _time);
			else _state = false;
		}

		return api = {
			Add:   function (task, param, type, mode) {
				if (!jBD.isFunction(task)) return "";

				if (arguments.length == 3) {
					mode = type;
					type = taskType.Once;
				}

				task = {
					key:   getKey(),
					run:   false,
					cnt:   0,
					task:  task,
					param: param || null,
					type:  jBD.has(taskType, type) ? type : taskType.Once,
					mode:  jBD.has(taskMode, mode) ? mode : taskMode.Other
				};

				while (_queue[1][task.key]) task.key = getKey();

				_queue[0].push(task.key);
				_queue[1][task.key] = task;

				if (!_state) timeEvent(!(_state = true));

				return task.key;
			},
			Del:   function (k) {
				if (_queue[1][k]) {
					delete _queue[1][k];

					jBD.each(_queue[0], function (d, i) {
						if (d === k) {
							_queue[0].splice(i, 1);

							return false;
						}
					});
				}
			},
			Clear: function () {
				_this.Stop();

				_queue = [
					[],
					{}
				];
			},
			Start: function (scope, time) {
				if (scope) _scope = scope;
				if (time) {
					_time = Number(time);
					_time = isNaN(_time) || _time < 1 ? 0 : _time;
				}

				timeEvent(!(_state = true));
			},
			Stop:  function () {
				_state = false;
			}
		};
	};

	Task.Type = taskType;
	Task.Mode = taskMode;

	return Task;
}, {module: module, exports: this}, ["String"], "Task");