/**
 * ==========================================
 * Name:           Event
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    Event操作
 * Log
 * 2015-06-08    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";
	let api;

	api = {
		/**
		 * 添加监听
		 *
		 * @public
		 * @param obj        {Object}
		 * @param name        {String}
		 * @param callback    {Function}
		 * @param empty        {Boolean}
		 */
		Add: (obj, name, callback, empty) => {
			if (!jBD.isString(name)) return;

			callback = jBD.isFunction(callback) ? callback : null;

			if (obj && callback) {
				if (jBD.isjQuery(obj)) {
					if (jBD.isBool(empty) ? false : empty) obj.off();

					obj.on(name, callback);
				}
				else {
					obj.addEventListener(name, callback, false);
				}
			}
		},
		/**
		 * 删除监听
		 *
		 * @public
		 * @param obj        {Object}
		 * @param name        {String}
		 * @param callback    {Function}
		 */
		Del: function (obj, name, callback) {
			if (!jBD.isString(name)) return;

			callback = jBD.isFunction(callback) ? callback : null;

			if (obj && callback) {
				if (jBD.isjQuery(obj)) {
					if (callback) obj.off(name, callback);
					else obj.off(name);
				}
				else {
					obj.removeEventListener(name, callback, false);
				}
			}
		}
	};

	return api;
}, {module: module, exports: this}, [], "Event");