/**
 * ==========================================
 * Name:           DOM
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    DOM操作
 * Log
 * 2015-06-08    优化模块结构
 * 2015-06-16    更改结构，加入html解析，实现jquery
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";
	let qMethod = {
			Create: 1,
			Filter: 2,
			Finder: 3
		},
		DOM, Query, _$;

	/**
	 * DOM选择器引擎，筛选、查找节点
	 * 精简自Sizzle
	 */
	(function (head, body) {
		const getEle      = function (dom, list, mode, write) {
				  list = jBD.isNumber(list.length) ? list : [list];
				  write = write === true;
				  mode = mode && jBD.isNumber(mode) && mode >= 0 ? Math.floor(mode) : 8;

				  for (var i = 0, j = 0, n = 0, result = []; i < dom.length;) {
					  for (j = 0; j < list.length; j++) {
						  n = dom[i].compareDocumentPosition(list[j]);
						  if ((n & mode) == mode || n == 1) break;
					  }

					  if (j >= list.length) {
						  if (write) dom.splice(i, 1);
						  else result.push(dom[i++]);
					  }
					  else if (write) i++;
				  }

				  return write ? dom : result;
			  },
			  getEleID    = function (name, dom) {
				  var result = document.getElementById(name);

				  return (result && dom && (result.compareDocumentPosition(dom) & 8) == 8) ? result : null;
			  },
			  getEleTag   = function (name, dom) {
				  var result = [];

				  jBD.each((dom || document).getElementsByTagName(name), function (d) {
					  result.push(d);
				  }, 0, false, true);

				  return result;
			  },
			  getEleName  = function (name, dom) {
				  return getEle(document.getElementsByName(name), dom || document);
			  },
			  getEleClass = function (name, dom) {
				  var result = [],
					  child  = function (dom, name) {
						  var r = [],
							  i, j, c;

						  for (i = 0; i < dom.length; i++) {
							  if (dom[i].nodeType != 1) continue;

							  for (j = 0, c = dom[i].className.split(" "); j < name.length; j++) {
								  if (c.indexOf(name[i]) < 0) break;
							  }

							  if (j >= name.length) r.push(dom[i]);
							  else Array.prototype.push.apply(r, child(dom[i], name));
						  }

						  return r;
					  };

				  if (!jBD.isArray(name)) name = [name];
				  dom = dom || document;

				  if (dom.getElementsByClassName) {
					  jBD.each(dom.getElementsByClassName(name.join(" ")), function (d) {
						  result.push(d);
					  }, 0, false, true);
				  }
				  else result = child(getEleChild(body), name);

				  return xpathUnique(result);
			  },
			  getEleChild = function (dom, deep, all) {
				  all = all === true;
				  deep = deep === true;
				  dom = dom.childNodes;

				  for (var i = 0, result = [], d = null; i < dom.length; i++) {
					  if ((d = dom[i]).nodeType == 1 || all) {
						  result.push(d);

						  if (deep && d.nodeType == 1) Array.prototype.push.apply(result, getEleChild(d, true));
					  }
				  }

				  return result;
			  },
			  getCssStyle = function (name) {
				  var rxp = /-([a-zA-Z])/;

				  switch (name) {
					  default:
						  while (rxp.exec(name)) name = name.replace(RegExp.lastMatch, RegExp.$1);
						  break;
					  case "float":
						  name = "floatStyle";
				  }

				  return name;
			  },
			  getStyleCss = function (name) {

			  },
			  xpathSplit  = function (str, list) {
				  var rxpSplit = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(?:\s*(,)\s*)?((?:.|\r|\n)*)/,
					  rxpOper  = /[ >+~]/,
					  rxp, tmp;

				  list = list || [];

				  if (!(rxp = rxpSplit.exec(str))) return list;

				  if (list.length < 1 || str[0] == " " || rxpOper.test(rxp[1])) list.push(rxp[1]);
				  else {
					  tmp = list[list.length - 1];
					  (jBD.isArray(tmp) ? tmp : list).push(rxp[1]);
				  }

				  tmp = list[list.length - 1];

				  if (rxp[2] === "," && !jBD.isArray(tmp)) list[list.length - 1] = [tmp];

				  return split(rxp[3], list);
			  },
			  xpathParse  = function (list) {
				  var rxpOper   = /[ >+~]/,
					  rxpTag    = /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
					  rxpID     = /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
					  rxpClass  = /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/g,
					  rxpName   = /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
					  rxpAttr   = /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/g,
					  rxpPos    = /:(eq|gte|lte|gt|lt|first|last|even|odd|not|animated|lang|hidden|visible|has|empty|parent)(?:\((\d*)\))?(?=[^\-]|$)/,
					  rxpChild  = /:((?:only|nth|last|first)-child)(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
					  rxpPseudo = /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/,
					  index     = -1,
					  items     = [],
					  tmp;

				  jBD.each(list, function (d, i) {
					  if (rxpOper.test(d)) return items.push(d == " " ? ">" : d);
					  else items.push(">");

					  tmp = items.pop();
					  items.push(tmp = {
						  "oper": tmp || "",
						  "data": null
					  });

					  if (jBD.isArray(d)) return tmp.data = xpathParse(d).items;
					  else {
						  tmp.data = {
							  "length": 1,
							  "id":     "",
							  "tag":    "*",
							  "name":   "",
							  "class":  [],
							  "attr":   [],
							  "pos":    null
						  };
					  }

					  if (rxpID.test(d)) {
						  tmp.data["id"] = RegExp.$1;
						  if (index < 0) index = i;
					  }
					  if (rxpName.test(d)) tmp.data["name"] = RegExp.$1.toUpperCase();
					  if (rxpTag.test(d)) tmp.data["tag"] = RegExp.$1.toUpperCase();
					  while (rxpClass.exec(d)) tmp.data["class"].push(RegExp.$1);
					  while (rxpAttr.exec(d)) tmp.data["attr"].push({
						  "key":   RegExp.$1,
						  "oper":  RegExp.$2 || "",
						  "value": RegExp.$4 || ""
					  });

					  //优先级：基类 > 伪类 > 子类
					  if (rxpPos.test(d)) tmp.data["pos"] = {"type": 1, "key": RegExp.$1, "value": RegExp.$2};
					  else if (rxpPseudo.test(d)) tmp.data["pos"] = {"type": 2, "key": RegExp.$1, "value": RegExp.$3};
					  else if (rxpChild.test(d)) tmp.data["pos"] = {"type": 3, "key": RegExp.$1, "value": RegExp.$2};

					  if (tmp = tmp.data["pos"]) {
						  //定位数据值的格式化
						  switch (tmp.key) {
							  case "eq":
							  case "gt":
							  case "gte":
							  case "lt":
							  case "lte":
								  tmp.value = parseInt(tmp.value);
								  break;
							  case "nth-child":
								  if (tmp.value != "even" && tmp.value != "odd") tmp.value = parseInt(tmp.value);
								  break;
						  }
					  }
				  });

				  return {
					  index: index,
					  items: items
				  };
			  },
			  xpathUnique = function (list1, list2) {
				  var result = [],
					  i, j, l;

				  for (i = 0; i < list1.length; i++) {
					  for (j = 0, l = list2 || result; j < l.length; j++) {
						  if (!list1[i].compareDocumentPosition(l[j])) break;
					  }

					  if (j >= l.length) result.push(list1[i]);
				  }

				  return result;
			  };

		/**
		 * 初始化
		 *
		 * @public
		 * @param selector        {String/DOM/jQuery}
		 * @param context        {String/DOM/jQuery}
		 * @return {Array}
		 */
		DOM = function (selector, context) {
			let result = [];

			if (!jBD.isObject(selector) || !selector.items) {
				if (!jBD.isString(selector)) return result;

				//初始化分割选择条件
				selector = xpathParse(xpathSplit(selector));
			}

			//查询、筛选过滤选择条件
			(function (index, xpath) {
				let oper, search;

				if (xpath.length < 1) return;

				//正向查找，以ID为起始
				if (index >= 0) {
					oper = xpath[index].oper;

					if (result = getEleID(xpath[index].data.id, context)) search = result = [result];
					else return result = [];

					for (i = index + 1; i < xpath.length && result.length > 0; i++) {
						switch (xpath[i].oper) {
							case ">":
							default:
								result = DOM.child(result, xpath[i].data, true);
								break;
							case "+":
								result = DOM.next(result, xpath[i].data, false);
								break;
							case "~":
								result = DOM.next(result, xpath[i].data, true);
								break;
						}
					}

					if (index == 0) return;
				}
				else {
					oper = xpath[xpath.length - 1].oper;
					search = xpath[xpath.length - 1].data;
					result = [context || document];

					if (search.name) search = getEleName(search.name, result);
					else if (search.class) search = getEleClass(search.class, result);
					else if (search.tag) search = getEleTag(search.tag, result);
					else search = getEleChild(result, true);

					search = result = DOM.filter(search, xpath[xpath.length - 1].data);
				}

				if (result.length < 1) return;

				//反向筛选，以ID-1为起始
				for (var i = (index < 0 ? xpath.length - 1 : index) - 1; i >= 0 && search.length > 0; i--) {
					switch (oper) {
						case ">":
						default:
							search = DOM.parent(search, xpath[i].data, true);
							break;
						case "+":
							search = DOM.next(search, xpath[i].data, false);
							break;
						case "~":
							search = DOM.next(search, xpath[i].data, true);
							break;
					}

					oper = xpath[i].oper;
				}

				if (search.length < 1) return result = [];

				getEle(result, search, 8, true);
			})(selector.index, selector.items);

			return result;
		};

		jBD.clone({
			parse:         function (selector) {
				return jBD.isString(selector) ? xpathParse(xpathSplit(selector)) : null;
			},
			/**
			 * 本元素条件筛选
			 *
			 * @public
			 * @param dom  {Array}
			 * @param xpath  {Object}
			 * @return {Array}
			 */
			filter:        function (dom, xpath) {
				var result = [],
					tmp;

				if (dom.length < 1) return result;
				else if (!jBD.isObject(xpath)) return Array.prototype.push.apply(result, dom);

				jBD.each(dom, function (node) {
					if (xpath.id && node.id != xpath.id) return;
					if (xpath.tag && xpath.tag != "*" && node.nodeName != xpath.tag) return;
					if (xpath.name && node.getAttribute("name") != xpath.name) return;
					if (xpath.class && xpath.class.length > 0) {
						tmp = node.className.split(" ");

						if (tmp.length < xpath.class.length || !jBD.each(xpath.class, function (d) { if (tmp.indexOf(d) < 0) return false; })) return;
					}
					if (xpath.attr && xpath.attr.length > 0) {
						tmp = jBD.each(xpath.attr, function (attr, val) {
							if ((val = node.getAttribute(attr.key)) === null && attr.oper != "!=") return false;

							switch (attr.oper) {
								case "":
									break;
								case "＝":
									if (val != attr.value) return false;
									break;
								case "!=":
									if (val === attr.value) return false;
									break;
								case "*=":
									if (val.length < attr.length || val.indexOf(attr.value) < 0) return false;
									break;
								case "^=":
									if (val.length < attr.length || val.indexOf(attr.value) != 0) return false;
									break;
								case "$=":
									if (val.length < attr.length || val.lastIndexOf(attr.value) != val.length - attr.length) return false;
									break;
							}
						});

						if (!tmp) return;
					}

					result.push(node);
				});

				if (result.length < 1 || !(tmp = xpath.pos)) return result;

				switch (tmp.type) {
					case 1:
						//基类
						switch (tmp.key) {
							case "eq":
								result = !isNaN(tmp.value) && Math.abs(tmp.value) < result.length ? [result[(tmp.value < 0 ? result.length : 0) + tmp.value]] : [];
								break;
							case "gt":
								result = !isNaN(tmp.value) && Math.abs(tmp.value) < result.length ? result.slice(tmp.value + 1) : [];
								break;
							case "gte":
								result = !isNaN(tmp.value) && Math.abs(tmp.value) < result.length ? result.slice(tmp.value) : [];
								break;
							case "lt":
								result = !isNaN(tmp.value) && Math.abs(tmp.value) < result.length ? result.slice(0, result.length + tmp.value * -1) : [];
								break;
							case "lte":
								result = !isNaN(tmp.value) && Math.abs(tmp.value) < result.length ? result.slice(0, result.length + tmp.value * -1 + 1) : [];
								break;
							case "first":
								result = [result[0]];
								break;
							case "last":
								result = [result[result.length - 1]];
								break;
							case "even":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d, i) {
									if (i % 2 == 0) result.push(d);
								}, 0, false, true);
								break;
							case "odd":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d, i) {
									if (i % 2 != 0) result.push(d);
								}, 0, false, true);
								break;
							case "not":
								result = DOM.not(result, tmp.value);
								break;
							case "animated":
								result = DOM.filter(result, {
									"attr": [{
										"key":   "animated",
										"oper":  "=",
										"value": "animated"
									}]
								});
								break;
							case "lang":
								result = DOM.filter(result, {
									"attr": [{
										"key":   "lang",
										"oper":  "=",
										"value": tmp.value
									}]
								});
								break;
							case "hidden":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (DOM.is(d, "hidden")) result.push(d);
								}, 0, false, true);
								break;
							case "visible":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (DOM.is(d, "visible")) result.push(d);
								}, 0, false, true);
								break;
							case "has":
								jBD.each(result, function (d, i) {
									if (!DOM.has(d, tmp.vale)) result[i] = false;
								}, 0, false, true);

								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (d) result.push(d);
								}, 0, false, true);
								break;
							case "empty":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (!d.hasChildNodes()) result.push(d);
								});
								break;
							case "parent":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (d.parentNode) result.push(d);
								});
								break;
						}
						break;
					case 2:
						//伪类
						switch (tmp.key) {
							case "button":
							case "checkbox":
							case "file":
							case "image":
							case "password":
							case "radio":
							case "reset":
							case "submit":
							case "text":
								result = DOM.filter(result, {
									"tag":  "input",
									"attr": [{
										"key":   "type",
										"oper":  "=",
										"value": tmp.key
									}]
								});
								break;
							case "input":
								result = DOM.filter(result, {
									"tag": "input"
								});
								break;
							case "checked":
							case "disabled":
							case "enabled":
								result = DOM.filter(result, {
									"tag":  "input",
									"attr": [{
										"key":  tmp.key,
										"oper": ""
									}]
								});
								break;
							case "focus":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (d.hasFocus && d.hasFocus()) result.push(d);
								}, 0, false, true);
								break;
							case "selected":
								result = DOM.filter(result, {
									"tag":  "option",
									"attr": [{
										"key":  tmp.key,
										"oper": ""
									}]
								});
								break;
						}
						break;
					case 3:
						//子类
						switch (tmp.key) {
							case "first-child":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									d = getEleChild(d);
									if (d.length > 0) result.push(d[0]);
								}, 0, false, true);
								break;
							case "last-child":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									d = getEleChild(d);
									if (d.length > 0) result.push(d[d.length - 1]);
								}, 0, false, true);
								break;
							case "only-child":
								result = (tmp = result) && [];
								jBD.each(tmp, function (d) {
									if (getEleChild(d.parentNode).length == 1) result.push(d);
								}, 0, false, true);
								break;
							case "nth-child":
								if (!jBD.isString(tmp.value) && isNaN(tmp.value)) result = [];
								else {
									result = (tmp = result) && [];
									jBD.each(tmp, function (node, val, list) {
										val = xpath.pos.value;
										list = getEleChild(node.parentNode);

										if (jBD.isNumber(val)) {
											if (Math.abs(val) < list.length) result.push(list[(val < 0 ? list.length : val) + val]);
										}
										else {
											jBD.each(list, function (d, i) {
												if ((val == "even" && i % 2 == 0) || (val == "odd" && i % 2 != 0)) result.push(d);
											}, 0, false, true);
										}
									}, 0, false, true);
								}
								break;
						}
						break;
				}

				return xpathUnique(result);
			},
			/**
			 * 子元素条件筛选
			 *
			 * @public
			 * @param dom  {Array}
			 * @param xpath  {Object}
			 * @param deep {Boolean} deep=false;
			 * @return {Array}
			 */
			child:         function (dom, xpath, deep) {
				var result = [],
					list;

				deep = deep === true;

				jBD.each(dom, function (node) {
					list = getEleChild(node);

					Array.prototype.push.apply(result, xpath ? DOM.filter(list, xpath) : list);

					if (deep) Array.prototype.push.apply(result, DOM.child(list, xpath, true));
				}, 0, false, true);

				return xpathUnique(result);
			},
			/**
			 * 父元素条件筛选
			 *
			 * @public
			 * @param dom  {Array}
			 * @param xpath  {Object}
			 * @param deep {Boolean} deep=false;
			 * @return {Array}
			 */
			parent:        function (dom, xpath, deep) {
				var result = [],
					list   = [];

				jBD.each(dom, function (node) {
					if (node.parentNode) list.push(node.parentNode);
				}, 0, false, true);

				Array.prototype.push.apply(result, xpath ? DOM.filter(list, xpath) : list);

				if (deep === true) Array.prototype.push.apply(result, DOM.parent(list, xpath, true));

				return xpathUnique(result);
			},
			next:          function (dom, xpath, deep) {
				var result = [],
					temp;

				deep = deep === true;

				jBD.each(dom, function (node) {
					if (deep) Array.prototype.push.apply(result, xpathUnique(getEleChild(node.parentNode), [node]));
					else if (temp = node.nextSibling) result.push(temp);
				}, 0, false, true);

				return DOM.filter(result, xpath);
			},
			prev:          function (dom, xpath, deep) {
				var result = [],
					temp;

				deep = deep === true;

				jBD.each(dom, function (node) {
					if (deep) Array.prototype.push.apply(result, xpathUnique(getEleChild(node.parentNode), [node]));
					else if (temp = node.previousSibling) result.push(temp);
				}, 0, false, true);

				return DOM.filter(result, xpath);
			},
			/**
			 * 筛选不符合条件的结果
			 *
			 * @public
			 * @param dom  {Array}
			 * @param selector  {string}
			 * @return {Array}
			 */
			not:           function (dom, selector) {
				var list;

				selector = xpathParse(xpathSplit(selector));

				if (selector.items.length == 1) list = DOM.filter(dom, selector.items[0]);
				else list = DOM(selector);

				return xpathUnique(dom, list);
			},
			/**
			 * 判断是否符合条件
			 *
			 * @public
			 * @param node  {DOM}
			 * @param param  {string}
			 * @return {Boolean}
			 */
			is:            function (node, param) {
				//判断hidden、visible
				return false;
			},
			/**
			 * 判断是否存在条件
			 *
			 * @public
			 * @param node  {DOM}
			 * @param selector  {string}
			 * @return {Boolean}
			 */
			has:           function (node, selector) {
				return DOM(selector, node).length > 0;
			},
			pos:           function (n1, n2, mode) {
				if (!mode) mode = 0;
				else if (!jBD.isNumber(mode) || mode < 0) mode = 0;
				else mode = Math.floor(mode);

				n2 = n1.compareDocumentPosition(n2);

				return mode == 0 || mode == 1 || mode == 32 ? n2 : (n2 & mode) == mode;
			},
			unique:        function (dom, list) { return xpathUnique(dom, list); },
			build:         function (dom, context) {
				for (var i = 0, f = document.createDocumentFragment(); i < dom.length; i++) f.appendChild(dom[i]);

				context.appendChild(f);
			},
			createElement: function (data, param, context) {
				if (!jBD.isString(data = data.trim())) return [];
				if (!jBD.isObject(param) || jBD.isNull(param, {udf: true, obj: true})) param = null;
				if (!context || !context.nodeType) context = null;

				var result = /^<(\w+)\s*\/?>(?:<\/\1>|)$/.exec(data);

				if (result) {
					result = [document.createElement(result[1])];
				}
				else {
					result = document.createElement("div");
					result.innerHTML = data;

					result = getEleChild(result, false, true);
				}

				if (param) {
					jBD.each(result, function (d) {
						if (d.nodeType == 1) jBD.each(param, function (v, k) { DOM.setStyle(d, k, v); });
					});
				}

				if (context) DOM.build(result, context);

				return result;
			},
			getID:         function (name, dom) { return getEleID(name, dom); },
			getTag:        function (name, dom) { return getEleTag(name, dom); },
			getName:       function (name, dom) { return getEleName(name, dom); },
			getClass:      function (name, dom) { return getEleClass(name, dom); },
			getChild:      function (dom, deep, all) { return getEleChild(dom, deep, all); },
			getList:       function (dom, context, mode) { return getEle(dom, context, mode); },
			/**
			 * 设置节点属性、样式
			 *
			 * @public
			 * @param node  {DOM}
			 * @param key  {String}
			 * @param value {String}
			 * @return {Boolean}
			 */
			setStyle:      function (node, key, value) {
				if (!node || !node.nodeType || !jBD.isString(key)) return false;
				if (value === void 0) value = null;
				if (jBD.isNumber(value)) value = value + "px";

				key = getCssStyle(key);
				switch (key) {
					case "css":
					case "style":
						jBD.each(value, function (v, k) { DOM.setStyle(node, k, v); });
						return;
				}

				node.style[key] = value;
			}
		}, DOM);
	})(document.head, document.body);

	/**
	 * Query查询器引擎，针对节点进行操作
	 * 精简自jQuery
	 */
	(function () {
		var toArray = function (v1, v2) {
				if (!v1.length && v1.length !== 0) v1 = [v1];

				if (v2) {
					jBD.each(v2, function (d, i) {
						v1[v1.length + i] = d;
					}, 0, false, true);
				}

				return v1;
			},
			toFill  = function (selector, context, origin) {
				var _this = this,
					_len  = _this.length || 0;

				_this.origin = origin || _this;
				_this.context = context === void(0) ? document : context;
				_this.length = 0;

				jBD.each(selector || [], function (d) {
					if (!d) return;

					_this[_this.length] = d;
					_this.length++;
				});

				while (_len-- > _this.length) delete _this[_this.length];

				return _this;
			};

		Query = jBD.TObject.extend({
			/**
			 * 初始化
			 *
			 * @public
			 * @param selector        {String/DOM/jQuery/Function}
			 * @param param            {Object}
			 * @param context        {String/DOM/jQuery}
			 * @return {jQuery}
			 */
			Create: function (selector, param, context) {
				var _this = this,
					_type, _temp;

				if (!selector) return toFill.call(_this);
				if (jBD.isFunction(selector)) return Query.Ready(selector);

				if (arguments.length == 2 && jBD.type(param, true) != "object") {
					context = param;
					param = null;
				}

				_type = [jBD.type(selector, true), jBD.type(param, true), jBD.type(context, true)];

				//解析selector，作为初过滤
				switch (_type[0]) {
					default:
						selector = [];
						break;
					case "dom":
						if (selector.nodeType == 1) selector = [selector];
						else selector = [];
						break;
					case "jquery":
						selector = toArray([], selector);
						break;
					case "array":
						selector = (_temp = selector) && [];
						jBD.each(_temp, function (d, t) {
							switch (t) {
								case "dom":
									selector.push(d);
									break;
								case "string":
									if ((d = new Query(d, param, context)).length > 0) toArray(selector, d);
									break;
								case "jquery":
									if (d.length > 0) toArray(selector, d);
									break;
							}
						}, true);
						break;
					case "string":
						//解析字符串，区分建立html和查询条件
						_temp = selector.trim();
						selector = [];

						if (_temp) {
							_temp = (_temp.length >= 3 && _temp[0] === "<" && _temp[_temp.length - 1] === ">")
								? [null, _temp, null] : /^(?:(<[\w\W]+>)|(#[A-Za-z][\w-]*))$/.exec(_temp);

							if (!_temp) {
								//复杂内容解析
								selector = DOM(selector);
							}
							else if (_temp[1]) {
								//html新建
								selector = DOM.createElement(_temp[1], param);
							}
							else if (_temp[2]) {
								//id查找
								if (!(selector = DOM.getID(_temp[2], context))) selector = [];
							}
						}

						break;
				}

				if (selector.length < 1) return toFill.call(_this);

				//解析context，作为父级区域
				switch (_type[2]) {
					case "string":
						context = DOM(context);
					case "jquery":
						if (context.length < 1) return toFill.call(_this);
						break;
					case "array":
						context = (_temp = context) && [];
						jBD.each(_temp, function (d, t) {
							switch (t) {
								case "dom":
									context.push(d);
									break;
								case "string":
									if ((d = DOM(d)).length > 0) toArray(context, d);
									break;
								case "jquery":
									if (d.length > 0) toArray(context, d);
									break;
							}
						}, true);
						break;
					case "dom":
						context = [context];
						break;
					default:
						return toFill.call(_this, selector);
						break;
				}

				return toFill.call(_this, DOM.getList(selector, context), context);
			},
			filter: function (selector) {
				var _this  = this,
					result = [];

				switch (jBD.type(selector, true)) {
					case "dom":
						selector = selector.nodeType == 1 ? [selector] : [];
					case "jquery":
						result = DOM.unique(_this, selector);
						break;
					case "string":
						result = DOM.unique(_this, DOM(selector));
						break;
					case "array":
						jBD.each(selector, function (d) { toArray(result, _this.filter(d)); });
						break;
				}

				toFill.call(_this, result, _this.context, _this.origin);

				return _this;
			},
			end:    function () {
				return this.origin;
			},
			each:   function (callback) {
				if (!jBD.isFunction(callback)) return;

				jBD.each(this, function (node, index) {
					if (callback.call(node, index, node) === false) return false;
				}, 0, false, true);
			}
		});

		/**
		 * 选择元素
		 *
		 * find    ＝ 向下查找
		 * parent    ＝ 向上查找
		 * next    ＝ 向后查找
		 * prev    ＝ 向前查找
		 *
		 */
		jBD.clone({
			find:     function (selector, context, deep) {
				var _this = this,
					result;

				if (arguments.length == 2 && jBD.isBool(context)) {
					deep = context;
					context = null;
				}

				if (!jBD.isBool(deep)) deep = true;

				if (deep) {
					result = new Query(context || selector, _this);
					if (result.length > 0 && context) result = result.find(selector, null, true);
				}
				else {
					result = new Query(DOM.child(_this));
					if (selector) result.filter(selector);
				}

				result.origin = _this;

				return result;
			},
			parent:   function (selector, deep) {
				var _this = this,
					result;

				if (arguments.length == 1 && jBD.isBool(selector)) {
					deep = selector;
					selector = null;
				}

				result = new Query(DOM.parent(_this, null, deep));

				if (selector) result.filter(selector);

				result.origin = _this;

				return result;
			},
			next:     function (selector, deep) {
				var _this = this,
					result;

				if (arguments.length == 1 && jBD.isBool(selector)) {
					deep = selector;
					selector = null;
				}

				result = new Query(DOM.next(_this, null, deep));

				if (selector) result.filter(selector);

				result.origin = _this;

				return result;
			},
			prev:     function (selector, deep) {
				var _this = this,
					result;

				if (arguments.length == 1 && jBD.isBool(selector)) {
					deep = selector;
					selector = null;
				}

				result = new Query(DOM.prev(_this, null, deep));

				if (selector) result.filter(selector);

				result.origin = _this;

				return result;
			},
			siblings: function (selector) {
				var _this  = this,
					result = toArray(DOM.prev(_this, null, false), DOM.next(_this, null, false));

				result = new Query(result);

				if (selector) result.filter(selector);

				result.origin = _this;

				return result;
			}

		}, Query.fn = Query.prototype);
	})();

	_$ = function (selector, param, context) {
		return new Query(selector, param, context);
	};

	_$.fn = Query.prototype;

	jBD.clone({
		/**
		 * 加载Flash
		 *
		 * @public
		 * @param url     {String}
		 * @param opt     {Object}
		 * @return {String}
		 */
		flash: function (url, opt) {
			var _opt = {
					width:  "100%",
					height: "100%",
					alpha:  true,
					fv:     []
				},
				_fmt = "<embed pluginspage=\"http://www.macromedia.com/go/getflashplayer\" type=\"application/x-shockwave-flash\" quality=\"high\" menu=\"false\" swLiveConnect=\"true\" src=\"{0}\" width=\"{1}\" height=\"{2}\" wmode=\"{3}\"{4}>请安装Flash插件</embed>",
				_fv  = "";

			if (jBD.isObject(opt)) {
				jBD.clone(opt, _opt, function (k, o, n, t) {
					switch (k) {
						case "width":
						case "height":
							return t == "number" || t == "string" ? n : o;
						case "alpha":
							return t == "boolean" ? n : o;
						case "fv":
							return t == "string" && n.indexOf("=") >= 0 ? n.split("|") : (t == "array" ? n : o);
					}
				});
			}
			if (_opt.fv.length > 0) _fv = jBD.String.Format(" flashvars=\"{0}\"", _opt.fv.join("|"));

			return jBD.isString(url) ? jBD.String.Format(_fmt, url, _opt.width, _opt.height, (_opt.alpha ? "transparent" : "opaque"), _fv) : "<embed>Url错误</embed>";
		},
		/**
		 * 打开新窗体
		 *
		 * @public
		 * @param url     {String}
		 * @param opt     {Object}
		 * @param data    {Object}
		 * @return {String}
		 */
		open:  function (url, opt, data) {
			url = jBD.isString(url) ? url : "";

			function getState (s) { return (jBD.isBool(s) ? s : false) ? "yes" : "no"; }

			var _wnd, _fmt,
				_opt = {
					mode:      "window",
					width:     1024, height: 768, left: 0, top: 0,
					status:    false,
					resizable: false, scrollbars: false
				};

			if (jBD.isObject(opt)) {
				jBD.clone(opt, _opt, function (k, o, n, t) {
					switch (k) {
						default:
							return n;
						case "mode":
							return t == "number" || t == "string" ? n : o;
						case "width":
						case "height":
						case "left":
						case "top":
							return t == "number" && !isNaN(n) ? n : o;
						case "status":
						case "resizable":
						case "scrollbars":
							return t == "boolean" ? n : o;
					}
				});
			}

			switch (_opt.mode) {
				case "window":
				case 1:
					data = jBD.isString(data) ? data : "";

					_opt = jBD.clone(_opt,
						{
							title:    "",
							zlook:    true, depended: true,
							location: false, menubar: false, titlebar: false, toolbar: false
						},
						function (k, o, n, t) {
							switch (k) {
								default:
									return n;
								case "title":
									return t == "string" ? n : o;
								case "zlook":
								case "depended":
								case "location":
								case "menubar":
								case "titlebar":
								case "toolbar":
									return t == "boolean" ? n : o;
							}
						}
					);

					_fmt = jBD.String.Format(
						"width={0},height={1},left={2},top={3},resizable={4},z-look={5},depended={6},location={7},menubar={8},titlebar={9},toolbar={10},scrollbars={11},status={12}",
						_opt.width, _opt.height, _opt.left, _opt.top,
						getState(_opt.resizable), getState(_opt.zlook), getState(_opt.depended),
						getState(_opt.location), getState(_opt.menubar), getState(_opt.titlebar),
						getState(_opt.toolbar), getState(_opt.scrollbars), getState(_opt.status)
					);

					_wnd = window.open(url, data, _fmt);
					if (url == "") {
						_wnd.document.writeln("<html>");
						_wnd.document.writeln("<head>");
						_wnd.document.writeln("<title>" + _opt.title + "</title>");
						_wnd.document.writeln("</head>");
						_wnd.document.writeln("<body></body>");
						_wnd.document.writeln("</html>");
						_wnd.document.close();
					}
					break;
				case "module":
				case 2:
					data = jBD.isNull(data, true) ? null : data;

					_opt = jBD.clone(_opt,
						{
							center: true, help: false
						},
						function (k, o, n, t) {
							switch (k) {
								default:
									return n;
								case "center":
								case "help":
									return t == "boolean" ? n : o;
							}
						}
					);

					_fmt = jBD.String.Format(
						"dialogWidth={0}px;dialogHeight={1}px;dialogLeft={2}px;dialogTop={3}px;center={4};resizable={5};help={6};scroll={7};status={8}",
						_opt.width, _opt.height, _opt.left, _opt.top, getState(_opt.center),
						getState(_opt.resizable), getState(_opt.help), getState(_opt.scrollbars), getState(_opt.status)
					);

					_wnd = window.showModalDialog(url, data, _fmt);
					break;
			}

			return _wnd;
		},
		/**
		 * DOM载入
		 *
		 * @public
		 * @param callback     {Function}
		 */
		ready: jBD.ready
	}, _$);

	// if (!$) this.$ = _$;

	return _$;
}, {module: module, exports: this}, ["String", "Conver"], "DOM");