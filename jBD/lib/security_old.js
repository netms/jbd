/**
 * ==========================================
 * Name:           Security
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    加解密
 * Log
 * 2015-06-09    优化模块结构
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	let api;

	/**
	 * 安全算法
	 *
	 * @namespace Security
	 */
	api = {
		/**
		 * Base64编解码
		 *
		 * @public
		 * @param {string} value
		 * @param {boolean} [enc=true] 是否编码
		 * @returns {string}
		 */
		Base64: function (value, enc = true) {
			const en = value=> {
					  var char   = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
						  result = "";

					  for (let i = 0, l = value.length, c1, c2, c3; i < l;) {
						  c1 = value.charCodeAt(i++) & 0xff;
						  if (i == l) {
							  result += char.charAt(c1 >> 2);
							  result += char.charAt((c1 & 0x3) << 4);
							  result += "==";
							  break;
						  }

						  c2 = value.charCodeAt(i++);
						  if (i == l) {
							  result += char.charAt(c1 >> 2);
							  result += char.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
							  result += char.charAt((c2 & 0xf) << 2);
							  result += "=";
							  break;
						  }

						  c3 = value.charCodeAt(i++);
						  result += char.charAt(c1 >> 2);
						  result += char.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
						  result += char.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
						  result += char.charAt(c3 & 0x3f);
					  }

					  return result;
				  },
				  de = value=> {
					  let char   = [
							  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
							  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
							  -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
							  52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
							  -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
							  15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
							  -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
							  41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1
						  ],
						  result = "";

					  for (let i = 0, l = value.length, c1, c2, c3, c4; i < l;) {
						  do c1 = char[value.charCodeAt(i++) & 0xff];
						  while (i < l && c1 == -1);
						  if (c1 == -1) break;

						  do c2 = char[value.charCodeAt(i++) & 0xff];
						  while (i < l && c2 == -1);
						  if (c2 == -1) break;
						  result += String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));

						  do {
							  c3 = value.charCodeAt(i++) & 0xff;
							  if (c3 == 61) return result;
							  c3 = char[c3];
						  }
						  while (i < l && c3 == -1);
						  if (c3 == -1) break;
						  result += String.fromCharCode(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2));

						  do {
							  c4 = value.charCodeAt(i++) & 0xff;
							  if (c4 == 61) return result;
							  c4 = char[c4];
						  }
						  while (i < l && c4 == -1);
						  if (c4 == -1) break;
						  result += String.fromCharCode(((c3 & 0x03) << 6) | c4);
					  }

					  return result;
				  };

			value = String(value);
			return enc ? en(jBD.Conver.toUTF8(value)) : jBD.Conver.toUTF16(de(value));
		},
		/**
		 * MD5编码
		 *
		 * @param {string} value
		 * @param {boolean} [simple=false] 是否获取16位MD5
		 * @param {function} [callback]
		 * @returns {string}
		 */
		MD5:    (value, simple = false, callback = simple)=> {
			const RotateLeft   = (v, bit)=> (v << bit) | (v >>> (32 - bit)),
				  toUnsigned   = (x, y)=> {
					  let x4 = x & 0x40000000,
						  y4 = y & 0x40000000,
						  x8 = x & 0x80000000,
						  y8 = y & 0x80000000,
						  r  = (x & 0x3FFFFFFF) + (y & 0x3FFFFFFF);

					  if (x4 & y4) return r ^ 0x80000000 ^ x8 ^ y8;
					  else if (x4 | y4) return r ^ (r & 0x40000000 ? 0xC0000000 : 0x40000000) ^ x8 ^ y8;
					  else return r ^ x8 ^ y8;
				  },
				  toWordArray  = value=> {
					  let ml    = value.length,
						  lw_t1 = ml + 8,
						  lw_t2 = (lw_t1 - (lw_t1 % 64)) / 64,
						  nw    = (lw_t2 + 1) * 16,
						  bp    = 0,
						  bc    = 0,
						  r     = new Array(nw - 1),
						  wc;

					  while (bc < ml) {
						  wc = (bc - (bc % 4)) / 4;
						  bp = (bc % 4) * 8;
						  r[wc] = (r[wc] | (value.charCodeAt(bc) << bp));
						  bc++;
					  }

					  wc = (bc - (bc % 4)) / 4;
					  bp = (bc % 4) * 8;
					  r[wc] = r[wc] | (0x80 << bp);
					  r[nw - 2] = ml << 3;
					  r[nw - 1] = ml >>> 29;

					  return r;
				  },
				  toHex        = value=> {
					  let r = "";

					  for (let i = 0, s = "", b; i <= 3; i++) {
						  b = (value >>> (i * 8)) & 255;
						  s = "0" + b.toString(16);
						  r = r + s.substr(s.length - 2, 2);
					  }

					  return r;
				  },
				  toUTF8Encode = value=> {
					  value = value.replace(/\r\n/g, "\n");

					  let r = "";

					  for (let i = 0, c; i < value.length; i++) {
						  c = value.charCodeAt(i);

						  if (c < 128) r += String.fromCharCode(c);
						  else if ((c > 127) && (c < 2048)) {
							  r += String.fromCharCode((c >> 6) | 192);
							  r += String.fromCharCode((c & 63) | 128);
						  }
						  else {
							  r += String.fromCharCode((c >> 12) | 224);
							  r += String.fromCharCode(((c >> 6) & 63) | 128);
							  r += String.fromCharCode((c & 63) | 128);
						  }
					  }

					  return r;
				  },
				  toF          = (x, y, z)=> (x & y) | (~x & z),
				  toG          = (x, y, z)=> (x & z) | (y & ~z),
				  toH          = (x, y, z)=> x ^ y ^ z,
				  toI          = (x, y, z)=> y ^ (x | ~z),
				  toFF         = (a, b, c, d, x, s, ac)=> toUnsigned(RotateLeft(toUnsigned(a, toUnsigned(toUnsigned(toF(b, c, d), x), ac)), s), b),
				  toGG         = (a, b, c, d, x, s, ac)=> toUnsigned(RotateLeft(toUnsigned(a, toUnsigned(toUnsigned(toG(b, c, d), x), ac)), s), b),
				  toHH         = (a, b, c, d, x, s, ac)=> toUnsigned(RotateLeft(toUnsigned(a, toUnsigned(toUnsigned(toH(b, c, d), x), ac)), s), b),
				  toII         = (a, b, c, d, x, s, ac)=> toUnsigned(RotateLeft(toUnsigned(a, toUnsigned(toUnsigned(toI(b, c, d), x), ac)), s), b);

			value = (value || "").toString();

			let a      = 0x67452301,
				b      = 0xEFCDAB89,
				c      = 0x98BADCFE,
				d      = 0x10325476,
				S      = [
					[0, 0, 0, 0, 0],
					[0, 7, 12, 17, 22],
					[0, 5, 9, 14, 20],
					[0, 4, 11, 16, 23],
					[0, 6, 10, 15, 21]
				],
				D      = toWordArray(toUTF8Encode(value)),
				result = "";

			for (let i = 0; i < D.length; i += 16) {
				S[0][1] = a;
				S[0][2] = b;
				S[0][3] = c;
				S[0][4] = d;

				a = toFF(a, b, c, d, D[i], S[1][1], 0xD76AA478);
				d = toFF(d, a, b, c, D[i + 1], S[1][2], 0xE8C7B756);
				c = toFF(c, d, a, b, D[i + 2], S[1][3], 0x242070DB);
				b = toFF(b, c, d, a, D[i + 3], S[1][4], 0xC1BDCEEE);
				a = toFF(a, b, c, d, D[i + 4], S[1][1], 0xF57C0FAF);
				d = toFF(d, a, b, c, D[i + 5], S[1][2], 0x4787C62A);
				c = toFF(c, d, a, b, D[i + 6], S[1][3], 0xA8304613);
				b = toFF(b, c, d, a, D[i + 7], S[1][4], 0xFD469501);
				a = toFF(a, b, c, d, D[i + 8], S[1][1], 0x698098D8);
				d = toFF(d, a, b, c, D[i + 9], S[1][2], 0x8B44F7AF);
				c = toFF(c, d, a, b, D[i + 10], S[1][3], 0xFFFF5BB1);
				b = toFF(b, c, d, a, D[i + 11], S[1][4], 0x895CD7BE);
				a = toFF(a, b, c, d, D[i + 12], S[1][1], 0x6B901122);
				d = toFF(d, a, b, c, D[i + 13], S[1][2], 0xFD987193);
				c = toFF(c, d, a, b, D[i + 14], S[1][3], 0xA679438E);
				b = toFF(b, c, d, a, D[i + 15], S[1][4], 0x49B40821);
				a = toGG(a, b, c, d, D[i + 1], S[2][1], 0xF61E2562);
				d = toGG(d, a, b, c, D[i + 6], S[2][2], 0xC040B340);
				c = toGG(c, d, a, b, D[i + 11], S[2][3], 0x265E5A51);
				b = toGG(b, c, d, a, D[i], S[2][4], 0xE9B6C7AA);
				a = toGG(a, b, c, d, D[i + 5], S[2][1], 0xD62F105D);
				d = toGG(d, a, b, c, D[i + 10], S[2][2], 0x2441453);
				c = toGG(c, d, a, b, D[i + 15], S[2][3], 0xD8A1E681);
				b = toGG(b, c, d, a, D[i + 4], S[2][4], 0xE7D3FBC8);
				a = toGG(a, b, c, d, D[i + 9], S[2][1], 0x21E1CDE6);
				d = toGG(d, a, b, c, D[i + 14], S[2][2], 0xC33707D6);
				c = toGG(c, d, a, b, D[i + 3], S[2][3], 0xF4D50D87);
				b = toGG(b, c, d, a, D[i + 8], S[2][4], 0x455A14ED);
				a = toGG(a, b, c, d, D[i + 13], S[2][1], 0xA9E3E905);
				d = toGG(d, a, b, c, D[i + 2], S[2][2], 0xFCEFA3F8);
				c = toGG(c, d, a, b, D[i + 7], S[2][3], 0x676F02D9);
				b = toGG(b, c, d, a, D[i + 12], S[2][4], 0x8D2A4C8A);
				a = toHH(a, b, c, d, D[i + 5], S[3][1], 0xFFFA3942);
				d = toHH(d, a, b, c, D[i + 8], S[3][2], 0x8771F681);
				c = toHH(c, d, a, b, D[i + 11], S[3][3], 0x6D9D6122);
				b = toHH(b, c, d, a, D[i + 14], S[3][4], 0xFDE5380C);
				a = toHH(a, b, c, d, D[i + 1], S[3][1], 0xA4BEEA44);
				d = toHH(d, a, b, c, D[i + 4], S[3][2], 0x4BDECFA9);
				c = toHH(c, d, a, b, D[i + 7], S[3][3], 0xF6BB4B60);
				b = toHH(b, c, d, a, D[i + 10], S[3][4], 0xBEBFBC70);
				a = toHH(a, b, c, d, D[i + 13], S[3][1], 0x289B7EC6);
				d = toHH(d, a, b, c, D[i], S[3][2], 0xEAA127FA);
				c = toHH(c, d, a, b, D[i + 3], S[3][3], 0xD4EF3085);
				b = toHH(b, c, d, a, D[i + 6], S[3][4], 0x4881D05);
				a = toHH(a, b, c, d, D[i + 9], S[3][1], 0xD9D4D039);
				d = toHH(d, a, b, c, D[i + 12], S[3][2], 0xE6DB99E5);
				c = toHH(c, d, a, b, D[i + 15], S[3][3], 0x1FA27CF8);
				b = toHH(b, c, d, a, D[i + 2], S[3][4], 0xC4AC5665);
				a = toII(a, b, c, d, D[i], S[4][1], 0xF4292244);
				d = toII(d, a, b, c, D[i + 7], S[4][2], 0x432AFF97);
				c = toII(c, d, a, b, D[i + 14], S[4][3], 0xAB9423A7);
				b = toII(b, c, d, a, D[i + 5], S[4][4], 0xFC93A039);
				a = toII(a, b, c, d, D[i + 12], S[4][1], 0x655B59C3);
				d = toII(d, a, b, c, D[i + 3], S[4][2], 0x8F0CCC92);
				c = toII(c, d, a, b, D[i + 10], S[4][3], 0xFFEFF47D);
				b = toII(b, c, d, a, D[i + 1], S[4][4], 0x85845DD1);
				a = toII(a, b, c, d, D[i + 8], S[4][1], 0x6FA87E4F);
				d = toII(d, a, b, c, D[i + 15], S[4][2], 0xFE2CE6E0);
				c = toII(c, d, a, b, D[i + 6], S[4][3], 0xA3014314);
				b = toII(b, c, d, a, D[i + 13], S[4][4], 0x4E0811A1);
				a = toII(a, b, c, d, D[i + 4], S[4][1], 0xF7537E82);
				d = toII(d, a, b, c, D[i + 11], S[4][2], 0xBD3AF235);
				c = toII(c, d, a, b, D[i + 2], S[4][3], 0x2AD7D2BB);
				b = toII(b, c, d, a, D[i + 9], S[4][4], 0xEB86D391);

				a = toUnsigned(a, S[0][1]);
				b = toUnsigned(b, S[0][2]);
				c = toUnsigned(c, S[0][3]);
				d = toUnsigned(d, S[0][4]);
			}

			result = (toHex(a) + toHex(b) + toHex(c) + toHex(d));
			result = simple === true ? result.substr(8, 16) : result;

			return jBD.isFunction(callback) ? callback(result, value) : result;
		},
		/**
		 * SHA编解码
		 *
		 * @param {string} value
		 * @param {number|object} [opt] 参数
		 *    @param {string} [opt.format=TEXT] 编码方式
		 *    @param {string} [opt.encoding=UTF8] 文本编码
		 *    @param {number} [opt.level=256] 编码等级
		 *    @param {string} [opt.key] 编码key
		 * @param {function} [callback]
		 * @returns {string}
		 */
		SHA:    function (value, opt = {}, callback = opt) {
			const int_64           = (msint_32, lsint_32)=> {
					  this.highOrder = msint_32;
					  this.lowOrder = lsint_32;
				  },
				  rotl_32          = (x, n)=> (x << n) | (x >>> (32 - n)),
				  rotr_32          = (x, n)=> (x >>> n) | (x << (32 - n)),
				  rotr_64          = (x, n)=> {
					  let tmp = new int_64(x.highOrder, x.lowOrder);

					  if (n <= 32) {
						  return new int_64(
							  (tmp.highOrder >>> n) | ((tmp.lowOrder << (32 - n)) & 0xFFFFFFFF),
							  (tmp.lowOrder >>> n) | ((tmp.highOrder << (32 - n)) & 0xFFFFFFFF)
						  );
					  }
					  else {
						  return new int_64(
							  (tmp.lowOrder >>> (n - 32)) | ((tmp.highOrder << (64 - n)) & 0xFFFFFFFF),
							  (tmp.highOrder >>> (n - 32)) | ((tmp.lowOrder << (64 - n)) & 0xFFFFFFFF)
						  );
					  }
				  },
				  shr_32           = (x, n)=> x >>> n,
				  shr_64           = (x, n)=> {
					  if (n <= 32) {
						  return new int_64(
							  x.highOrder >>> n,
							  x.lowOrder >>> n | ((x.highOrder << (32 - n)) & 0xFFFFFFFF)
						  );
					  }
					  else {
						  return new int_64(
							  0,
							  x.highOrder >>> (n - 32)
						  );
					  }
				  },
				  parity_32        = (x, y, z)=> x ^ y ^ z,
				  ch_32            = (x, y, z)=> (x & y) ^ (~x & z),
				  ch_64            = (x, y, z)=> new int_64((x.highOrder & y.highOrder) ^ (~x.highOrder & z.highOrder), (x.lowOrder & y.lowOrder) ^ (~x.lowOrder & z.lowOrder)),
				  maj_32           = (x, y, z)=> (x & y) ^ (x & z) ^ (y & z),
				  maj_64           = (x, y, z)=> {
					  return new int_64(
						  (x.highOrder & y.highOrder) ^
						  (x.highOrder & z.highOrder) ^
						  (y.highOrder & z.highOrder),
						  (x.lowOrder & y.lowOrder) ^
						  (x.lowOrder & z.lowOrder) ^
						  (y.lowOrder & z.lowOrder)
					  );
				  },
				  sigma0_32        = (x)=> rotr_32(x, 2) ^ rotr_32(x, 13) ^ rotr_32(x, 22),
				  sigma0_64        = (x)=> {
					  let rotr28 = rotr_64(x, 28),
						  rotr34 = rotr_64(x, 34),
						  rotr39 = rotr_64(x, 39);

					  return new int_64(
						  rotr28.highOrder ^ rotr34.highOrder ^ rotr39.highOrder,
						  rotr28.lowOrder ^ rotr34.lowOrder ^ rotr39.lowOrder);
				  },
				  sigma1_32        = (x)=> rotr_32(x, 6) ^ rotr_32(x, 11) ^ rotr_32(x, 25),
				  sigma1_64        = (x)=> {
					  let rotr14 = rotr_64(x, 14),
						  rotr18 = rotr_64(x, 18),
						  rotr41 = rotr_64(x, 41);

					  return new int_64(
						  rotr14.highOrder ^ rotr18.highOrder ^ rotr41.highOrder,
						  rotr14.lowOrder ^ rotr18.lowOrder ^ rotr41.lowOrder);
				  },
				  gamma0_32        = (x)=> rotr_32(x, 7) ^ rotr_32(x, 18) ^ shr_32(x, 3),
				  gamma0_64        = (x)=> {
					  let rotr1 = rotr_64(x, 1),
						  rotr8 = rotr_64(x, 8),
						  shr7  = shr_64(x, 7);

					  return new int_64(
						  rotr1.highOrder ^ rotr8.highOrder ^ shr7.highOrder,
						  rotr1.lowOrder ^ rotr8.lowOrder ^ shr7.lowOrder
					  );
				  },
				  gamma1_32        = (x)=> rotr_32(x, 17) ^ rotr_32(x, 19) ^ shr_32(x, 10),
				  gamma1_64        = (x)=> {
					  let rotr19 = rotr_64(x, 19),
						  rotr61 = rotr_64(x, 61),
						  shr6   = shr_64(x, 6);

					  return new int_64(
						  rotr19.highOrder ^ rotr61.highOrder ^ shr6.highOrder,
						  rotr19.lowOrder ^ rotr61.lowOrder ^ shr6.lowOrder
					  );
				  },
				  safeAdd_32_2     = (a, b)=> {
					  let lsw = (a & 0xFFFF) + (b & 0xFFFF),
						  msw = (a >>> 16) + (b >>> 16) + (lsw >>> 16);

					  return ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);
				  },
				  safeAdd_32_4     = (a, b, c, d)=> {
					  let lsw = (a & 0xFFFF) + (b & 0xFFFF) + (c & 0xFFFF) + (d & 0xFFFF),
						  msw = (a >>> 16) + (b >>> 16) + (c >>> 16) + (d >>> 16) + (lsw >>> 16);

					  return ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);
				  },
				  safeAdd_32_5     = (a, b, c, d, e)=> {
					  let lsw = (a & 0xFFFF) + (b & 0xFFFF) + (c & 0xFFFF) + (d & 0xFFFF) + (e & 0xFFFF),
						  msw = (a >>> 16) + (b >>> 16) + (c >>> 16) + (d >>> 16) + (e >>> 16) + (lsw >>> 16);

					  return ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);
				  },
				  safeAdd_64_2     = (x, y) => {
					  let lsw      = (x.lowOrder & 0xFFFF) + (y.lowOrder & 0xFFFF),
						  msw      = (x.lowOrder >>> 16) + (y.lowOrder >>> 16) + (lsw >>> 16),
						  lowOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF),
						  highOrder;

					  lsw = (x.highOrder & 0xFFFF) + (y.highOrder & 0xFFFF) + (msw >>> 16);
					  msw = (x.highOrder >>> 16) + (y.highOrder >>> 16) + (lsw >>> 16);
					  highOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);

					  return new int_64(highOrder, lowOrder);
				  },
				  safeAdd_64_4     = (a, b, c, d)=> {
					  let lsw      = (a.lowOrder & 0xFFFF) + (b.lowOrder & 0xFFFF) + (c.lowOrder & 0xFFFF) + (d.lowOrder & 0xFFFF),
						  msw      = (a.lowOrder >>> 16) + (b.lowOrder >>> 16) + (c.lowOrder >>> 16) + (d.lowOrder >>> 16) + (lsw >>> 16),
						  lowOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF),
						  highOrder;

					  lsw = (a.highOrder & 0xFFFF) + (b.highOrder & 0xFFFF) + (c.highOrder & 0xFFFF) + (d.highOrder & 0xFFFF) + (msw >>> 16);
					  msw = (a.highOrder >>> 16) + (b.highOrder >>> 16) + (c.highOrder >>> 16) + (d.highOrder >>> 16) + (lsw >>> 16);
					  highOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);

					  return new int_64(highOrder, lowOrder);
				  },
				  safeAdd_64_5     = (a, b, c, d, e)=> {
					  let lsw      = (a.lowOrder & 0xFFFF) + (b.lowOrder & 0xFFFF) + (c.lowOrder & 0xFFFF) + (d.lowOrder & 0xFFFF) + (e.lowOrder & 0xFFFF),
						  msw      = (a.lowOrder >>> 16) + (b.lowOrder >>> 16) + (c.lowOrder >>> 16) + (d.lowOrder >>> 16) + (e.lowOrder >>> 16) + (lsw >>> 16),
						  lowOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF),
						  highOrder;

					  lsw = (a.highOrder & 0xFFFF) + (b.highOrder & 0xFFFF) +
						  (c.highOrder & 0xFFFF) + (d.highOrder & 0xFFFF) +
						  (e.highOrder & 0xFFFF) + (msw >>> 16);
					  msw = (a.highOrder >>> 16) + (b.highOrder >>> 16) +
						  (c.highOrder >>> 16) + (d.highOrder >>> 16) +
						  (e.highOrder >>> 16) + (lsw >>> 16);
					  highOrder = ((msw & 0xFFFF) << 16) | (lsw & 0xFFFF);

					  return new int_64(highOrder, lowOrder);
				  },
				  //E
				  str2binb         = (str, buf, len, utfType = encoding)=> {
					  var binArr = [], byteCnt = 0,
						  c, i, j, l,
						  intOffset, byteOffset;

					  utfType = utfType || encoding;

					  buf = buf || [0];
					  len = len || 0;
					  l = len >>> 3;

					  if (utfType == "UTF8") {
						  for (i = 0; i < str.length; i++) {
							  c = str.charCodeAt(i);
							  binArr = [];

							  if (0x80 > c) binArr.push(c);
							  else if (0x800 > c) {
								  binArr.push(0xC0 | (c >>> 6));
								  binArr.push(0x80 | (c & 0x3F));
							  }
							  else if ((0xd800 > c) || (0xe000 <= c)) {
								  binArr.push(
									  0xe0 | (c >>> 12),
									  0x80 | ((c >>> 6) & 0x3f),
									  0x80 | (c & 0x3f)
								  );
							  }
							  else {
								  i++;
								  c = 0x10000 + (((c & 0x3ff) << 10) | (str.charCodeAt(i) & 0x3ff));
								  binArr.push(
									  0xf0 | (c >>> 18),
									  0x80 | ((c >>> 12) & 0x3f),
									  0x80 | ((c >>> 6) & 0x3f),
									  0x80 | (c & 0x3f)
								  );
							  }

							  for (j = 0; j < binArr.length; j++) {
								  byteOffset = byteCnt + l;
								  intOffset = byteOffset >>> 2;
								  while (buf.length <= intOffset) buf.push(0);

								  buf[intOffset] |= binArr[j] << (8 * (3 - (byteOffset % 4)));
								  byteCnt++;
							  }
						  }
					  }
					  else {
						  for (i = 0; i < str.length; i++) {
							  c = str.charCodeAt(i);

							  if (utfType == "UTF16LE") {
								  j = c & 0xFF;
								  c = (j << 8) | (c >>> 8);
							  }

							  byteOffset = byteCnt + l;
							  intOffset = byteOffset >>> 2;
							  while (buf.length <= intOffset) buf.push(0);

							  buf[intOffset] |= c << (8 * (2 - (byteOffset % 4)));
							  byteCnt += 2;
						  }
					  }

					  return {value: buf, length: byteCnt * 8 + len};
				  },
				  hex2binb         = (str, buf, len) => {
					  var n = str.length,
						  i, j, l,
						  intOffset, byteOffset;

					  buf = buf || [0];
					  len = len || 0;
					  l = len >>> 3;

					  if (0 !== (n % 2)) {
						  throw new Error("String of HEX type must be in byte increments");
					  }

					  for (i = 0; i < n; i += 2) {
						  j = parseInt(str.substr(i, 2), 16);
						  if (!isNaN(j)) {
							  byteOffset = (i >>> 1) + l;
							  intOffset = byteOffset >>> 2;
							  while (buf.length <= intOffset) {
								  buf.push(0);
							  }
							  buf[intOffset] |= j << 8 * (3 - (byteOffset % 4));
						  }
						  else {
							  throw new Error("String of HEX type contains invalid characters");
						  }
					  }

					  return {value: buf, length: n * 4 + len};
				  },
				  bytes2binb       = (str, buf, len)=> {
					  var c, i, l,
						  intOffset, byteOffset;

					  buf = buf || [0];
					  len = len || 0;
					  l = len >>> 3;

					  for (i = 0; i < str.length; i++) {
						  c = str.charCodeAt(i);

						  byteOffset = i + l;
						  intOffset = byteOffset >>> 2;
						  if (buf.length <= intOffset) buf.push(0);

						  buf[intOffset] |= c << 8 * (3 - (byteOffset % 4));
					  }

					  return {value: buf, length: str.length * 8 + len};
				  },
				  b642binb         = (str, buf, len) => {
					  var c      = 0,
						  b64Tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
						  n, i, j, l,
						  tmpInt, strPart, firstEqual,
						  intOffset, byteOffset;

					  buf = buf || [0];
					  len = len || 0;
					  l = len >>> 3;

					  if (-1 === str.search(/^[a-zA-Z0-9=+\/]+$/)) {
						  throw new Error("Invalid character in base-64 string");
					  }
					  firstEqual = str.indexOf('=');
					  str = str.replace(/\=/g, '');
					  if ((-1 !== firstEqual) && (firstEqual < str.length)) {
						  throw new Error("Invalid '=' found in base-64 string");
					  }

					  for (i = 0; i < str.length; i += 4) {
						  strPart = str.substr(i, 4);
						  tmpInt = 0;

						  for (j = 0; j < strPart.length; j++) {
							  n = b64Tab.indexOf(strPart[j]);
							  tmpInt |= n << (18 - (6 * j));
						  }

						  for (j = 0; j < strPart.length - 1; j++) {
							  byteOffset = c + l;
							  intOffset = byteOffset >>> 2;
							  while (buf.length <= intOffset) buf.push(0);

							  buf[intOffset] |= ((tmpInt >>> (16 - (j * 8))) & 0xFF) <<
								  8 * (3 - (byteOffset % 4));
							  c++;
						  }
					  }

					  return {value: buf, length: c * 8 + len};
				  },
				  binb2hex         = (data) => {
					  var result = "",
						  hex    = "0123456789abcdef",
						  l      = data.length * 4,
						  i, s;

					  for (i = 0; i < l; i++) {
						  s = data[i >>> 2] >>> ((3 - (i % 4)) * 8);
						  result += hex.charAt((s >>> 4) & 0xF) +
							  hex.charAt(s & 0xF);
					  }

					  return result;
				  },
				  binb2b64         = (data) => {
					  var result = "",
						  b64    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
						  l      = data.length * 4, i, j,
						  triplet, offset, int1, int2;

					  for (i = 0; i < l; i += 3) {
						  offset = (i + 1) >>> 2;
						  int1 = (data.length <= offset) ? 0 : data[offset];
						  offset = (i + 2) >>> 2;
						  int2 = (data.length <= offset) ? 0 : data[offset];
						  triplet = (((data[i >>> 2] >>> 8 * (3 - i % 4)) & 0xFF) << 16) |
							  (((int1 >>> 8 * (3 - (i + 1) % 4)) & 0xFF) << 8) |
							  ((int2 >>> 8 * (3 - (i + 2) % 4)) & 0xFF);
						  for (j = 0; j < 4; j++) {
							  if (i * 8 + j * 6 <= data.length * 32) {
								  result += b64.charAt((triplet >>> 6 * (3 - j)) & 0x3F);
							  }
							  else result += "=";
						  }
					  }
					  return result;
				  },
				  binb2bytes       = (data) => {
					  var result = "", l = data.length * 4, i, s;

					  for (i = 0; i < l; i++) {
						  s = (data[i >>> 2] >>> ((3 - (i % 4)) * 8)) & 0xFF;
						  result += String.fromCharCode(s);
					  }

					  return result;
				  },
				  getH             = level=> {
					  const H_trunc = [
								0xc1059ed8, 0x367cd507, 0x3070dd17, 0xf70e5939,
								0xffc00b31, 0x68581511, 0x64f98fa7, 0xbefa4fa4
							],
							H_full  = [
								0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
								0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
							];

					  switch (level) {
						  case 1:
							  return [
								  0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0
							  ];
						  case 224:
							  return H_trunc;
						  default:
						  case 256:
							  return H_full;
						  case 384:
							  return [
								  new int_64(0xcbbb9d5d, H_trunc[0]),
								  new int_64(0x0629a292a, H_trunc[1]),
								  new int_64(0x9159015a, H_trunc[2]),
								  new int_64(0x0152fecd8, H_trunc[3]),
								  new int_64(0x67332667, H_trunc[4]),
								  new int_64(0x98eb44a87, H_trunc[5]),
								  new int_64(0xdb0c2e0d, H_trunc[6]),
								  new int_64(0x047b5481d, H_trunc[7])
							  ];
						  case 512:
							  return [
								  new int_64(H_full[0], 0xf3bcc908),
								  new int_64(H_full[1], 0x84caa73b),
								  new int_64(H_full[2], 0xfe94f82b),
								  new int_64(H_full[3], 0x5f1d36f1),
								  new int_64(H_full[4], 0xade682d1),
								  new int_64(H_full[5], 0x2b3e6c1f),
								  new int_64(H_full[6], 0xfb41bd6b),
								  new int_64(H_full[7], 0x137e2179)
							  ];
					  }
				  },
				  getK             = high=> {
					  const result = [
						  0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
						  0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
						  0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
						  0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
						  0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
						  0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
						  0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
						  0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
						  0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
						  0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
						  0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
						  0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
						  0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
						  0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
						  0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
						  0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
					  ];

					  if (high !== true) return result;

					  return [
						  new int_64(result[0], 0xd728ae22), new int_64(result[1], 0x23ef65cd),
						  new int_64(result[2], 0xec4d3b2f), new int_64(result[3], 0x8189dbbc),
						  new int_64(result[4], 0xf348b538), new int_64(result[5], 0xb605d019),
						  new int_64(result[6], 0xaf194f9b), new int_64(result[7], 0xda6d8118),
						  new int_64(result[8], 0xa3030242), new int_64(result[9], 0x45706fbe),
						  new int_64(result[10], 0x4ee4b28c), new int_64(result[11], 0xd5ffb4e2),
						  new int_64(result[12], 0xf27b896f), new int_64(result[13], 0x3b1696b1),
						  new int_64(result[14], 0x25c71235), new int_64(result[15], 0xcf692694),
						  new int_64(result[16], 0x9ef14ad2), new int_64(result[17], 0x384f25e3),
						  new int_64(result[18], 0x8b8cd5b5), new int_64(result[19], 0x77ac9c65),
						  new int_64(result[20], 0x592b0275), new int_64(result[21], 0x6ea6e483),
						  new int_64(result[22], 0xbd41fbd4), new int_64(result[23], 0x831153b5),
						  new int_64(result[24], 0xee66dfab), new int_64(result[25], 0x2db43210),
						  new int_64(result[26], 0x98fb213f), new int_64(result[27], 0xbeef0ee4),
						  new int_64(result[28], 0x3da88fc2), new int_64(result[29], 0x930aa725),
						  new int_64(result[30], 0xe003826f), new int_64(result[31], 0x0a0e6e70),
						  new int_64(result[32], 0x46d22ffc), new int_64(result[33], 0x5c26c926),
						  new int_64(result[34], 0x5ac42aed), new int_64(result[35], 0x9d95b3df),
						  new int_64(result[36], 0x8baf63de), new int_64(result[37], 0x3c77b2a8),
						  new int_64(result[38], 0x47edaee6), new int_64(result[39], 0x1482353b),
						  new int_64(result[40], 0x4cf10364), new int_64(result[41], 0xbc423001),
						  new int_64(result[42], 0xd0f89791), new int_64(result[43], 0x0654be30),
						  new int_64(result[44], 0xd6ef5218), new int_64(result[45], 0x5565a910),
						  new int_64(result[46], 0x5771202a), new int_64(result[47], 0x32bbd1b8),
						  new int_64(result[48], 0xb8d2d0c8), new int_64(result[49], 0x5141ab53),
						  new int_64(result[50], 0xdf8eeb99), new int_64(result[51], 0xe19b48a8),
						  new int_64(result[52], 0xc5c95a63), new int_64(result[53], 0xe3418acb),
						  new int_64(result[54], 0x7763e373), new int_64(result[55], 0xd6b2b8a3),
						  new int_64(result[56], 0x5defb2fc), new int_64(result[57], 0x43172f60),
						  new int_64(result[58], 0xa1f0ab72), new int_64(result[59], 0x1a6439ec),
						  new int_64(result[60], 0x23631e28), new int_64(result[61], 0xde82bde9),
						  new int_64(result[62], 0xb2c67915), new int_64(result[63], 0xe372532b),
						  new int_64(0xca273ece, 0xea26619c), new int_64(0xd186b8c7, 0x21c0c207),
						  new int_64(0xeada7dd6, 0xcde0eb1e), new int_64(0xf57d4f7f, 0xee6ed178),
						  new int_64(0x06f067aa, 0x72176fba), new int_64(0x0a637dc5, 0xa2c898a6),
						  new int_64(0x113f9804, 0xbef90dae), new int_64(0x1b710b35, 0x131c471b),
						  new int_64(0x28db77f5, 0x23047d84), new int_64(0x32caab7b, 0x40c72493),
						  new int_64(0x3c9ebe0a, 0x15c9bebc), new int_64(0x431d67c4, 0x9c100d4c),
						  new int_64(0x4cc5d4be, 0xcb3e42b6), new int_64(0x597f299c, 0xfc657e2a),
						  new int_64(0x5fcb6fab, 0x3ad6faec), new int_64(0x6c44198c, 0x4a475817)
					  ];
				  },
				  roundSHA1        = (block, H) => {
					  let a = H[0],
						  b = H[1],
						  c = H[2],
						  d = H[3],
						  e = H[4];

					  for (let i = 0, W = [], T; i < 80; i++) {
						  if (i < 16)  W[i] = block[i];
						  else W[i] = rotl_32(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);

						  if (i < 20)  T = safeAdd_32_5(rotl_32(a, 5), ch_32(b, c, d), e, 0x5a827999, W[i]);
						  else if (i < 40) T = safeAdd_32_5(rotl_32(a, 5), parity_32(b, c, d), e, 0x6ed9eba1, W[i]);
						  else if (i < 60) T = safeAdd_32_5(rotl_32(a, 5), maj_32(b, c, d), e, 0x8f1bbcdc, W[i]);
						  else T = safeAdd_32_5(rotl_32(a, 5), parity_32(b, c, d), e, 0xca62c1d6, W[i]);

						  e = d;
						  d = c;
						  c = rotl_32(b, 30);
						  b = a;
						  a = T;
					  }

					  H[0] = safeAdd_32_2(a, H[0]);
					  H[1] = safeAdd_32_2(b, H[1]);
					  H[2] = safeAdd_32_2(c, H[2]);
					  H[3] = safeAdd_32_2(d, H[3]);
					  H[4] = safeAdd_32_2(e, H[4]);

					  return H;
				  },
				  finalizeSHA1     = (buf, bLen, pLen, H) => {
					  let offset = (((bLen + 65) >>> 9) << 4) + 15;

					  while (buf.length <= offset) buf.push(0);

					  buf[bLen >>> 5] |= 0x80 << (24 - (bLen % 32));
					  buf[offset] = bLen + pLen;

					  for (let i = 0, l = buf.length; i < l; i += 16) H = roundSHA1(buf.slice(i, i + 16), H);

					  return H;
				  },
				  roundSHA2        = (block, H, level) => {
					  let a = H[0],
						  b = H[1],
						  c = H[2],
						  d = H[3],
						  e = H[4],
						  f = H[5],
						  g = H[6],
						  h = H[7],
						  l,
						  Int,
						  binaryStringMult,
						  safeAdd_2, safeAdd_4, safeAdd_5, gamma0, gamma1, sigma0, sigma1,
						  maj, ch, K;

					  if (level <= 256) {
						  l = 64;
						  binaryStringMult = 1;
						  Int = Number;
						  safeAdd_2 = safeAdd_32_2;
						  safeAdd_4 = safeAdd_32_4;
						  safeAdd_5 = safeAdd_32_5;
						  gamma0 = gamma0_32;
						  gamma1 = gamma1_32;
						  sigma0 = sigma0_32;
						  sigma1 = sigma1_32;
						  maj = maj_32;
						  ch = ch_32;
						  K = getK();
					  }
					  else {
						  l = 80;
						  binaryStringMult = 2;
						  Int = int_64;
						  safeAdd_2 = safeAdd_64_2;
						  safeAdd_4 = safeAdd_64_4;
						  safeAdd_5 = safeAdd_64_5;
						  gamma0 = gamma0_64;
						  gamma1 = gamma1_64;
						  sigma0 = sigma0_64;
						  sigma1 = sigma1_64;
						  maj = maj_64;
						  ch = ch_64;
						  K = getK(true);
					  }

					  for (let i = 0, W = [], T1, T2, int1, int2, offset; i < l; i++) {
						  if (i < 16) {
							  offset = i * binaryStringMult;
							  int1 = (block.length <= offset) ? 0 : block[offset];
							  int2 = (block.length <= offset + 1) ? 0 : block[offset + 1];
							  W[i] = new Int(int1, int2);
						  }
						  else {
							  W[i] = safeAdd_4(
								  gamma1(W[i - 2]), W[i - 7],
								  gamma0(W[i - 15]), W[i - 16]
							  );
						  }

						  T1 = safeAdd_5(h, sigma1(e), ch(e, f, g), K[i], W[i]);
						  T2 = safeAdd_2(sigma0(a), maj(a, b, c));
						  h = g;
						  g = f;
						  f = e;
						  e = safeAdd_2(d, T1);
						  d = c;
						  c = b;
						  b = a;
						  a = safeAdd_2(T1, T2);
					  }

					  H[0] = safeAdd_2(a, H[0]);
					  H[1] = safeAdd_2(b, H[1]);
					  H[2] = safeAdd_2(c, H[2]);
					  H[3] = safeAdd_2(d, H[3]);
					  H[4] = safeAdd_2(e, H[4]);
					  H[5] = safeAdd_2(f, H[5]);
					  H[6] = safeAdd_2(g, H[6]);
					  H[7] = safeAdd_2(h, H[7]);

					  return H;
				  },
				  finalizeSHA2     = (buf, bLen, pLen, H, level) => {
					  let offset, binaryStringInc;

					  if (level <= 256) {
						  offset = (((bLen + 65) >>> 9) << 4) + 15;
						  binaryStringInc = 16;
					  }
					  else {
						  offset = (((bLen + 129) >>> 10) << 5) + 31;
						  binaryStringInc = 32;
					  }

					  while (buf.length <= offset) buf.push(0);

					  buf[bLen >>> 5] |= 0x80 << (24 - bLen % 32);
					  buf[offset] = bLen + pLen;

					  for (let i = 0, l = buf.length; i < l; i += binaryStringInc) H = roundSHA2(buf.slice(i, i + binaryStringInc), H, level);

					  switch (level) {
						  case 224:
							  return [
								  H[0], H[1], H[2], H[3],
								  H[4], H[5], H[6]
							  ];
						  case 256:
							  return H;
						  case 384:
							  return [
								  H[0].highOrder, H[0].lowOrder,
								  H[1].highOrder, H[1].lowOrder,
								  H[2].highOrder, H[2].lowOrder,
								  H[3].highOrder, H[3].lowOrder,
								  H[4].highOrder, H[4].lowOrder,
								  H[5].highOrder, H[5].lowOrder
							  ];
						  case 512:
							  return [
								  H[0].highOrder, H[0].lowOrder,
								  H[1].highOrder, H[1].lowOrder,
								  H[2].highOrder, H[2].lowOrder,
								  H[3].highOrder, H[3].lowOrder,
								  H[4].highOrder, H[4].lowOrder,
								  H[5].highOrder, H[5].lowOrder,
								  H[6].highOrder, H[6].lowOrder,
								  H[7].highOrder, H[7].lowOrder
							  ];
					  }
				  },
				  getTypeConverter = (format)=> {
					  switch (format) {
						  case "HEX":
							  return hex2binb;
						  case "TEXT":
						  default:
							  return str2binb;
						  case "B64":
							  return b642binb;
						  case "BYTES":
							  return bytes2binb;
					  }
				  },
				  setKey           = (key, level) => {
					  let buf            = converterFunc(key),
						  len            = buf.length,
						  blockByteSize  = variantBlockSize >>> 3,
						  lastArrayIndex = (blockByteSize / 4) - 1;

					  buf = buf.value;

					  if (blockByteSize < (len / 8)) {
						  buf = finalizeFunc(buf, len, 0, getH(level), level);

						  while (buf.length <= lastArrayIndex) buf.push(0);
						  buf[lastArrayIndex] &= 0xFFFFFF00;
					  }
					  else if (blockByteSize > (len / 8)) {
						  while (buf.length <= lastArrayIndex) buf.push(0);
						  buf[lastArrayIndex] &= 0xFFFFFF00;
					  }

					  for (let i = 0; i <= lastArrayIndex; i++) {
						  keyWithIPad[i] = buf[i] ^ 0x36363636;
						  keyWithOPad[i] = buf[i] ^ 0x5C5C5C5C;
					  }

					  intermediateH = roundFunc(keyWithIPad, intermediateH, level);
					  processedLen = variantBlockSize;
				  },
				  setData          = (src, level)=> {
					  let buf                = converterFunc(src, remainder, remainderLen),
						  len                = buf.length,
						  bufInLen           = len >>> 5,
						  updateProcessedLen = 0,
						  variantBlockIntInc = variantBlockSize >>> 5;

					  buf = buf.value;

					  for (let i = 0; i < bufInLen; i += variantBlockIntInc) {
						  if (updateProcessedLen + variantBlockSize <= len) {
							  intermediateH = roundFunc(
								  buf.slice(i, i + variantBlockIntInc),
								  intermediateH,
								  level
							  );
							  updateProcessedLen += variantBlockSize;
						  }
					  }
					  processedLen += updateProcessedLen;
					  remainder = buf.slice(updateProcessedLen >>> 5);
					  remainderLen = len % variantBlockSize;
				  };

			let {
					format,
					encoding,
					level,
					key
				}            = opt,
				processedLen = 0,
				remainder    = [],
				remainderLen = 0,
				keyWithIPad  = [],
				keyWithOPad  = [];

			if (jBD.isNumber(opt)) level = opt;
			else if (jBD.isString(opt)) key = opt;

			if (!jBD.has(format, ["TEXT", "HEX", "B64", "BASE64", "BYTES"])) format = "TEXT";
			if (!jBD.has(encoding, ["ANSI", "UTF8", "UTF16"])) encoding = "UTF8";
			if (!jBD.has(level, [1, 224, 256, 384, 512])) level = 256;
			if (!jBD.isString(key)) key = "";

			if (format == "BASE64") format = "B64";

			if (jBD.isNull(value, true)) value = "";

			let converterFunc = getTypeConverter(format),
				roundFunc     = roundSHA2,
				finalizeFunc  = finalizeSHA2,
				formatFunc;

			let variantBlockSize = 512,
				outputBinLen     = level,
				intermediateH    = getH(level);

			switch (level) {
				case 1:
					outputBinLen = 160;
					roundFunc = roundSHA1;
					finalizeFunc = finalizeSHA1;
					break;
				case 224:
				case 256:
					break;
				case 384:
					variantBlockSize = 1024;
					outputBinLen = 384;
					break;
				case 512:
					variantBlockSize = 1024;
					outputBinLen = 512;
					break;
			}

			if (key) setKey(key, level);

			setData(value, level);

			switch (format) {
				default:
				case "HEX":
					formatFunc = binb2hex;
					break;
				case "B64":
					formatFunc = binb2b64;
					break;
				case "BYTES":
					formatFunc = binb2bytes;
					break;
			}

			if (!key) intermediateH = finalizeFunc(remainder, remainderLen, processedLen, intermediateH, level);
			else {
				let i = finalizeFunc(remainder, remainderLen, processedLen, intermediateH, level);
				intermediateH = roundFunc(keyWithOPad, getH(level), level);
				intermediateH = finalizeFunc(i, outputBinLen, variantBlockSize, intermediateH, level);
			}

			return formatFunc(intermediateH);
		},
		/**
		 * CRC编解码
		 *
		 * @public
		 * @param {Array} buf 0xFFFF数组
		 * @param {number} [type=16] 8、16、32、
		 * @returns {number}
		 */
		CRC:    (buf, type)=> {
			const to8  = buf => {
					  let r = 0x00;

					  for (let i = 0, l = buf.length, j, d; i < l; i++) {
						  r ^= buf[i];
						  for (j = 0; j < 8; j++) {
							  d = r & 1;
							  r = r >> 1;
							  if (d === 1) r = r & 0x8c;
						  }
					  }

					  return r;
				  },
				  to7  = buf => {
					  let r = 0;

					  for (let i = 0, l = buf.length, j, d, c; i < l; i++) {
						  for (j = 0, d = buf[i]; j < 8; j++) {
							  c = d >> (7 - i);
							  if ((r >> 15) ^ (c & 1)) r = r << 1 ^ 0x8005;
							  else r <<= 1;
						  }
					  }

					  return r;
				  },
				  to16 = buf=> {
					  let r = 0xFFFF;

					  for (let i = 0, l = buf.length, j, d; i < l; i++) {
						  r ^= buf[i];
						  for (j = 0; j < 8; j++) {
							  d = r & 1;
							  r = (r >> 1) & 0x7FFF;

							  if (d === 1) r = (r ^ 0xA001) & 0xFFFF;
						  }
					  }

					  return r;
				  },
				  to32 = buf=> {
					  const TAB = [
						  0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA, 0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
						  0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988, 0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
						  0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE, 0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
						  0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC, 0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
						  0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172, 0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
						  0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940, 0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
						  0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116, 0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
						  0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924, 0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,
						  0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A, 0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
						  0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818, 0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
						  0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E, 0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
						  0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C, 0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
						  0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2, 0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
						  0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0, 0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
						  0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086, 0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
						  0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4, 0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,
						  0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A, 0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
						  0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8, 0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
						  0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE, 0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
						  0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC, 0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
						  0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252, 0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
						  0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60, 0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
						  0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236, 0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
						  0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04, 0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,
						  0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A, 0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
						  0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38, 0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
						  0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E, 0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
						  0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C, 0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
						  0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2, 0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
						  0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0, 0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
						  0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6, 0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
						  0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94, 0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D
					  ];

					  let r = 0xFFFFFFFF;

					  for (let i = 0, l = buf.length, d; i < l; i++) {
						  d = buf[i];
						  r = TAB[(r ^ d) & 0xFF] ^ ((r >> 8) & 0xFFFFFF);
					  }

					  return r ^ 0xFFFFFFFF;
				  };

			buf = jBD.isArray(buf) ? buf : [];

			switch (String(type)) {
				default:
				case "1":
				case "16":
					return to16(buf);
				case "2":
				case "32":
					return to32(buf);
				case "3":
				case "8":
					return to8(buf);
				case "4":
				case "7":
					return to7(buf);
			}
		}
	};

	return api;
}, {module: module, exports: this}, ["Conver"], "Security");