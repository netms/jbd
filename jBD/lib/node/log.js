/**
 * ==========================================
 * Name:           Log
 * Author:         Buddy-Deus
 * CreTime:        2014-11-20
 * Description:    mongo操作
 * Log
 * 2015-06-09    初始化
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";

	let logType = {
			None: 0,
			Sys:  1,
			DB:   2,
			Usr:  3
		},
		error   = (dtd, code, msg)=> {
			if (dtd) dtd.reject({errorCode: code, errorMsg: msg});
			else return {errorCode: code, errorMsg: msg};
		};

	let LOG = (()=> {
		class LOG {
			constructor (cfg) {
				let _this = this;

				return _this;
			};

			Free () {

			};

			Add (level, code, msg) {
				//let _dt = new Date();
			};

//		==============================================================

			get mode () { return "db"; };

			get TYPE () { return logType; };

//		==============================================================

		}

		return LOG;
	})();

	LOG.Error = error;
	LOG.Type = logType;

	return LOG;
}, {module: module, exports: this}, ["String", "Conver"], "DB");