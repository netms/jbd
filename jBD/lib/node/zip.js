/**
 * ==========================================
 * Name:           Zip
 * Author:            Buddy-Deus
 * CreTime:            2016-02-25
 * Description:        ZIP操作
 * Log
 * 2016-02-25    初始化
 * ==========================================
 */
jBD.define(function (module, exports, require) {
	"use strict";

	const fs   = require("fs"),
		  path = require("path"),
		  zlib = require("zlib");

	let api;

	api = {};

	return api;
}, {module: module, exports: this}, ["Security"], "ZIP");