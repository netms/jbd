const initPKG       = pkg => {
		  let v;

		  for (let k in pkg) {
			  v = pkg[k];

			  if (typeof(v) == "object") {
				  v.MAX = initPKG_ITEM(v);
				  if (!v.NAME) v.NAME = k;
			  }
		  }
	  },
	  initPKG_ITEM  = pkg => {
		  if (pkg.MAX) return pkg.MAX;

		  let item = (o, c, l) => {
				  if (!o.count) o.count = c * (l || 1);

				  return o.count;
			  },
			  type = obj => {
				  switch (obj.type) {
					  case "byte":
					  case "sbyte":
						  return 1;
					  case "ushort":
					  case "short":
						  return 2;
					  case "uint":
					  case "int":
						  return 4;
					  case "ulong":
					  case "long":
						  return 8;
					  case "float":
						  return 4;
					  case "double":
						  return 8;
					  case "string":
						  return 1 * obj.max;
					  case "array":
						  return type({type: obj.arg}) * obj.max;
					  default:
						  if (PKG[obj.type]) return initPKG_ITEM(PKG[obj.type]);
						  break;
				  }
			  },
			  lst  = {length: 0},
			  obj;

		  for (let k in pkg) {
			  if (k != "MT") {
				  obj = pkg[k];

				  item(obj, type(obj));

				  lst[obj.index] = obj;
				  lst.length++;
			  }
		  }

		  for (let i = 0, l = 0; i < lst.length; i++) {
			  obj = lst[i];

			  obj.start = l;
			  l = obj.start + obj.count;
			  obj.end = l - 1;
			  // o.index = l;
		  }

		  pkg.MAX = lst[lst.length - 1].end + 1;

		  return pkg.MAX;
	  },
	  initType      = pkg => {
		  switch (pkg.type) {
			  case "byte":
			  case "sbyte":
				  return 0;
			  case "ushort":
			  case "short":
				  return 0;
			  case "uint":
			  case "int":
				  return 0;
			  case "ulong":
			  case "long":
				  return 0;
			  case "float":
				  return 0.0;
			  case "double":
				  return 0.0;
			  case "string":
				  return "";
			  case "array":
				  return initArray(pkg);
			  default:
				  if (PKG[pkg.type]) return initBlock(PKG[pkg.type]);
				  break;
		  }
	  },
	  initArray     = pkg => {
		  let result = [],
			  arg    = {type: pkg.arg, start: 0};

		  for (let i = 0; i < pkg.max; i++) result.push(initType(arg));

		  return result;
	  },
	  initBlock     = pkg => {
		  let result = {};

		  for (let k in pkg) {
			  if (!jBD.has(k, ["MAX", "NAME"])) result[k] = initType(pkg[k]);
		  }

		  return result;
	  },
	  readType      = (item, buf, offset) => {
		  offset += item.start;

		  switch (item.type) {
			  case "byte":
				  return buf.readUInt8(offset);
			  case "sbyte":
				  return buf.readInt8(offset);
			  case "ushort":
				  return buf.readUInt16LE(offset);
			  case "short":
				  return buf.readInt16LE(offset);
			  case "uint":
				  return buf.readUInt32LE(offset);
			  case "int":
				  return buf.readInt32LE(offset);
			  case "ulong":
			  case "long":
				  return (buf.readUInt32LE(offset) << 8) + buf.readUInt32LE(offset + 4);
			  case "float":
				  return buf.readFloatLE(offset);
			  case "double":
				  return buf.readDoubleLE(offset);
			  case "string":
				  return buf.toString(item.encoding, offset, offset + item.count).trim();
			  case "array":
				  return readArray(item, buf, offset);
			  default:
				  if (PKG[item.type]) return readBlock(PKG[item.type], buf, offset);
				  break;
		  }
	  },
	  readArray     = (item, buf, offset) => {
		  let result = [],
			  arg    = {type: item.arg, start: 0};

		  for (let i = 0; i < item.max; i++) result.push(readType(arg, buf, offset + i));

		  return result;
	  },
	  readBlock     = (pkg, buf, offset) => {
		  let result = {};

		  if (!offset || offset < 0) offset = 0;

		  for (let k in pkg) {
			  if (!jBD.has(k, ["MAX", "NAME", "MT"])) result[k] = readType(pkg[k], buf, offset);

		  }

		  return result;
	  },
	  writeType     = (item, value, buf, offset) => {
		  offset += item.start;
		  value = value || 0;

		  switch (item.type) {
			  case "byte":
				  buf.writeUInt8(value, offset);
				  break;
			  case "sbyte":
				  buf.writeInt8(value, offsett);
				  break;
			  case "ushort":
				  buf.writeUInt16LE(value, offset);
				  break;
			  case "short":
				  buf.writeInt16LE(value, offset);
				  break;
			  case "uint":
				  buf.writeUInt32LE(value, offset);
				  break;
			  case "int":
				  buf.writeInt32LE(value, offset);
				  break;
			  case "ulong":
			  case "long":
				  buf.writeUInt32LE(value, offset);
				  buf.writeUInt32LE(value, offset + 4);
				  break;
			  case "float":
				  buf.writeFloatLE(value || 0.0, offset);
				  break;
			  case "double":
				  buf.writeDoubleLE(value || 0.0, offset);
				  break;
			  case "string":
				  buf.fill(0, offset, offset + item.count);
				  buf.write(value || "", offset, offset + item.count, item.encoding);
				  break;
			  case "array":
				  writeArray(item, value || [], buf, offset);
				  break;
			  default:
				  if (PKG[item.type]) writeBlock(PKG[item.type], value || {}, buf, offset);
				  break;
		  }
	  },
	  writeArray    = (item, value, buf, offset) => {
		  let arg = {type: item.arg, start: 0};

		  for (let i = 0; i < item.max; i++) writeType(arg, i < value.length ? value[i] : 0, buf, offset + i);
	  },
	  writeBlock    = (pkg, data, buf, offset) => {
		  if (!offset || offset < 0) offset = 0;

		  for (let k in pkg) writeType(pkg[k], data[k], buf, offset);

		  return buf;
	  },
	  GetIP         = ip => {
		  let result = ip.split(".").concat(0, 0, 0, 0).slice(0, 3);

		  for (let i = 0; i < 4; i++) result[i] = parseInt(result[i]);

		  return result;
	  },
	  GetSerialID   = () => {
		  let result = jBD.Conver.toString("yyyymmddhhnnsszzz", new Date());

		  result = parseInt(result);

		  return result;
	  },
	  GetCheckCode  = (buf, offset, count) => {
		  let result = 0x00;

		  count += offset;
		  for (let i = offset; i < count; i++) {
			  result ^= buf[i];
		  }

		  return result;
	  },
	  CheckIP       = buf => {
		  if (!buf[0] && !buf[1] && !buf[2] && !buf[3]) return true;
		  else return buf[0] > 0 && buf[0] < 255 && buf[3] > 0 && buf[3] < 255;
	  },
	  CheckSerialID = buf => {
		  if (buf === 0) return true;
		  if ((buf = buf + "").length < 17) return false;

		  buf = [
			  parseInt(buf.substring(0, 4)),
			  parseInt(buf.substring(4, 2)),
			  parseInt(buf.substring(6, 2)),
			  parseInt(buf.substring(8, 2)),
			  parseInt(buf.substring(10, 2)),
			  parseInt(buf.substring(12, 2)),
			  parseInt(buf.substring(14, 3))
		  ];

		  return buf[0] >= 2000 &&
			  buf[1] > 0 && buf[1] < 13 &&
			  buf[2] > 0 && buf[2] < 32 &&
			  buf[3] >= 0 && buf[3] < 24 &&
			  buf[4] >= 0 && buf[4] < 60 &&
			  buf[5] >= 0 && buf[5] < 60 &&
			  buf[7] >= 0 && buf[7] < 1000;
	  };

let PKG = {
		Header:      {
			Standard:  {type: "ushort", index: 0},
			Version:   {type: "byte", index: 1},
			SourceIP:  {type: "array", index: 2, arg: "byte", max: 4},
			TargetIP:  {type: "array", index: 3, arg: "byte", max: 4},
			SerialID:  {type: "ulong", index: 4},
			Type:      {type: "byte", index: 5},
			EnCode:    {type: "byte", index: 6},
			Zip:       {type: "byte", index: 7},
			DataLen:   {type: "uint", index: 8},
			DataMT:    {type: "ushort", index: 9},
			CheckCode: {type: "byte", index: 10}
		},
		V2:          {
			x: {type: "float", index: 0},
			y: {type: "float", index: 1}
		},
		V3:          {
			x: {type: "float", index: 0},
			y: {type: "float", index: 1},
			z: {type: "float", index: 2}
		},
		V4:          {
			x: {type: "float", index: 0},
			y: {type: "float", index: 1},
			z: {type: "float", index: 2},
			w: {type: "float", index: 3}
		},
		Share2D:     {
			MT:       0x1002,
			name:     {type: "string", index: 0, max: 32, encoding: "utf8"},
			size:     {type: "V2", index: 1},
			position: {type: "V2", index: 2},
			rotate:   {type: "V3", index: 3},
			scale:    {type: "V2", index: 4}
		},
		Share3D:     {
			MT:       0x1003,
			name:     {type: "string", index: 0, max: 32, encoding: "utf8"},
			model:    {type: "string", index: 1, max: 32, encoding: "utf8"},
			position: {type: "V3", index: 2},
			rotate:   {type: "V3", index: 3},
			scale:    {type: "V3", index: 4}
		},
		ShareCMD:    {
			MT:     0x1001,
			name:   {type: "string", index: 0, max: 32, encoding: "utf8"},
			action: {type: "byte", index: 1},
			arg:    {type: "array", index: 2, arg: "byte", max: 8}
		},
		ShareOrigin: {
			MT:       0x1004,
			name:     {type: "string", index: 0, max: 32, encoding: "utf8"},
			position: {type: "V3", index: 1},
			rotate:   {type: "V3", index: 2}
		}
	},
	PKGProtocol;

PKGProtocol = jBD.TCache.extend({
	className:   "PKGProtocol",
	ip:          "0.0.0.0",
	create:      function (opt) {
		let size, std;

		switch (typeof(opt)) {
			case "object":
				if (opt) {
					size = opt.size;
					std = opt.std;
				}
				break;
			case "number":
				size = opt;
		}

		this.super("create", size);

		this.std = std > 0 ? std : 0xFEFC;
		this.state = false;
		this.rev = null;

		this.PKG = PKG;
	},
	parse:       function () {
		if (this.length < 1) return;

		let len = 0;

		if (!this.state) {
			if (this.length < 2) len = 0;
			else if (this.cache.readUInt16LE(this.index) != this.std) len = 2;
			else if (this.length >= PKG.Header.MAX) len = this.parseHeader();
		}
		else if (this.length >= this.rev.len) len = this.parseData();

		if (len < 1) return;

		this.remove(len);

		if (this.length > 0) this.parse();
	},
	parseHeader: function () {
		let pkg = this.read(PKG.Header);

		if (!CheckIP(pkg.SourceIP)) return 7;
		if (!CheckIP(pkg.TargetIP)) return 11;
		if (!CheckSerialID(pkg.SerialID)) return 19;

		this.state = true;
		this.rev = {
			mt:      pkg.DataMT,
			len:     pkg.DataLen,
			code:    pkg.CheckCode,
			surplus: pkg.DataLen
		};

		this.call("buffer", [this.rev, this.cache, this.index, this.PKG.Header.MAX]);

		return PKG.Header.MAX;
	},
	parseData:   function () {

	},
	init:        function (pkg) {
		return initBlock(pkg);
	},
	read:        function (pkg) {
		return readBlock(pkg, this.cache, this.index);
	},
	write:       function (pkg, value, buf, offset) {
		if (buf) writeBlock(pkg, value, buf, offset);
		else {
			offset = PKG.Header.MAX;
			buf = Buffer.alloc(offset + pkg.MAX);

			this.write(pkg, value, buf, offset);
			this.write(PKG.Header, {
				Standard:  this.std,
				Version:   1,
				SourceIP:  [0, 0, 0, 0],
				TargetIP:  GetIP(this.ip),
				SerialID:  GetSerialID(),
				Type:      0,
				EnCode:    0,
				Zip:       0,
				DataLen:   pkg.MAX,
				DataMT:    pkg.MT,
				CheckCode: GetCheckCode(buf, offset, pkg.MAX)
			}, buf, 0);

		}

		return buf;
	}
});

initPKG(PKGProtocol.PKG = PKG);

PKGProtocol.read = readBlock;
PKGProtocol.write = writeBlock;
PKGProtocol.init = initBlock;

module.exports = exports = PKGProtocol;
