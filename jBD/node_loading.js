/**
 * ==========================================
 * Name:           jBD‘s Loading
 * Author:         Buddy-Deus
 * CreTime:        2016-05-10
 * Description:    jBD Loading Node
 * Log
 * 2016-05-10    初始化加载列表
 * 2016-11-24    基于ES6改写
 * ==========================================
 */
(jBD => {
	if (jBD.mode != jBD.MODE.Node) return;

	jBD.__require("./lib/node/net", "Net");
	jBD.__require("./lib/node/db", "DB");
	jBD.__require("./lib/node/log", "Log");
	jBD.__require("./lib/node/fs", "FS");
	// jBD.__require("./lib/node/zip", "Zip");
})(this);