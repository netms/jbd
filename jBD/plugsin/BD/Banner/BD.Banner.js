define(function (exports, require) {
	var TBDBanner = TObject.extend({
		Create: function(own, opt){
			var _this = this;

			_this.own = own == void 0 ? null : own;
			_this.opt = opt == void 0 ? null : opt;
			_this.data = new TList((_this.opt && jBD.isArray(_this.opt.data)) ? _this.opt.data : null);
			_this.ti = -1;
			_this.nw = -1;

			_this.Draw(_this.own);

			return _this;
		},
		Draw: function (own) {
			var _this = this,
				_own = $("<div>");

			_this.Stop();

			_this.own = own || _this.own;
			_this.down = $("<div class=\"bd_banner_data\">");
			_this.sown = $("<div class=\"bd_banner_side\">");

			if(_this.opt.mode !== 2) _this.down.addClass("box_h");

			own.empty();
			own.append(_own);

			_own.addClass("bd_banner_container")
				.append(_this.down)
				.append(_this.sown);

			jBD.each(_this.data.valueOf(), function (d, i) {
				_this.DrawData(d, i);
			});
			if(_this.data.length > 1) _this.DrawData(_this.data.First(), _this.data.length);

			_this.Event();

			return _own;
		},
		Event: function () {
			var _this = this;

			_this.nw = 0;

			_this.down.find("a").hover(
				function () { _this.Stop(); },
				function () { _this.Start(); }
			);

			_this.sown
				.css("margin-left", parseInt((_this.own.width() - _this.sown.width()) / 2) + 15)
				.find("a").off().on({
					click: function () {
						_this.DrawEffect(_this.opt.mode);
					}
				})
				.eq(0).addClass("sel");

			_this.Start();
		},
		DrawData: function (data, index) {
			var _this = this;

			$("<a>")
				.attr({
					title: data.title || "",
					href:  data.url || ""
				})
				.append($("<img class=\"bd_banner_img\">").attr("src", data.image || "../inc/lib/BD/Banner/bd_nopic.png"))
				.appendTo(_this.down);

			if(index < _this.data.length) {
				$("<a>")
					.attr("index", index)
					.appendTo(_this.sown);
			}
		},
		DrawEffect: function (mode, index) {
			if (this.data.length < 2) return;

			var _this = this,
				pos = {
					i: jBD.isNumber(index) ? index : _this.nw + 1,
					w: _this.down.find("img:first").width(),
					h: _this.down.find("img:first").height(),
					x: 0,
					y: 0
				};

			mode = parseInt(mode) || 0;

			pos.i = pos.i <= _this.data.length ? pos.i : 0;

			switch (mode) {
				case 2:
					pos.x = 0;
					pos.y = 0 - (pox.h * pos.i);
					break;
				case 1:
				default:
					pos.x = 0 - (pos.w * pos.i);
					pos.y = 0;
					break;
			}

			_this.down.animate({
				"margin-left": pos.x,
				"margin-top":  pos.y
			}, _this.opt.speed, function () {
				_this.sown
					.find("a").removeClass("sel")
					.eq(pos.i).addClass("sel");

				if (pos.i == _this.data.length) {
					_this.down.css({
						"margin-left": 0,
						"margin-top":  0
					});
					_this.sown.find("a:first").addClass("sel");

					_this.nw = 0;
				} else
					_this.nw = pos.i;
			});
		},
		Add: function (data, draw) {
			var _this = this;

			if (_this.opt.max >= 0 && _this.opt.max <= _this.data.length) return -1;

			draw = jBD.isBool(draw) ? draw : true;

			switch (jBD.type(data, true)) {
				case "array":
					jBD.each(data, function (d) {
						_this.Add(d, false);
					});

					_this.Draw();
					break;
				case "object":
					if (_this.opt.max < 0 || _this.data.length <= _this.opt.max) {
						_this.data.Add(data);

						if (draw) _this.Draw();

						break;
					}
				default:
					return -1;
			}

			return _this.data.length - 1;
		},
		Del: function (index) {
			var _this = this;

			_this.data.Del(index);

			_this.Draw();
		},
		Clear: function () {
			var _this = this;

			_this.data.Clear();

			_this.Draw();
		},
		Start: function () {
			var _this = this;

			_this.Stop();

			_this.ti = setTimeout(function () {
				_this.DrawEffect(_this.opt.mode, _this.nw + 1);

				_this.Start();
			}, _this.opt.time * 1000);
		},
		Stop: function () {
			var _this = this;

			if(_this.ti < 0) return;

			clearTimeout(_this.ti);
			_this.ti = -1;
		}
	});

	return $.fn.BDBanner = function(opt){
		opt = opt == void 0 || typeof (opt) != "object" ? {} : opt;
		var _opt = $.extend({
			mode: 1, time: 5, max: -1, speed: 500,
			data: []
		}, opt);

		return new TBDBanner($(this), _opt);
	};
}, "../inc/lib/BD/Banner/BD.Banner.css", "BD.Banner");