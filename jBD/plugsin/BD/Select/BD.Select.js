﻿$.fn.BDSelect = function (opt, d) {
	d = d == void 0 || !(d instanceof Array) ? null : d;
	opt = opt == void 0 || typeof (opt) != "object" ? {} : opt;
	var _opt = $.extend({
		type: "single",
		embed: true,
		OnChange: null
	}, opt);
	var _this = null;

	//switch (_opt.type.toLowerCase()) {
	//	case "single":
	//		_opt = $.extend({

	//		}, _opt);
	//		break;
	//	case "multi":
	//		_opt = $.extend({

	//		}, _opt);
	//		break;
	//}


	switch (_opt.type.toLowerCase()) {
		case "single":
		default:
			_this = new TBDSelect_Single($(this), _opt);
			break;
		case "multi":
			_this = new TBDSelect_Multi($(this), _opt);
			break;
	}

	if (d) {
		for (var i = 0; i < d.length; i++) _this.Add(d[i]);
	}

	return _this;
}

function TBDSelect(own, opt) { }
TBDSelect.prototype = (new TObject()).extend({
	opt: null, own: null, down: null, data: null,
	state: false, pack: false, now: [],
	Create: function (own, opt) {
		var _this = this;

		_this.own = own == void 0 ? null : own;
		_this.opt = opt == void 0 ? null : opt;
		_this.data = new TList();

		if (_this.own) _this.Draw(_this.own);

		return _this;
	},
	Draw: function (own) {
		own = own == void 0 || !(own) ? $("<div>") : own;

		var _this = this;
		_this.own = own;
		_this.down = null;

		own.addClass("bd_select")
			.append($("<div class=\"f_l bd_select_value box_h\">")
				.append(_this.DrawValue())
			)
			.append($("<div class=\"f_l bd_select_button\">")
				.append($("<a class=\"bd_select_button_btn\" state=\"false\">").text("更多"))
			);

		_this.Event();

		return _this.own;
	},
	DrawValue: function () { },
	DrawData: function (pos) {
		var _this = this;

		if (_this.opt.embed) {
			_this.own.append(_this.down.addClass("embed"));
		} else {
			$("body").append(_this.down);
			_this.down.css({ left: pos.left, top: pos.top });
		}

		_this.down.width(_this.own.find("div.bd_select_value").outerWidth()).show();
		_this.Event_Data(_this.down);
	},
	Resize: function (e) {
		if (e.data.state) {
			var _pos = { left: e.data.pos.left, top: e.data.pos.top }

			_pos.left += $("bodyer").scrollLeft() - 1;
			_pos.top += e.data.height + 4;

			e.data.own.css({ left: _pos.left, top: _pos.top });
		}
	},
	Event: function () {
		var _this = this;

		_this.own
			.find("a.bd_select_button_btn[state]").off().on({
				click: function () {
					if (_this.state) {
						_this.Pack(false);
					} else {
						_this.Pack(true);
					}
				}
			});
	},
	Event_Data: function (own) {
		var _this = this;

		if (!_this.opt.embed) {
			var _height = _this.own.find("div.bd_select_value").outerHeight(true)
			var _pos = _this.own.find("div.bd_select_value").offset();

			//$(window)
			//		.bind("resize", { state: _this.state, pos: _pos, height: _height, own: _this.down }, _this.Resize)
			//		.bind("scroll", { state: _this.state, pos: _pos, height: _height, own: _this.down }, _this.Resize);
		}
	},
	Pack: function (s) {
		var _this = this;

		s = s == void 0 || typeof (s) != "boolean" ? !(_this.pack) : s;
		if (s) {
			if (_this.down) _this.down.remove();
			_this.DrawData();
			_this.state = true;
			_this.own.find("a.bd_select_button_btn[state]").addClass("sel").attr("state", _this.state);
		} else {
			if (_this.down) this.down.hide().remove();
			_this.down = null;
			_this.state = false;
			_this.own.find("a.bd_select_button_btn[state]").removeClass("sel").attr("state", _this.state);
		}

		return _this;
	},
	Show: function () {
		this.own.show();
		return this;
	},
	Hide: function () {
		this.Pack(false);
		this.own.hide();
		return this;
	},
	Add: function (t, k, d) {
		var _d = null;
		if (arguments.length == 1 && typeof (arguments[0]) == "object") {
			_d = arguments[0];
		} else {
			k = k == void 0 ? t : k;
			d = d == void 0 ? k : d;
			_d = { key: k, text: t, value: d };
		}

		if (_d) this.data.Add(_d);
		if (this.state) this.Pack(true);

		return this;
	},
	Del: function (v) {
		var _this = this;
		var i = 0;

		switch (typeof (v)) {
			case "string":
				var j = 0;
				while (i < _this.data.length) {
					if (_this.data[i].key == v) {
						_this.data.Del(i);

						j = 0;
						while (j < _this.now.length) {
							if (i == _this.now[j]) _this.now.splice(j, 1);
						}
					} else
						i++;
				}
				break;
			case "number":
				if (v >= 0 && v < _this.data.length) {
					_this.data.Del(v);

					while (i < _this.now.length) {
						if (v == _this.now[i]) _this.now.splice(i, 1);
					}
				}
				break;
			default:
				return _this;
		}

		if (this.state) this.Pack(true);

		return _this;
	},
	Clear: function () {
		this.Pack(false);
		this.data.Clear();
		this.now.splice(0, this.now.length);
		return this;
	},
	Count: function () {
		this.length = this.data.Count();
		return this.length;
	},
	Sort: function (fun) {
		if (fun) {
			this.data.Sort(fun);
		} else {
			this.data.Sort(function (a, b) {
				return a.key.localeCompare(b.key);
			});
		}
		this.now.splice(0, this.now.length);
		if (this.state) this.Pack(true);

		return this;
	},
	Select: function (i) {
		var _this = this;
		var _d = null;

		if (i >= 0 && i < _this.data.length) {
			_d = _this.data.Item(i);
			if (_d) {
				_this.Select_Value(_this.own.find("div.bd_select_value"), _d, i);
				if (_this.opt.OnChange) _this.opt.OnChange(i, _d);
				_this.Pack(false);
			} else
				return null;
		}

		return _d;
	},
	Select_Value: function (own, d, i) { },
	Value: function () {
		var _r = [];
		var _d = null;
		for (var i = 0; i < this.now.length; i++) {
			_d = this.data.Item(this.now[i]);
			if (_d) _r.push(_d.value);
		}

		return _r.join(",");
	},
	val: function () {
		if (arguments.length > 0) {
			var _v = arguments[0];
			var _d = null;
			for (var i = 0; i < this.data.length; i++) {
				_d = this.data.Item(i);
				if (_d && _d.key == _v) {
					this.Select(i);
					return _d;
				}
			}
		} else {
			return this.Value(arguments);
		}
	}
});

function TBDSelect_Single(own, opt) { return this.Create(own, opt); }
TBDSelect_Single.prototype = (new TBDSelect()).extend({
	DrawValue: function () {
		return $("<span class=\"value single\">");
	},
	DrawData: function () {
		var _this = this;
		var _pos = _this.own.find("div.bd_select_value").offset();
		var _d = null;

		if (_this.down) _this.down.remove();
		_this.down = $("<div class=\"bd_select_data\">");

		for (var i = 0; i < _this.data.length; i++) {
			_d = _this.data.Item(i);

			_this.DrawData_Data(_this.down, i, _d);
		}

		if (!_this.opt.embed) {
			_pos.left += $("bodyer").scrollLeft() - 1;
			_pos.top += _this.own.find("div.bd_select_value").outerHeight(true) + 4;
		}

		TBDSelect.prototype.DrawData.call(_this, _pos);
	},
	DrawData_Data: function (own, i, d) {
		$("<a class=\"bd_select_data_item\">")
			.attr({ index: i, key: d.key })
			.append($("<span>").text(d.text))
			.appendTo(own);
	},
	Event_Data: function (own) {
		TBDSelect.prototype.Event_Data.call(this);

		var _this = this;

		own
			.find("a.bd_select_data_item[index][key]").off().on({
				click: function () {
					var _i = parseInt($(this).attr("index"));
					var _d = _this.Select(_i);

					if (_d) {
						if (_this.opt.OnChange) _this.opt.OnChange(_i, _d);
					}
				}
			});
	},
	Select_Value: function (own, d, i) {
		this.now.splice(0, this.now.length);
		this.now.push(i);

		own.find("span.value").text(d.text);
	}
});

function TBDSelect_Multi(own, opt) { return this.Create(own, opt); }
TBDSelect_Multi.prototype = (new TBDSelect()).extend({
	DrawValue: function () {
		return $("<span class=\"value multi\">");
	},
	DrawData: function () {
		var _this = this;
		var _pos = _this.own.find("div.bd_select_value").offset();
		var _d = null;

		if (_this.down) _this.down.remove();
		_this.down = $("<div class=\"bd_select_data\">");

		for (var i = 0; i < _this.data.length; i++) {
			_d = _this.data.Item(i);

			_this.DrawData_Data(_this.down, i, _d);
		}

		if (!_this.opt.embed) {
			_pos.left += $("bodyer").scrollLeft() - 1;
			_pos.top += _this.own.find("div.bd_select_value").outerHeight(true) + 4;
		}

		TBDSelect.prototype.DrawData.call(_this, _pos);
	},
	DrawData_Data: function (own, i, d) {
		$("<a class=\"bd_select_data_item\">")
			.attr({ index: i, key: d.key })
			.append($("<span>").text(d.text))
			.appendTo(own);
	},
	Event_Data: function (own) {
		var _this = this;

		own
			.find("a.bd_select_data_item[index][key]").off().on({
				click: function () {
					var _i = parseInt($(this).attr("index"));
					var _d = _this.Select(_i);

					if (_d) {
						if (_this.opt.OnChange) _this.opt.OnChange(_i, _d);
					}
				}
			});
	},
	Select_Value: function (own, d, i) {
		this.now.splice(0, this.now.length);
		this.now.push(i);

		own.find("span.value").text(d.text);
	}
});
