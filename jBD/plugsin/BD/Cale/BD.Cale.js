﻿(function ($) {
	$.fn.BDCale = function (opt) {
		opt = opt == void 0 || typeof (opt) != "object" ? {} : opt;
		var _opt = $.extend({
			show: true,
			min: "1990-1-1", max: "2023-1-1", now: new Date(), count: 1, speed: 500,
			WeekOffset: 1, WeekTally: ["9/1", "2/9"], WeekTallyMax: -1,
			ToolsShow: false, WeekShow: false, WeekAtLast: true,
			week: ["一", "二", "三", "四", "五", "六", "日", "周"],
			month: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"],
			OnClick: null, OnHover: [null, null], OnLClick: null, OnRClick: null, OnRealLClick: null, OnRealRClick: null
		}, opt);

		var _this = new TBDCale($(this), _opt);
		_this.Draw($(this));

		return _this;
	}

	function TBDCale(own, opt) {
		TBDCale.prototype.own = own;
		TBDCale.prototype.opt = opt;
		TBDCale.prototype.min = jBD.Conver.StrToDate(this.opt.min);
		TBDCale.prototype.max = jBD.Conver.StrToDate(this.opt.max);
		TBDCale.prototype.lnow = new Date(this.opt.now);
		TBDCale.prototype.rnow = new Date(this.opt.now);
		TBDCale.prototype.wbg = new Date();
		TBDCale.prototype.obj_ul_width = 0;
		TBDCale.prototype.obj_own = null;
		TBDCale.prototype.lrstate = [true, true];
		TBDCale.prototype.shstate = false;
	}
	TBDCale.prototype = (new TObject()).extend({
		GetWeekTally: function (dt) {
			var _this = this;
			var _now = dt == void 0 || !(dt instanceof Date) ? new Date() : new Date(dt);
			var _wd = null;
			var _wt = [];
			_now.setHours(0); _now.setMinutes(0); _now.setSeconds(0); _now.setMilliseconds(0);
			for (var i = 0; i < _this.opt.WeekTally.length; i++) {
				_wt.push((_now.getFullYear() + "/" + _this.opt.WeekTally[i]).StrToDate());
				_wt[i].setHours(0); _wt[i].setMinutes(0); _wt[i].setSeconds(0); _wt[i].setMilliseconds(0);

				if (i > 0) {
					while (_wt[i - 1] > _wt[i]) _wt[i].setFullYear(_wt[i].getFullYear() + 1);
				}
				if (i >= _this.opt.WeekTally.length - 1) {
					_wt.push(new Date(_wt[0]));
					_wt[i + 1].setFullYear(_wt[0].getFullYear() + 1);
				}
			}
			var _fun_setYear = function (yy) {
				for (var i = 0; i < _wt.length; i++) {
					_wt[i].setFullYear(yy);

					if (i > 0) {
						while (_wt[i - 1] >= _wt[i]) _wt[i].setFullYear(_wt[i].getFullYear() + 1);
					}
				}
			};

			var _b = false;
			while (!_b) {
				var _i = 0;
				for (_i = 0; _i < _wt.length; _i++) {
					if (_wt.length < 2) {
						_b = _now < _wt[_i];
					} else {
						if (_i == 0) {
							_b = false;
						} else {
							_b = _now >= _wt[_i - 1] && _now <= _wt[_i];
						}
					}
					if (_b) break;
				}
				if (_b)
					_wd = _wt[_i - 1];
				else
					_fun_setYear(_wt[0].getFullYear() - 1);
			}

			return _wd;
		},
		GetWeek: function (dt) {
			var _this = this;
			var _now = dt == void 0 || !(dt instanceof Date) ? new Date() : new Date(dt);
			var _l = 0;
			var _wd = _this.GetWeekTally(_now);

			_l = (7 - (_wd.getDay() + 7) % 7);
			_l += Math.ceil((_now - _wd) / 86400000);

			return Math.floor(_l / 7);
		},
		DrawCale: function (nd, wd) {
			var _this = this;
			var _NowTime = (new Date()).format("yyyy-m-d");
			nd = nd == void 0 ? new Date() : nd;
			nd.setDate(1); nd.setHours(0); nd.setMinutes(0); nd.setSeconds(0); nd.setMilliseconds(0);
			wd.setFullYear(nd.getFullYear()); wd.setHours(0); wd.setMinutes(0); wd.setSeconds(0); wd.setMilliseconds(0);
			while (wd > nd) { wd.setFullYear(wd.getFullYear() - 1); }

			var _ul = $("<ul>").addClass("bd_cale_month");
			for (var i = 0; i < 7; i++) {
				var _li = $("<li>").addClass("bd_cale_tn");
				var _i = (i + (7 - _this.opt.WeekOffset)) % 7;
				_li.text(_this.opt.week[_i]);
				if (_i == 5 || _i == 6) _li.addClass("bd_cale_hd");
				_ul.append(_li);
			}
			if (_this.opt.WeekShow) {
				var _li = $("<li>").addClass("bd_cale_tn").addClass("bd_cale_week");
				if (_this.opt.week.length > 7) _li.text(_this.opt.week[7]); else _li.text("周");
				if (_this.opt.WeekAtLast) _ul.append(_li); else _ul.prepend(_li);
			}
			var _blank = $("<li>").addClass("bd_cale_blank");
			_ul.append(_blank);

			var _w = (nd.getDay() + (_this.opt.WeekOffset - 1) + 7) % 7;
			var _d = 31;
			switch (nd.getMonth() + 1) {
				case 2:
					_d = nd.getFullYear() % 4 == 0 ? 29 : 28;
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					_d = 30;
					break;
			};

			_ul.prepend($("<li>").addClass("bd_cale_month").attr("year", nd.getFullYear()).text(_this.opt.month[nd.getMonth()]));
			var _difday = Math.ceil(Math.floor(nd - wd) / 604800000);
			var _qsl = (7 - (_d + _w) % 7) % 7;
			for (var i = 0; i <= _d + _w + _qsl; i++) {
				var _i = i - _w;
				var _li = $("<li>");

				if (_i >= 0 && _i < _d) {
					var _li_dt = "{0}-{1}-{2}".format(nd.getFullYear(), nd.getMonth() + 1, _i + 1);
					_li.text(_i + 1).attr("dt", _li_dt);
					if (_NowTime == _li_dt) _li.addClass("bd_cale_now");
					var _iw = (i + (7 - _this.opt.WeekOffset)) % 7;
					if (_iw == 5 || _iw == 6) _li.addClass("bd_cale_hd");
				}
				_ul.append(_li);

				if ((i + 1) % 7 == 0 || i == 0) {
					if (i > 0) {
						_blank = $("<li>").addClass("bd_cale_blank");
						_ul.append(_blank);
					}
					if (_this.opt.WeekShow && (_difday <= _this.opt.WeekTallyMax || _this.opt.WeekTallyMax <= 0)) {
						if (_this.opt.WeekAtLast) {
							if (i == 6 && _difday - 1 > 0) _difday--;
							if (i > 0) _blank.before($("<li>").addClass("bd_cale_week").text(_difday));
						} else {
							if (_difday < 1) _difday++;
							_blank.after($("<li>").addClass("bd_cale_week").text(_difday));
						}
						_difday++;
					}
				}
			}

			return _ul;
		},
		DrawTools: function () {
			var _this = this;
			var _left = $("<div>").addClass("bd_cale_jt_left");
			var _right = $("<div>").addClass("bd_cale_jt_right");

			return $("<div>").addClass("bd_cale_jt").append(_left).append(_right);
		},
		DrawEvent: function () {
			var _this = this;

			var _own = _this.own;
			if (_this.opt.ToolsShow) {
				_own
					.find("div.bd_cale_jt div.bd_cale_jt_left").off().on({
						click: function () {
							_this.Prev();
						}
					}).end()
					.find("div.bd_cale_jt div.bd_cale_jt_right").off().on({
						click: function () {
							_this.Next();
						}
					});
			}

			_own
				.find("div.bd_cale_own ul.bd_cale_month li[dt]").off().on({
					click: function () { if (_this.opt.OnClick) _this.opt.OnClick($(this)); },
					mouseenter: function () { if (_this.opt.OnHover[0]) _this.opt.OnHover[0]($(this)); },
					mouseleave: function () { if (_this.opt.OnHover[1]) _this.opt.OnHover[1]($(this)); }
				});
		},
		Draw: function (target) {
			target = target ? target : this.own; if (target == null) return;
			var _this = this;
			var _now = new Date(_this.opt.now);
			var _wbg = _this.GetWeekTally(_now);

			_this.obj_own = $("<div>").addClass("bd_cale_own");
			_this.obj_own.append(_this.DrawCale(_now, _wbg));
			_this.lnow.setTime(_now);
			_this.rnow.setTime(_now);
			if (_this.opt.count > 1) {
				if (_this.opt.count % 2) {
					for (var i = 1; i <= Math.floor(_this.opt.count / 2) ; i++) {
						_this.lnow.setMonth(_this.lnow.getMonth() - 1);
						_this.obj_own.prepend(_this.DrawCale(_this.lnow, _wbg));

						_this.rnow.setMonth(_this.rnow.getMonth() + 1);
						_this.obj_own.append(_this.DrawCale(_this.rnow, _wbg));
					}
				} else {
					for (var i = 1; i < _opt.count; i++) {
						_this.rnow.setMonth(_this.rnow.getMonth() + 1);
						_this.obj_own.append(_this.DrawCale(_this.rnow, _wbg));
					}
				}
			}

			target.find("div").remove("div.bd_cale_container, div.bd_cale_jt");
			if (_this.opt.ToolsShow) target.append(_this.DrawTools());
			target.append($("<div>").addClass("bd_cale_container").append(_this.obj_own));

			_this.obj_ul_width = _this.obj_own.find("ul.bd_cale_month").outerWidth(true);

			_this.obj_own
				.prepend($("<ul>").addClass("bd_cale_month")).prepend($("<ul>").addClass("bd_cale_month"))
				.append($("<ul>").addClass("bd_cale_month")).append($("<ul>").addClass("bd_cale_month"))
				.css({ "left": 0 - (_this.obj_ul_width * 2) })
				.width(_this.obj_own.find("ul").length * _this.obj_ul_width);

			if (_this.opt.show)
				target.find("div.bd_cale_container, div.bd_cale_jt").show();
			else
				target.find("div.bd_cale_container, div.bd_cale_jt").hide();

			_this.DrawEvent();
		},
		Next: function () {
			var _this = this;

			if (!_this.lrstate[1]) return; else _this.lrstate[1] = false;
			if (!_this.opt.OnRealRClick) {
				_this.rnow.setMonth(_this.rnow.getMonth() + 1);
				_this.lnow.setMonth(_this.lnow.getMonth() + 1);

				var _wbg = _this.GetWeekTally(_this.rnow);
				_this.obj_own.find("ul.bd_cale_month:eq(-2)").append(_this.DrawCale(_this.rnow, _wbg).find("li"));
				_this.obj_own.animate({ "left": "-=" + _this.obj_ul_width }, {
					duration: _this.opt.speed,
					complete: function () {
						_this.obj_own.find("ul.bd_cale_month:eq(2)").remove();
						_this.obj_own.append($("<ul>").addClass("bd_cale_month")).css({ "left": 0 - (_this.obj_ul_width * 2) });
						if (_this.opt.OnRClick) _this.opt.OnRClick();
						_this.lrstate[1] = true;
					}
				});
				_this.DrawEvent();
			} else {
				_this.opt.OnRealRClick();
				_this.lrstate[1] = true;
			}
		},
		Prev: function () {
			var _this = this;
			

			if (!_this.lrstate[0]) return; else _this.lrstate[0] = false;
			if (!_this.opt.OnRealLClick) {
				_this.lnow.setMonth(_this.lnow.getMonth() - 1);
				_this.rnow.setMonth(_this.rnow.getMonth() - 1);

				var _wbg = _this.GetWeekTally(_this.lnow);
				_this.obj_own.find("ul.bd_cale_month:eq(1)").append(_this.DrawCale(_this.lnow, _wbg).find("li"));
				_this.obj_own.animate({ "left": "+=" + _this.obj_ul_width }, {
					duration: _this.opt.speed,
					complete: function () {
						_this.obj_own.find("ul.bd_cale_month:eq(-3)").remove();
						_this.obj_own.prepend($("<ul>").addClass("bd_cale_month")).css({ "left": 0 - (_this.obj_ul_width * 2) });
						if (_this.opt.OnLClick) _this.opt.OnLClick();
						_this.lrstate[0] = true;
					}
				});

				_this.DrawEvent();
			} else {
				_this.opt.OnRealLClick();
				_this.lrstate[0] = true;
			}
		},
		Rest: function () {
			var _this = this;

			_this.own.find("*").remove();
			_this.Draw(_this.own);
		},
		Show: function (fun) {
			fun = fun == void 0 || typeof(fun) != "function" ? null : fun;
			var _this = this;

			_this.own
				.animate({ height: _this.own.outerHeight(true) + _this.own.find("div.bd_cale_container").outerHeight(true) }, 200, function () {
					_this.own.find("div.bd_cale_container").fadeIn(500, function () {
						_this.own.find("div.bd_cale_jt").show();
						_this.shstate = true;
						if (fun) fun();
					});
				});
		},
		Hide: function (fun) {
			fun = fun == void 0 || typeof (fun) != "function" ? null : fun;
			var _this = this;

			_this.own
				.find("div.bd_cale_jt").hide().end()
				.find("div.bd_cale_container").fadeOut(500, function () {
					_this.own.animate({ height: 10 }, 200, function () {
						_this.Rest();
						_this.shstate = false;
						if (fun) fun();
					});
				});
		},
		Toggle: function (sfun, hfun) {
			var _this = this;

			if (_this.shstate) {
				_this.Hide(hfun);
			} else {
				_this.Show(sfun);
			}
		}
	});
})(jQuery);