﻿$.BDialog = function (opt, d) {
	d = d == void 0 ? null : d;
	opt = opt == void 0 || typeof (opt) != "object" ? {} : opt;
	var _opt = $.extend({ type: "mask" }, opt);

	switch (_opt.type.toLowerCase()) {
		case "mask":
			_opt = $.extend({
				type: "mask", style: "", speed: 300,
				OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
			}, _opt);
			break;
		case "modal":
			_opt = $.extend({
				type: "modal", speed: 300, mask: null,
				closebtn: { state: true, origin: "rt", position: { x: 14, y: -14 } },
				OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
			}, _opt);
			break;
		case "ajax":
			_opt = $.extend({
				type: "ajax", speed: 300, mask: null,
				closebtn: { state: true, origin: "rt", position: { x: 14, y: -14 } },
				OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
			}, _opt);
			break;
		case "alert":
			_opt = $.extend({
				type: "alert", speed: 300, mask: null,
				title: "",
				closebtn: { state: true, origin: "rt", position: { x: 14, y: -14 } },
				button: [
					{ text: "确认", result: 1, closed: false, def: true }
				],
				OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
			}, _opt);
			break;
		case "confirm":
			_opt = $.extend({
				type: "confirm", speed: 300, mask: null,
				title: "",
				closebtn: { state: true, origin: "rt", position: { x: 14, y: -14 } },
				button: [
					{ text: "取消", result: 2, closed: false, def: false },
					{ text: "确认", result: 1, closed: false, def: true }
				],
				OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
			}, _opt);
			break;
	}

	var _this = null;
	switch (_opt.type.toLowerCase()) {
		case "mask":
			_this = new TBDialog_Mask(_opt);
			break;
		case "modal":
			_this = new TBDialog_Modal(_opt, d);
			break;
		case "ajax":
			_this = new TBDialog_AJAX(_opt, d);
			break;
		case "iframe":
			_this = new TBDialog_iFrame(_opt, d);
			break;
		case "alert":
		case "confirm":
		default:
			_this = new TBDialog_Alert(_opt, d == null ? "" : d);
			break;
	}

	return _this;
}

$.fn.BDialog = function (opt) {
	return $.BDialog(opt, $(this));
}

function TBDialog(opt) { return this.Create(opt); }
TBDialog.prototype = (new TObject()).extend({
	opt: null, own: null,
	Create: function (opt) {
		var _this = this;
		_this.opt = opt == void 0 ? null : opt;

		return _this;
	},
	Draw: function (own) { },
	DrawEvent: function () { },
	Show: function () { },
	Hide: function () { },
	Resize: function () { }
});

function TBDialog_Mask(opt) { return this.Create(opt); }
TBDialog_Mask.prototype = (new TBDialog()).extend({
	DrawEvent: function () {
		var _this = this;

		$(window)
			.off("resize", _this.Resize)
			.on("resize", { opt: _this.opt, own: _this.own }, _this.Resize);

		_this.own
			.off().on({
				click: function () { if (_this.opt.OnClick) _this.opt.OnClick(_this); }
			});
	},
	DrawData: function (own) {
		var _div = $("<div class=\"bd_dialog_mask\">").prop("id", "BDMask_" + (new Date()).format("m_d_h_n_s_z"));
		own.append(_div);

		return _div;
	},
	Resize: function (e) {
		var _wnd = $(window);
		var _wps = { w: _wnd.width(), h: _wnd.height() };
		var _ps = { w: $(document).width(), h: $(document).height() };
		_ps.w = _wps.w > _ps.w ? _wps.w : _ps.w;
		_ps.h = _wps.h > _ps.h ? _wps.h : _ps.h;

		if (e.data.opt.style != "") e.data.own.prop("style", e.data.opt.style);
		e.data.own
			.width(_ps.w).height(_ps.h)
			.css({ left: 0, top: 0 });

		return e.data.own;
	},
	Show: function (speed) {
		var _this = this;
		var _dtd = $.Deferred();
		speed = speed == void 0 || typeof (speed) != "number" ? _this.opt.speed : speed;

		if (_this.own == null) {
			_this.own = _this.DrawData($("body"));
			_this.DrawEvent();
		}

		$(window).resize();
		_this.own.fadeTo(0, 0.1).fadeTo(speed, 1, function () {
			if (_this.opt.OnShow) _this.opt.OnShow();
			_dtd.resolve();
		});

		return _dtd.promise();
	},
	Hide: function (speed) {
		var _this = this;
		var _dtd = $.Deferred();
		speed = speed == void 0 || typeof (speed) != "number" ? _this.opt.speed : speed;

		if (_this.own != null) {
			_this.own.fadeTo(speed, 0.1, function () {
				_this.own.remove();
				_this.own = null;
				if (_this.opt.OnHide) _this.opt.OnHide();
				_dtd.resolve();
			});
		}

		return _dtd.promise();
	}
});

function TBDialog_Modal(opt, own) { return this.Create(opt, own); }
TBDialog_Modal.prototype = (new TBDialog()).extend({
	mask: null, data: null,
	dstate: false, dpos: null, dcls: "bd_dm",
	dtd: null,
	Create: function (opt, own) {
		var _this = this;
		_this.opt = opt == void 0 ? null : opt;
		_this.own = own == void 0 ? null : own;

		var _mopt = $.extend({
			type: "mask", style: "", speed: 300,
			OnShow: null, OnHide: null, OnClose: null, OnSubmit: null
		}, _this.opt ? _this.opt.mask : null);
		_this.mask = $.BDialog(_mopt);
		_this.mask.opt.OnShow = null;
		_this.dpos = { lr: 0, tb: 0, x: 0, y: 0, w: 0, h: 0 };

		return _this;
	},
	Resize: function (e) {
		var _wnd = $(window);
		var _ws = { x: _wnd.scrollLeft(), y: _wnd.scrollTop() };
		var _wps = { w: _wnd.width(), h: _wnd.height(), x: 0, y: 0 };
		var _bps = { w: $("body").width(), h: $("body").height() };
		//_ps.w = _wps.w > _ps.w ? _wps.w : _ps.w;
		//_ps.h = _wps.h > _ps.h ? _wps.h : _ps.h;


		if (e.data.own != null) {
			e.data.own

			var _d = e.data.own;
			var _dps = e.data.pos;
			_dps.w = _d.outerWidth();
			_dps.h = _d.outerHeight();

			_wps.x = parseInt((_wps.w - _dps.w) / 2) + _ws.x;
			_wps.y = parseInt((_wps.h - _dps.h) / 2) + _ws.y;

			if (_dps.lr == 0) {
				if (_dps.w > _wps.w) {
					if (_dps.w < _bps.w)
						_dps.lr = _bps.w - _dps.w > _wps.x ? 1 : 2;
					else
						_dps.lr = 1;
				} else
					_dps.lr = 9;
			}

			if (_dps.tb == 0) {
				if (_dps.h > _wps.h) {
					if (_dps.h < _bps.h) {
						_dps.tb = _bps.h - _dps.h > _wps.y ? 1 : 2;
					} else
						_dps.tb = 1;
				} else
					_dps.tb = 9;
			}

			switch (_dps.lr) {
				case 1:
				case 2:
				case 3:
					if (_ws.x > _dps.x) {
						_dps.lr = _dps.w - (_ws.x - _dps.x + _wps.w) > 0 ? 3 : 2;
						_wps.x = _dps.lr == 3 ? _dps.x : (_ws.x - (_dps.w - _wps.w));
					} else {
						_dps.lr = _dps.w - (_ws.x - _dps.x + _wps.x) > 0 ? 3 : 1;
						_wps.x = _dps.lr == 3 ? _dps.x : _ws.x;
					}
					break;
			}

			switch (_dps.tb) {
				case 1:
				case 2:
				case 3:
					if (_ws.y > _dps.y) {
						_dps.tb = _dps.h - (_ws.y - _dps.y + _wps.h) > 0 ? 3 : 2;
						_wps.y = _dps.tb == 3 ? _dps.y : (_ws.y - (_dps.h - _wps.h));
					} else if (_ws.y < _dps.y) {
						_dps.tb = _dps.h - (_ws.y - _dps.y + _wps.y) > 0 ? 3 : 1;
						_wps.y = _dps.tb == 3 ? _dps.y : _ws.y;
					} else {
						_wps.y = _ws.y + Math.abs(e.data.opt.closebtn.position.y) + 10;
					}
					break;
			}

			_d.css({ left: _wps.x, top: _wps.y });

			if (e.data.opt.closebtn.state) {
				var _cb = _d.find("div.bd_dialog_button_close");
				var _cb_pos = { x: 0, y: 0, w: _cb.outerWidth(), h: _cb.outerHeight() };

				switch (e.data.opt.closebtn.origin.toLowerCase()) {
					case "lt":
						_cb_pos.x = 0;
						_cb_pos.y = 0;
						break;
					case "rt":
					default:
						_cb_pos.x = _dps.w - _cb_pos.w;
						_cb_pos.y = 0;
						break;
					case "lb":
						_cb_pos.x = 0;
						_cb_pos.y = _dps.h - _cb_pos.h;
						break;
					case "rb":
						_cb_pos.x = _dps.w - _cb_pos.w;
						_cb_pos.y = _dps.h - _cb_pos.h;
						break;
				}
				_cb_pos.x += e.data.opt.closebtn.position.x;
				_cb_pos.y += e.data.opt.closebtn.position.y;

				_cb.css({ "margin-left": _cb_pos.x, "margin-top": _cb_pos.y });
			}

			_dps.x = _ws.x;
			_dps.y = _ws.y;
		}

		return e.data.own;
	},
	DrawEvent: function (b) {
		var _this = this;
		b = b == void 0 || typeof (b) != "boolean" ? false : b;

		if (b) {
			_this.dpos.x = $(window).scrollLeft();
			_this.dpos.y = $(window).scrollTop();

			$(window)
				.off("resize", _this.Resize)
				.off("scroll", _this.Resize)
				.on("resize", { opt: _this.opt, own: _this.data, pos: _this.dpos }, _this.Resize)
				.on("scroll", { opt: _this.opt, own: _this.data, pos: _this.dpos }, _this.Resize);

			_this.data.off().find("*").off();
			_this.data
				.on({
					click: function () { if (_this.opt.OnClick) _this.opt.OnClick(_this); }
				})
				.find("div.bd_dialog_button_close").on({
					click: function () {
						if (_this.opt.OnSubmit) {
							var _dtd = $.Deferred();
							_dtd.promise()
								.done(function () { _this.Hide(0); })
								.always(function () { _dtd = null; });
							_this.opt.OnSubmit(_dtd, -1);
						} else
							_this.Hide(0);
					}
				}).end()
				.find("li.bd_dialog_button_custom").on({
					click: function () {
						var _bc_result = parseInt($(this).attr("result"));
						if ($(this).attr("closed") == true)
							_this.Hide(_bc_result);
						else {
							if (_this.opt.OnSubmit) {
								var _dtd = $.Deferred();
								_dtd.promise()
									.done(function () { _this.Hide(_bc_result); })
									.always(function () { _dtd = null; });
								_this.opt.OnSubmit(_dtd, _bc_result);
							} else
								_this.Hide(_bc_result);
						}
					}
				});
		} else {
			$(window)
				.off("resize", _this.Resize)
				.off("scroll", _this.Resize);
			_this.data.off().find("*").off();
		}
	},
	DrawOwn: function (own, obj) {
		var _this = this;
		var _div = [$("<div class=\"bd_dialog_modal\">").addClass(_this.dcls).prop("id", "BDModal_" + (new Date()).format("m_d_h_n_s_z")), $("<div class=\"bd_dialog_modal_container\">"), $("<ul class=\"bd_dialog_modal_button box_h\">")];

		if (_this.opt.closebtn.state) {
			var _cb = $("<div class=\"bd_dialog_button_close\">").text("关闭");
			_div[0].append(_cb);
		}

		if (_this.opt.button) {
			for (var i = 0; i < _this.opt.button.length; i++) {
				var _db = _this.opt.button[i];
				_div[2].append($("<li>").addClass(_db.def ? "bd_dialog_button_custom def" : "bd_dialog_button_custom").text(_db.text).attr({ result: _db.result, closed: _db.closed }));
			}
		}

		own
			.find("div.bd_dialog_modal").remove().end()
			.append(_div[0].append(_div[1].append(obj)).append(_div[2]));

		return _div[0];
	},
	DrawData: function (d) {
		var _this = this;

		if (d instanceof jQuery) {
			return d;
		} else {
			return $("<div class=\"bd_dialog_mc_context\">").text(d);
		}
	},
	Show: function (speed, fun) {
		var _this = this;
		speed = speed == void 0 || typeof (speed) != "number" ? _this.opt.speed : speed;
		fun = fun == void 0 || typeof (fun) != "function" ? null : fun;

		if (!_this.dstate) {
			_this.dtd = $.Deferred();
			_this.dstate = true;
			_this.mask.Show(speed == 0 ? 0 : 100).done(function () {
				if (_this.own != null) {
					_this.data = _this.DrawOwn(_this.mask.own, _this.DrawData(_this.own));

					_this.DrawEvent(true);
					$(window).resize();

					_this.data.hide();
					switch (_this.opt.mode) {
						case 1:
							_this.data
								.show().css({ top: "-=20", opacity: 0.4 })
								.animate({ top: "+=20", opacity: 1 }, speed, function () {
									if (_this.opt.OnShow) _this.opt.OnShow();
									if (fun) fun();
								});
							break;
						default:
							_this.data.show();

							if (_this.opt.OnShow) _this.opt.OnShow();
							if (fun) fun();
							break;
					}
				} else
					_this.dtd.reject();
			});
		}

		return _this.dtd.promise();
	},
	Hide: function (r, speed) {
		var _this = this;
		r = r == void 0 || typeof (r) != "number" ? 0 : r;
		speed = speed == void 0 || typeof (speed) != "number" ? _this.opt.speed : speed;

		if (_this.dstate) {
			_this.dstate = false;
			if (_this.data != null) {
				_this.DrawEvent();
				switch (_this.opt.mode) {
					case 1:
						_this.data
							.animate({ top: "-=20", opacity: 0.4 }, speed, function () {
								_this.mask.Hide(speed == 0 ? 0 : 100).done(function () {
									if (_this.opt.OnHide) _this.opt.OnHide();
									_this.dtd.resolve(r);
									_this.dtd = null;
								});
							});
						break;
					default:
						_this.data.hide();
						_this.mask.Hide(speed == 0 ? 0 : 100).done(function () {
							if (_this.opt.OnHide) _this.opt.OnHide();
							_this.dtd.resolve(r);
						});
						break;
				}
			} else
				_this.dtd.resolve(r);
		} else
			_this.dtd.resolve(0);
	}
});

function TBDialog_Alert(opt, data) {
	return this.Create(opt, data);
}
TBDialog_Alert.prototype = (new TBDialog_Modal()).extend({
	dcls: "bd_dm_alert",
	DrawData: function (d) {
		var _this = this;

		if (d instanceof jQuery) {
			return d;
		} else {
			var _div = $("<div class=\"bd_dialog_mc_context\">");

			switch (typeof (d)) {
				case "string":
					_div
						.append($("<span class=\"bd_dialog_mcc_ico\">").addClass(_this.opt.type))
						.append($("<span class=\"bd_dialog_mcc_text\">").text(d));
					break;
				case "object":
					if (d == null) { }
					else if (d instanceof Array) { }
					else if (d instanceof Object) {
						_div
							.append($("<span class=\"bd_dialog_mcc_ico\">").addClass(d.ico ? d.ico : _this.opt.type))
							.append($("<span class=\"bd_dialog_mcc_text\">").text(d.text));
					}
					break;
			}

			//return $("<div>").addClass("bd_dialog_mc_main").append($("<div>").addClass("bd_dialog_mc_main_own").append(_div));
			return $("<div class=\"bd_dialog_mc_main\">").append(_div);
		}
	}
});

function TBDialog_AJAX(opt, data) {
	return this.Create(opt, data);
}
TBDialog_AJAX.prototype = (new TBDialog_Modal()).extend({
	dcls: "bd_dm_ajax"
});

function TBDialog_iFrame(opt, data) { return this.Create(opt, data); }
TBDialog_iFrame.prototype = (new TBDialog_Modal()).extend({});