/**
 * ==========================================
 * Name:           jBD‘s Loading
 * Author:         Buddy-Deus
 * CreTime:        2016-05-10
 * Description:    jBD Loading Web
 * Log
 * 2016-05-10    初始化加载列表
 * 2016-11-24    基于ES6改写
 * ==========================================
 */
(jBD => {
	if (jBD.mode & jBD.MODE.Web) {
		jBD.__require("./lib/web/dom", "DOM");
		jBD.__require("./lib/web/request", "Request");
		jBD.__require("./lib/web/cookie", "Cookie");
		jBD.__require("./lib/web/storage", "Storage");
		jBD.__require("./lib/web/history", "History");
	}

	if (jBD.mode & jBD.MODE.WebDesktop) {

	}

	if (jBD.mode & jBD.MODE.WebMobile) {

	}
})(this);