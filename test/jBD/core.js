let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

target = require("../../jBD/core.js");

factory("type", TYPE, target.type);
factory("is", IS, target.is);
factory("isSimple", isSimple, target.isSimple);
factory("isNull", isNull, target.isNull);
factory("isBool", isBool, target.isBool);
factory("isNumber", isNumber, target.isNumber);
factory("isString", isString, target.isString);
factory("isFunction", isFunction, target.isFunction);
factory("isArray", isArray, target.isArray);
factory("isObject", isObject, target.isObject);
factory("isJSON", isJSON, target.isJSON);
factory("isDate", isDate, target.isDate);
factory("isTime", isTime, target.isTime);
factory("isGuid", isGuid, target.isGuid);
factory("callback", callback, target.callback);
factory("each", each, target.each, [target.type]);
factory("unique", unique, target.unique);
factory("has", has, target.has);
factory("clone", clone, target.clone);

function TYPE (func) {
	block("default, detail=false", () => {
		can("null", () => {
			equal(func(null), "null");
		});
		can("undefined", () => {
			equal(func(undefined), "null");
		});
		can("array/date", () => {
			equal(func([]), "object");
			equal(func(new Date()), "object");
		});
	});
	block("detail=true", opt => {
		opt = true;
		can("boolean", () => {
			equal(func(true, opt), "boolean");
		});
		can("number", () => {
			equal(func(1, opt), "number");
			equal(func(-1, opt), "number");
			equal(func(0, opt), "number");
			equal(func(NaN, opt), "number");
		});
		can("string", () => {
			equal(func("", opt), "string");
			equal(func("1", opt), "string");
		});
		can("null", () => {
			equal(func(null, opt), "null");
		});
		can("undefined", () => {
			equal(func(undefined, opt), "undefined");
		});
		can("function", () => {
			equal(func(() => {}, opt), "function");
			equal(func(function () {}, opt), "function");
		});
		can("object", () => {
			equal(func({}, opt), "object");
		});
		can("array", () => {
			equal(func([], opt), "array");
		});
		can("regexp", () => {
			equal(func(/1/g, opt), "regexp");
			equal(func(new RegExp("asd"), opt), "regexp");
		});
	});
}

function IS (func) {
	block("default", () => {
		can("function, 绝对相同", () => {
			let a = function () { alert(1); },
				b = function () { alert(1); };
			equal(func(a, b), false);
		});
		can("object, 绝对相同", () => {
			let a = {a: 1},
				b = {a: 1};
			equal(func(a, b), false);
		});
		can("date, 绝对相同", () => {
			let a = new Date(),
				b = new Date(a.getTime());
			equal(func(a, b), false);
		});
	});
	block("abs=false", opt => {
		opt = false;
		can("function, toString相同", () => {
			let a = function () { alert(1); },
				b = function () { alert(1); };
			equal(func(a, b, opt), true);
		});
		can("object, key/value相同", () => {
			let a = {a: 1},
				b = {a: 1};
			equal(func(a, b, opt), true);
		});
		can("date, getTime相同", () => {
			let a = new Date(),
				b = new Date(a.getTime());
			equal(func(a, b, opt), true);
		});
	});
}

function isSimple (func) {
	can("简单类型, boolean/number/string/undefined/null", () => {
		equal(func(true), true);
		equal(func(0), true);
		equal(func(1), true);
		equal(func(NaN), true);
		equal(func(""), true);
		equal(func("1"), true);
		equal(func(undefined), true);
		equal(func(null), true);
	});
	can("复杂类型, function/array/object", () => {
		equal(func(function () {}), false);
		equal(func(() => {}), false);
		equal(func([]), false);
		equal(func({}), false);
		equal(func(new Date()), false);
	});
}

function isNull (func) {
	block("default, opt={udf:true,obj:false}", () => {
		can("undefined", () => {
			equal(func(undefined), true);
		});
		can("object", () => {
			equal(func({}), false);
		});
		can("array", () => {
			equal(func([]), false);
		});
	});
	block("opt={udf:false,obj:true}", opt => {
		opt = {udf: false, obj: true};
		can("undefined", () => {
			equal(func(undefined, opt), false);
		});
		can("object", () => {
			equal(func({}, opt), true);
			equal(func({a: 1}, opt), false);
		});
		can("array", () => {
			equal(func([], opt), true);
			equal(func([1], opt), false);
		});
	});
	block("opt=false, opt={udf:opt,obj:false}", opt => {
		opt = false;
		can("undefined", () => {
			equal(func(undefined, opt), false);
		});
		can("object", () => {
			equal(func({}, opt), false);
		});
	});
}

function isBool (func) {
	block("default, str=false", () => {
		can("string", () => {
			equal(func("true"), false);
			equal(func(""), false);
		});
		can("any", () => {
			equal(func(false), true);
			equal(func(0), false);
			equal(func(null), false);
		});
	});
	block("str=true", opt => {
		opt = true;
		can("string", () => {
			equal(func("true", opt), true);
			equal(func("false", opt), true);
			equal(func("", opt), false);
			equal(func("1", opt), false);
		});
	});
}

function isNumber (func) {
	block("default, opt={nan:false,str:false}", () => {
		can("number", () => {
			equal(func(1), true);
			equal(func(0), true);
			equal(func(NaN), false);
		});
		can("string", () => {
			equal(func("1"), false);
		});
	});
	block("opt={nan:true,str:true}", opt => {
		opt = {nan: true, str: true};
		can("number", () => {
			equal(func(1, opt), true);
			equal(func(0, opt), true);
			equal(func(NaN, opt), true);
		});
		can("string", () => {
			equal(func("1", opt), true);
			equal(func("0", opt), true);
			equal(func("-1", opt), true);
			equal(func("1.1", opt), true);
			equal(func("-1.1", opt), true);
			equal(func("", opt), false);
		});
	});
	block("opt=true, opt={nan:opt,str:false}", opt => {
		opt = true;
		can("number", () => {
			equal(func(1, opt), true);
			equal(func(0, opt), true);
			equal(func(NaN, opt), true);
		});
		can("string", () => {
			equal(func("1", opt), false);
			equal(func("0", opt), false);
			equal(func("-1", opt), false);
			equal(func("1.1", opt), false);
			equal(func("-1.1", opt), false);
			equal(func("", opt), false);
		});
	});
}

function isString (func) {
	block("default, empty=false", () => {
		can("string", () => {
			equal(func("1"), true);
			equal(func(""), false);
		});
		can("any", () => {
			equal(func(1), false);
			equal(func(null), false);
			equal(func({}), false);
		});
	});
	block("empty=true", opt => {
		opt = true;
		can("string", () => {
			equal(func("1", opt), true);
			equal(func("", opt), true);
		});
		can("any", () => {
			equal(func(1, opt), false);
			equal(func(null, opt), false);
			equal(func({}, opt), false);
		});
	});
}

function isFunction (func) {
	can("any", () => {
		equal(func(function () {}), true);
		equal(func(() => {}), true);
	});
}

function isArray (func) {
	block("default, arg=false", () => {
		can("array", () => {
			equal(func([]), true);
		});
		can("arguments", () => {
			(function () {
				equal(func(arguments), false);
			})();
		});
	});
	block("arg=true", opt => {
		opt = true;
		can("array", () => {
			equal(func([], opt), true);
		});
		can("arguments", () => {
			(function () {
				equal(func(arguments, opt), true);
			})();
		});
	});
}

function isObject (func) {
	block("default, arg=object", () => {
		can("object", () => {
			equal(func({}), true);
			equal(func(null), false);
			equal(func([]), false);
			equal(func(new Date()), false);
		});
		can("any", () => {
			equal(func(1), false);
			equal(func(""), false);
			equal(func(function () {}), false);
		});
	});
	block("arg=any|true", opt => {
		opt = "any";
		can("object", () => {
			equal(func({}, opt), true);
			equal(func(null, opt), true);
			equal(func([], opt), true);
			equal(func(new Date(), opt), true);
		});
		can("any", () => {
			equal(func(1, opt), false);
			equal(func("", opt), false);
			equal(func(function () {}, opt), false);
		});
	});
	block("arg=false", opt => {
		opt = false;
		can("object", () => {
			equal(func({}, opt), true);
			equal(func(null, opt), false);
			equal(func([], opt), true);
			equal(func(new Date(), opt), true);
		});
		can("any", () => {
			equal(func(1, opt), false);
			equal(func("", opt), false);
			equal(func(function () {}, opt), false);
		});
	});
}

function isJSON (func) {
	let a = "{}",
		b = "[1,2]",
		c = "{a:true,b:undefined,c:null,d:1,e:'111',f:'{a:1}',g:[1,2],h:{a:1}}";

	can("a:" + a, () => {
		equal(func(a), true);
	});
	can("b:" + b, () => {
		equal(func(b), true);
	});
	can("c:" + c, () => {
		equal(func(c), true);
	});
}

function isDate (func) {
	block("default, str=false", () => {
		can("date", () => {
			equal(func(new Date("2015-1-1")), true);
		});
		can("string", () => {
			equal(func("2015-1-1"), false);
		});
	});
	block("str=true", opt => {
		opt = true;
		can("date", () => {
			equal(func("0000-0-0", opt), false);
			equal(func("1993-02-03", opt), true);
			equal(func("2012-2-31", opt), false);
			equal(func("1800-1-1", opt), false);
			equal(func("1900-2-29", opt), true);
			equal(func("2012-1-31", opt), true);
			equal(func("2012-4-30", opt), true);
			equal(func("2012/4/30", opt), true);
		});
		can("time", () => {
			equal(func("0:0:0", opt), false);
			equal(func("1:1:1", opt), false);
		});
		can("date+time", () => {
			equal(func("2012-4-30 1:1:1", opt), true);
			equal(func("2012-4-30 1:1:1.1", opt), true);
			equal(func("2012-4-30 1:1:1:1", opt), true);
			equal(func("2012-4-30 24:0:0", opt), false);
			equal(func("2012-4-30 23:60:0", opt), false);
		});
	});
}

function isTime (func) {
	block("default, str=false", () => {
		can("date", () => {
			equal(func(new Date("1:1:1")), true);
		});
		can("string", () => {
			equal(func("1:1:1"), false);
		});
	});
	block("str=true", opt => {
		opt = true;
		can("time", () => {
			equal(func("0:0:0", opt), true);
			equal(func("0:0:0.0", opt), true);
			equal(func("1:1:1:1", opt), true);
			equal(func("1:1:1:1000", opt), false);
			equal(func("24:0:0", opt), false);
			equal(func("23:60:0", opt), false);
		});
	});
}

function isGuid (func) {
	can("00000000-0000-0000-0000-000000000000", () => {
		equal(func("00000000-0000-0000-0000-000000000000"), true);
	});
	can("4B0F57E0-545D-4080-B9BA-88DF24DC2110", () => {
		equal(func("4B0F57E0-545D-4080-B9BA-88DF24DC2110"), true);
	});
	can("4b0f57e0-545d-4080-b9ba-88df24dc2110", () => {
		equal(func("4b0f57e0-545d-4080-b9ba-88df24dc2110"), true);
	});
	can("{4b0f57e0-545d-4080-b9ba-88df24dc2110}", () => {
		equal(func("{4b0f57e0-545d-4080-b9ba-88df24dc2110}"), true);
	});
	can("4b0f57e0-545d-4080-b9ba-88df24dc21101", () => {
		equal(func("4b0f57e0-545d-4080-b9ba-88df24dc21101"), false);
	});
	can("4b0X57e0-545d-4080-b9ba-88df24dc2110", () => {
		equal(func("4b0X57e0-545d-4080-b9ba-88df24dc2110"), false);
	});
}

function callback (func) {
	let fn = v => typeof (v) == "function",
		a  = [function () {}],
		b  = [2, function () {}],
		c  = [2, function () {}, 1];

	block("default, index=0", () => {
		can("[fn]", () => {
			equal(fn(func(a)), true);
		});
		can("[2,fn]", () => {
			equal(fn(func(b)), true);
		});
		can("[2,fn,2]", () => {
			equal(fn(func(c)), false);
		});
	});
	block("index=1", opt => {
		opt = 1;
		can("[fn]", () => {
			equal(fn(func(a, opt)), false);
		});
		can("[2,fn]", () => {
			equal(fn(func(b, opt)), true);
		});
		can("[2,fn,2]", () => {
			equal(fn(func(c, opt)), false);
		});
	});
}

function each (func, type) {
	let a = [1, "2", null, undefined, {}],
		b = {a: 1, b: "2", c: null, d: undefined, e: {}},
		c = "12345678";

	block("default, opt={detail:false,force:false,index:0,desc:false}", () => {
		can("array", () => {
			func(a, (d, k) => {
				equal(a[k], d);
			});
		});
		can("object", () => {
			func(b, (d, k) => {
				equal(b[k], d);
			});
		});
		can("string", () => {
			func(c, (d, k) => {
				equal(c[k], d);
			});
		});
	});
	block("opt={detail:true}", opt => {
		opt = {detail: true};
		can("array", () => {
			func(a, (d, k, t) => {
				equal(a[k], d);
				equal(type(d, true), t);
			}, opt);
		});
		can("object", () => {
			func(b, (d, k, t) => {
				equal(b[k], d);
				equal(type(d, true), t);
			}, opt);
		});
		can("string", () => {
			func(c, (d, k, t) => {
				equal(c[k], d);
				equal(type(d, true), t);
			}, opt);
		});
	});
	block("opt={force:true,index:1,desc:true}", opt => {
		opt = {force: true, index: 1, desc: true};
		can("array", () => {
			let l = a.length - 2;
			func(a, (d, k) => {
				equal(a[k], a[l]);
				l--;
			}, opt);
		});
		can("string", () => {
			let l = c.length - 2;
			func(c, (d, k) => {
				equal(c[k], c[l]);
				l--;
			}, opt);
		});
	});
	block("opt=true, opt={detail:opt}", opt => {
		opt = true;
		can("array", () => {
			func(a, (d, k, t) => {
				equal(a[k], d);
				equal(type(d, true), t);
			}, opt);
		});
		can("object", () => {
			func(b, (d, k, t) => {
				equal(b[k], d);
				equal(type(d, true), t);
			}, opt);
		});
		can("string", () => {
			func(c, (d, k, t) => {
				equal(c[k], d);
				equal(type(d, true), t);
			}, opt);
		});
	});
	block("opt=1, opt={index:opt}", opt => {
		opt = 1;
		can("array", () => {
			let l = 1;
			func(a, (d, k) => {
				equal(a[k], a[l]);
				l++;
			}, opt);
		});
		can("string", () => {
			let l = 1;
			func(c, (d, k) => {
				equal(c[k], c[l]);
				l++;
			}, opt);
		});
	});
}

function unique (func) {
	let a = [1, 2, 5, 6, 3, 6, 2],
		b = [1, 2, 5, 6, 3],
		c;

	block("default, write=false", () => {
		can("unique", () => {
			c = func(a);
			dequal(c, b);
		});
		can("write", () => {
			c = func(a);
			dnequal(a, c);
		});
	});
	block("write=true", opt => {
		opt = true;
		can("unique", () => {
			c = func(a, opt);
			dequal(c, b);
		});
		can("write", () => {
			c = func(a, opt);
			dequal(c, b);
			dequal(a, c);
		});
	});
}

function has (func) {
	let a = {a: 1},
		b = [1];

	can("object", () => {
		equal(func(1, a), true);
		equal(func(2, a), false);
	});
	can("array", () => {
		equal(func(1, b), true);
		equal(func(2, b), false);
	});
}

function clone (func) {
	block("default, opt={deep:false,write:true}", () => {
		can("array", () => {
			let a = [1, 3, 6],
				b = [8, 3],
				c = [1, 3, 6],
				d;

			d = func(a, b);
			dequal(d, c);
			sequal(d, b);
		});
		can("object", () => {
			let a = {a: 1, b: 3},
				b = {a: 8, c: 3},
				c = {a: 1, b: 3, c: 3},
				d;

			d = func(a, b);
			dequal(d, c);
			sequal(d, b);
		});
	});
	block("opt={deep:true,write:false}", opt => {
		opt = {deep: true, write: false};
		can("array", () => {
			let a = [1, 3, 6, [7]],
				b = [8, 3],
				c = [1, 3, 6, [7]],
				d;

			d = func(a, b, opt);
			dequal(d, c);
			snequal(d, b);
		});
		can("object", () => {
			let a = {a: 1, b: 3, c: {a: 1}},
				b = {a: 8, c: 3},
				c = {a: 1, b: 3, c: {a: 1}},
				d;

			d = func(a, b, opt);
			dequal(d, c);
			snequal(d, b);
		});
	});
	block("opt=true, opt={deep:opt}", opt => {
		opt = true;
		can("array", () => {
			let a = [1, 3, 6, [7]],
				b = [8, 3],
				c = [1, 3, 6, [7]],
				d;

			d = func(a, b, opt);
			dequal(d, c);
			sequal(d, b);
		});
		can("object", () => {
			let a = {a: 1, b: 3, c: {a: 1}},
				b = {a: 8, c: 3},
				c = {a: 1, b: 3, c: {a: 1}},
				d;

			d = func(a, b, opt);
			dequal(d, c);
			sequal(d, b);
		});
	});
}