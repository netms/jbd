let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../jBD/core.js");
target = require("../../jBD/class.js");

factory("TSeal", TSeal, target.TSeal);
factory("TObject", TObject, target.TObject);
factory("TList", TList, target.TList);
factory("TChinaDate", TChinaDate, target.TChinaDate);
factory("TGuid", TGuid, target.TGuid);
factory("ChinaDate", ChinaDate, target.ChinaDate);
factory("NewGuid", NewGuid, target.NewGuid);
factory("EmptyGuid", EmptyGuid, target.EmptyGuid);

function TSeal (func) {
	block("class prop/function", () => {
		can("className = TSeal", () => {
			equal(func.className, "TSeal");
		});
	});
	block("object prop/function", () => {
		let api      = {
				className: "TTest",
				create:    function (v) { this.name = this.className + " " + v; },
				test:      function () { return this.name; }
			},
			child    = func.extend(api),
			childObj = new child("name"),
			cloneObj = childObj.clone();

		block("内容完整性", () => {
			can("属性完整", () => {
				for (let k in childObj) {
					switch (k) {
						default:
							fail();
							return;
						case "name":
						case "test":
							break;
						case "create":
						case "free":
						case "clone":
						case "className":
						case "__error__":
						case "errorMax":
						case "lastError":
						case "getError":
						case "setError":
							break;
					}
				}
			});
			can("内容正确", () => {
				/^function ([a-zA-Z][a-zA-Z0-9_-]*)/.exec(child.toString());
				equal(RegExp.$1, childObj.className);
				equal(childObj.className, "TTest");
				equal(childObj.name, "TTest name");
				equal(childObj.test(), "TTest name");
			});
			can("副本正确", () => {
				for (let k in childObj) {
					if (!cloneObj.hasOwnProperty(k)) fail();
				}
				snequal(cloneObj, childObj);
			});
		});
		block("类密闭性", () => {
			can("是否存在extend函数", () => {
				if (typeof(child.extend) == "function") fail();
			});
		});
	});
}

function TObject (func) {
	block("class prop/function", () => {
		can("className = TObject", () => {
			equal(func.className, "TObject");
		});
	});
	block("object prop/function", () => {
		let api      = {
				className: "TTest",
				create:    function (v) { this.name = this.className + " " + v; },
				test:      function () { return this.name; }
			},
			child    = func.extend(api),
			childObj = new child("name"),
			cloneObj = childObj.clone();

		block("内容完整性", () => {
			can("属性完整", () => {
				for (let k in childObj) {
					switch (k) {
						default:
							fail();
							return;
						case "name":
						case "test":
							break;
						case "create":
						case "free":
						case "super":
						case "clone":
						case "className":
						case "__super__":
						case "__error__":
						case "errorMax":
						case "lastError":
						case "getError":
						case "setError":
							break;
					}
				}
			});
			can("内容正确", () => {
				/^function ([a-zA-Z][a-zA-Z0-9_-]*)/.exec(child.toString());
				equal(RegExp.$1, childObj.className);
				equal(childObj.className, "TTest");
				equal(childObj.name, "TTest name");
				equal(childObj.test(), "TTest name");
			});
			can("父类函数", () => {
				equal(childObj.super("className"), "TObject");
			});
			can("副本正确", () => {
				for (let k in childObj) {
					if (!cloneObj.hasOwnProperty(k)) fail();
				}
				snequal(cloneObj, childObj);
			});
		});
		block("类密闭性", () => {
			can("是否存在extend函数", () => {
				if (typeof(child.extend) != "function") fail();
			});
		});
	});
}

function TList (func) {
	let obj = new func();

	block("object prop/function", () => {

	});
}

function TChinaDate (func) {
	let dt1 = new Date(2017, 0, 20),
		dt2 = new Date(2017, 1, 3);

	block("class prop/function", () => {
		can("toString = 丙申年(鸡) 腊月 廿三 大寒", () => {
			equal(func.toString(dt1), "丙申年(鸡) 腊月 廿三 大寒");
			equal(func.toString("CY年 CM月 CD日", dt1), "丙申年 辛丑月 丁未日");
		});
	});
	block("object prop/function", obj => {
		obj = new func(dt1);

		can("toString = 丙申年(鸡) 腊月 廿三 大寒", () => {
			equal(obj.toString(), "丙申年(鸡) 腊月 廿三 大寒");
			equal(obj.toString("CY年 CM月 CD日"), "丙申年 辛丑月 丁未日");
		});
		can("setTime = 丁酉年(鸡) 正月 初七 立春", () => {
			obj.setTime(dt2);
			equal(obj.toString(), "丁酉年(鸡) 正月 初七 立春");
		});
		can("getTime = " + dt2.getTime(), () => {
			sequal(obj.getTime(), dt2.getTime());
		});
	});
}

function TGuid (func) {
	let obj = new func();

	block("class prop/function", () => {
		can("toString", () => {
			let d = func.toString();
			equal(d.split("-").length, 5);
		});
		can("toByteArray", () => {
			let d = func.toByteArray();
			equal(d.length, 16);
		});
		can("New", () => {
			let d = func.New();
			equal(d.toByteArray().length, 16);
		});
	});
	block("object prop/function", () => {
		can("toString", () => {
			equal(obj.toString(), "00000000-0000-0000-0000-000000000000");
		});
		can("toByteArray", () => {
			dequal(obj.toByteArray(), [0, 0, 0, 0, 0]);
		});
		can("valueOf", () => {
			equal(obj.valueOf(), "00000000-0000-0000-0000-000000000000");
		});
		can("toString", () => {
			obj.New();
			equal(obj.toByteArray().length, 16);
		});
	});
}

function NewGuid (func) {
	can("toByteArray", () => {
		let d = func();
		equal(d.toByteArray().length, 16);
	});
}

function EmptyGuid (func) {
	can("toByteArray", () => {
		let d = func();
		equal(d.toByteArray().length, 5);
	});
}

function ChinaDate (func) {
	let dt1 = new Date(2017, 0, 20);

	can("toString", () => {
		equal(func(dt1).toString(), "丙申年(鸡) 腊月 廿三 大寒");
		equal(func(dt1).toString("CY年 CM月 CD日"), "丙申年 辛丑月 丁未日");
	});
}

