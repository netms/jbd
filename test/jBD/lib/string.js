let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../../jBD/core.js");
global.jBD.MODE = {Node: 0};
global.jBD.mode = 0;
target = require("../../../jBD/node_cmd.js");
target.__require("./lib/regexp.js", "RegExp");
global.jBD.RegExp = target.RegExp;
target.__require("./lib/string.js", "String");
target = target.String;

factory("Mid", MID, target.Mid);
factory("Left", LEFT, target.Left);
factory("Right", RIGHT, target.Right);
factory("Trim", TRIM, target.Trim);
factory("LTrim", LTRIM, target.LTrim);
factory("RTrim", RTRIM, target.RTrim);
factory("Format", FORMAT, target.Format);
factory("Filter", FILTER, target.Filter);
factory("Length", LENGTH, target.Length);
factory("IndexOf", INDEXOF, target.IndexOf);
factory("SubStr", SUBSTR, target.SubStr);
factory("SubFix", SUBFIX, target.SubFix);
factory("Random", RANDOM, target.Random);

function MID (func) {
	let value = "0123456789";
	can("start=2,len=2", () => {
		dequal(func(value, {start: 2, len: 2}), "23");
	});
	can("start=2,len=-1", () => {
		dequal(func(value, 2), "23456789");
	});
	can("start=-2,len=2", () => {
		dequal(func(value, {start: -2, len: 1}), "8");
	});
	can("start=-2,len=-1", () => {
		dequal(func(value, -2), "89");
	});
}

function LEFT (func) {
	let value = "0123456789";
	can("len=2", () => {
		dequal(func(value, 2), "01");
	});
	can("len=-1", () => {
		dequal(func(value), "0123456789");
	});
}

function RIGHT (func) {
	let value = "0123456789";
	can("len=2", () => {
		dequal(func(value, 2), "89");
	});
	can("len=-1", () => {
		dequal(func(value), "0123456789");
	});
}

function TRIM (func) {
	let value = " 12345678912";
	can("mode=2", () => {
		dequal(func(value, 0), "12345678912");
	});
	can("{char:12,mode:0}", () => {
		dequal(func(value, {char: "12", mode: 0}), " 123456789");
	});
}

function LTRIM (func) {
	let value = "12345678912 ";
	can("char=null", () => {
		dequal(func(" " + value), "12345678912 ");
	});
	can("char=12", () => {
		dequal(func(value, "12"), "345678912 ");
	});
}

function RTRIM (func) {
	let value = " 12345678912";
	can("char=null", () => {
		dequal(func(value + " ", 0), " 12345678912");
	});
	can("char=12", () => {
		dequal(func(value, "12"), " 123456789");
	});
}

function FORMAT (func) {
	let value = "key:{0}  value:{value}";

	can("[1,2]", () => {
		dequal(func(value, 1, 2), "key:1  value:{value}");
	});
	can("[1,{value:2}]", () => {
		dequal(func(value, 1, {value: 2}), "key:1  value:2");
	});
	can("[1,{value:2}]", () => {
		dequal(func(value, [1, {value: 2}]), "key:1  value:2");
	});
}

function FILTER (func) {

}

function LENGTH (func) {
	let value = "123上海";

	can("chs=false", () => {
		dequal(func(value, false), 5);
	});
	can("chs=true", () => {
		dequal(func(value), 7);
	});
}

function INDEXOF (func) {
	let value = "123上海";

	can("sub=23", () => {
		dequal(func(value, "23"), 1);
	});
	can("start=2,len=4", () => {
		dequal(func(value, {start: 2, len: 4}), 3);
	});
}

function SUBSTR (func) {
	let value = "123上海";

	can("start=2,len=3", () => {
		dequal(func(value, {start: 2, len: 3}), "3上");
	});
}

function SUBFIX (func) {
	let value = "上23上海";

	can("len=4,chs=false", () => {
		dequal(func(value, {len: 5, chs: true}), "上...");
	});
}

function RANDOM (func) {
	can("len=6", () => {
		dequal(func(6).length, 6);
	});
}