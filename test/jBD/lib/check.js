let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../../jBD/core.js");
global.jBD.MODE = {Node: 0};
global.jBD.mode = 0;
target = require("../../../jBD/node_cmd.js");
target.__require("./lib/check.js", "Check");
target = target.Check;

factory("Input", INPUT, target.Input);
factory("Info", INFO, target.Info);
factory("UID", UID, target.UID);
factory("PWD", PWD, target.PWD);
factory("Mail", MAIL, target.Mail);
factory("IP", IP, target.IP);
factory("Name", NAME, target.Name);
factory("CID", CID, target.CID);

function INPUT (func) {
	can("arg=null", () => {
		dequal(func("1234 5678"), false);
	});
	can("min=2,max=5", () => {
		dequal(func("1", 2, 5), false);
		dequal(func("1234567", 2, 5), false);
		dequal(func("12345", 2, 5), true);
	});
}
function INFO (func) {
	can("arg=null", () => {
		dequal(func("1234 5678"), true);
	});
	can("min=2,max=5", () => {
		dequal(func("1", 2, 5), false);
		dequal(func("1234567", 2, 5), false);
	});
}
function UID (func) {
	can("arg=null", () => {
		dequal(func("a12345"), true);
		dequal(func("a1234"), false);
		dequal(func("a12345678912345678901234567890123"), false);
		dequal(func("123456"), false);
		dequal(func("a_1234"), true);
		dequal(func("a_123 4"), false);
	});
	can("min=4,max=8", () => {
		dequal(func("a12", 4, 8), false);
		dequal(func("a12345678", 4, 8), false);
	});
}
function PWD (func) {
	can("arg=null", () => {
		dequal(func("a12345"), true);
		dequal(func("a1234"), false);
		dequal(func("a12345678912345678901234567890123"), false);
		dequal(func("123456"), true);
		dequal(func("a1!@#$%^&*-_=+~"), true);
		dequal(func("();'/"), false);
		dequal(func("a_123 4"), false);
	});
	can("min=4,max=8", () => {
		dequal(func("a12", 4, 8), false);
		dequal(func("a12345678", 4, 8), false);
	});
}
function MAIL (func) {
	can("arg=null", () => {
		dequal(func("1@1.com"), true);
		dequal(func("1-a_c.a.a-1@a_1-2.c.1.com"), true);
		dequal(func("1-a_c._.a-1@a_1-2.c.1.com"), false);
	});
}
function IP (func) {
	can("arg=null", () => {
		dequal(func("0.0.0.0"), true);
		dequal(func("1.1.1.1"), true);
		dequal(func("255.255.255.255"), true);
		dequal(func("256.0.0.0"), false);
		dequal(func("0.0.0.0.0"), false);
	});
}
function NAME (func) {
	can("arg=null", () => {
		dequal(func("a103"), true);
		dequal(func("三103"), true);
		dequal(func("1103"), false);
	});
	can("min=3,max=5,all=false", () => {
		dequal(func("上123", 3, 5, false), true);
		dequal(func("a1", 3, 5, false), false);
		dequal(func("a12345", 3, 5, false), false);
		dequal(func("a1234", 3, 5, false), false);
	});
	can("min=3,max=5,all=true", () => {
		dequal(func("a1234", 3, 5, true), true);
	});
}
function CID (func) {
	can("arg=null", () => {
		dequal(func("632123198209270518"), true);
		dequal(func("632123820927051"), true);
	});
}