let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../../jBD/core.js");
global.jBD.MODE = {Node: 0};
global.jBD.mode = 0;
target = require("../../../jBD/node_cmd.js");
target.__require("./lib/regexp.js", "RegExp");
target = target.RegExp;

factory("Filter", Filter, target.Filter);
// factory("TObject", TObject, target.TObject);
// factory("TList", TList, target.TList);
// factory("TChinaDate", TChinaDate, target.TChinaDate);
// factory("TGuid", TGuid, target.TGuid);
// factory("ChinaDate", ChinaDate, target.ChinaDate);
// factory("NewGuid", NewGuid, target.NewGuid);
// factory("EmptyGuid", EmptyGuid, target.EmptyGuid);

function Filter (func) {
	can("value=string", () => {
		equal(func("1(2)[3]", false), "1|\\(|2|\\)|\\[|3|\\]");
	});
	can("value=array", () => {
		equal(func(["1", "(", "2", ")", "[", "3", "]"], false), "1|\\(|2|\\)|\\[|3|\\]");
	});
}