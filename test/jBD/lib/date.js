let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../../jBD/core.js");
global.jBD.MODE = {Node: 0};
global.jBD.mode = 0;
target = require("../../../jBD/node_cmd.js");
target.__require("./lib/date.js", "Date");
target = target.Date;

factory("Basic", Basic, target);
factory("Inc", Inc, target);
factory("Dec", Dec, target);
// factory("TChinaDate", TChinaDate, target.TChinaDate);
// factory("TGuid", TGuid, target.TGuid);
// factory("ChinaDate", ChinaDate, target.ChinaDate);
// factory("NewGuid", NewGuid, target.NewGuid);
// factory("EmptyGuid", EmptyGuid, target.EmptyGuid);

function Basic (func) {
	let value = new Date(2012, 2, 4, 5, 6, 7, 890);

	can("DateOf", () => {
		dequal(func.DateOf(value).getTime(), new Date(2012, 2, 4).getTime());
	});
	can("TimeOf", () => {
		dequal(func.TimeOf(value).getTime(), new Date(0, 0, 0, 5, 6, 7, 890).getTime());
	});
	block("ValueOf", () => {
		can("{surplus:false}", () => {
			dequal(func.ValueOf(value), {value: 42, unit: "year"})
		});
		can("opt=year", () => {
			dequal(func.ValueOf(value, "year"), 42)
		});
		can("{surplus:true,fmt:year}", () => {
			dequal(func.ValueOf(value, {surplus: true, fmt: "year"}), 0)
		});
	});
	can("Yesterday", () => {
		dequal(func.Yesterday(value).getTime(), new Date(2012, 2, 3).getTime());
	});
	can("Tomorrow", () => {
		dequal(func.Tomorrow(value).getTime(), new Date(2012, 2, 5).getTime());
	});
	can("IsLeapYear", () => {
		dequal(func.IsLeapYear(value), true);
	});
	can("MonthOfDay", () => {
		dequal(func.MonthOfDay(value), 30);
	});
	can("YearOfDay", () => {
		dequal(func.YearOfDay(value), 366);
	});
	can("DayOfMonth", () => {
		dequal(func.DayOfMonth(value), 4);
	});
	can("DayOfYear", () => {
		dequal(func.DayOfYear(value), 64);
	});
	can("WeekOfMonth", () => {
		dequal(func.WeekOfMonth(value), 0);
	});
	can("WeekOfYear", () => {
		dequal(func.WeekOfYear(value), 9);
	});
}

function Inc (func) {
	let value = new Date(2012, 2, 4, 5, 6, 7, 890);

	block("Inc", () => {
		can("value=1", () => {
			dequal(func.Inc(1, value), new Date(2012, 2, 5, 5, 6, 7, 890));
		});
		can("value=1,type=ms", () => {
			dequal(func.Inc(1, value, "ms"), new Date(2012, 2, 4, 5, 6, 7, 891));
		});
		can("opt={type=ms}", () => {
			dequal(func.Inc(100, {date: value, type: "ms"}), new Date(2012, 2, 4, 5, 6, 7, 990));
		});
	});
	can("IncMillisecond", () => {
		dequal(func.IncMillisecond(100, value), new Date(2012, 2, 4, 5, 6, 7, 990));
	});
	can("IncSecond", () => {
		dequal(func.IncSecond(2, value), new Date(2012, 2, 4, 5, 6, 9, 890));
	});
	can("IncMinute", () => {
		dequal(func.IncMinute(2, value), new Date(2012, 2, 4, 5, 8, 7, 890));
	});
	can("IncHour", () => {
		dequal(func.IncHour(2, value), new Date(2012, 2, 4, 7, 6, 7, 890));
	});
	can("IncDay", () => {
		dequal(func.IncDay(2, value), new Date(2012, 2, 6, 5, 6, 7, 890));
	});
	can("IncWeek", () => {
		dequal(func.IncWeek(2, value), new Date(2012, 2, 18, 5, 6, 7, 890));
	});
	can("IncMonth", () => {
		dequal(func.IncMonth(2, value), new Date(2012, 4, 4, 5, 6, 7, 890));
	});
	can("IncYear", () => {
		dequal(func.IncYear(2, value), new Date(2014, 2, 4, 5, 6, 7, 890));
	});
}

function Dec (func) {
	let v1 = new Date(2012, 2, 4, 5, 6, 7, 390),
		v2 = new Date(2012, 2, 4, 6, 7, 8, 890);

	block("Dec", () => {
		can("type=null", () => {
			dequal(func.Dec(v1, v2), -3661500);
		});
		can("type=d", () => {
			dequal(func.Dec(v1, v2, "d"), 0);
		});
	});
	can("DecMillisecond", () => {
		dequal(func.DecMillisecond(v1, v2), -3661500);
	});
	can("DecSecond", () => {
		dequal(func.DecSecond(v1, v2), -3661);
	});
	can("DecMinute", () => {
		dequal(func.DecMinute(v1, v2), -61);
	});
	can("DecHour", () => {
		dequal(func.DecHour(v1, v2), -1);
	});
	block("DecDay", () => {
		let v1 = new Date(2017, 6, 23, 0, 0, 0, 0),
			v2 = new Date(2017, 7, 23, 0, 0, 0, 0);

		can("ms=false", () => {
			dequal(func.DecDay(v1, v2), -31);
		});
		can("ms=true", () => {
			dequal(func.DecDay(v1, v2, true), -31);
		});
	});
	block("DecWeek", () => {
		let v1 = new Date(2017, 6, 4),
			v2 = new Date(2017, 6, 23);

		can("day=false", () => {
			dequal(func.DecWeek(v1, v2), -2);
		});
		can("day=true", () => {
			dequal(func.DecWeek(v1, v2, true), -3);
		});
	});
	block("DecMonth", () => {
		let v1 = new Date(2016, 7, 31),
			v2 = new Date(2017, 6, 1);

		can("day=false", () => {
			dequal(func.DecMonth(v1, v2), -11);
		});
		can("day=true", () => {
			dequal(func.DecMonth(v1, v2, true), -10);
		});
	});
	block("DecYear", () => {
		let v1 = new Date(2018, 6, 4),
			v2 = new Date(2017, 6, 23);

		can("day=false", () => {
			dequal(func.DecYear(v1, v2), 1);
		});
		can("day=true", () => {
			dequal(func.DecYear(v1, v2, true), 0);
		});
	});
}