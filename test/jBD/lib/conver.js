let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../../jBD/core.js");
global.jBD.MODE = {Node: 0};
global.jBD.mode = 0;
target = require("../../../jBD/node_cmd.js");
target.__require("./lib/regexp.js", "RegExp");
global.jBD.RegExp = target.RegExp;
target.__require("./lib/string.js", "String");
global.jBD.String = target.String;
global.jBD.TChinaDate = require("../../../jBD/class.js").TChinaDate;
target.__require("./lib/conver.js", "Conver");
target = target.Conver;

factory("toBool", TOBOOL, target.toBool);
factory("toInteger", TOINTEGER, target.toInteger);
factory("toFloat", TOFLOAT, target.toFloat);
factory("toString", TOSTRING, target.toString);
factory("toDate", TODATE, target.toDate);
factory("toJSON", TOJSON, target.toJSON);
factory("toHtml", TOHTML, target.toHtml);
factory("toMoney", TOMONEY, target.toMoney);
factory("toUrl", TOURL, target.toUrl);
factory("toUTF8", TOUTF8, target.toUTF8);
factory("toHex", TOHEX, target.toHex);

function TOBOOL (func) {
	can("basic", () => {
		dequal(func(true), true);
		dequal(func(1), true);
		dequal(func(0), false);
		dequal(func(-2), true);
		dequal(func("true"), true);
		dequal(func("12"), false);
		dequal(func(undefined), false);
	});
	can("object", () => {
		dequal(func([]), false);
		dequal(func([1]), true);
		dequal(func({}), false);
		dequal(func({a: 1}), true);
	});
}

function TOINTEGER (func) {
	can("arg=null", () => {
		dequal(func("123"), 123);
		dequal(func(12.34), 12);
		dequal(func("12.34"), 12);
		dequal(func("12 34"), 0);
	});
	can("def=9", () => {
		dequal(func("12 34", 9), 9);
	});
	can("callback", () => {
		dequal(func("12 34", 9, (val, old, def) => {
			if (val == def) return 8;
		}), 8);
		dequal(func(12, (val, old, def) => {
			if (val == 12 && old == 12 && def == 0) return 8;
		}), 8);
	});
}

function TOFLOAT (func) {
	can("arg=null", () => {
		dequal(func("123"), 123.0);
		dequal(func(12.34), 12.3);
		dequal(func("12.34"), 12.3);
		dequal(func("12 34"), 0);
	});
	can("len=2", () => {
		dequal(func("12.37", 2), 12.37);
		dequal(func(12.377, 2), 12.38);
	});
	can("callback", () => {
		dequal(func("12 34", 2, (val, old, def) => {
			if (val == def) return 8;
		}), 8);
		dequal(func(12.35, 1, (val, old, def) => {
			if (val == 12.4) return 2.3;
		}), 2.3);
	});
}

function TOSTRING (func) {

	can("boolean", () => {
		dequal(func(true), "true");
		dequal(func(false), "false");
	});
	block("number", () => {
		can("arg=null", () => {
			dequal(func(12), "12");
			dequal(func(12.34), "12");
		});
		can("len=2,def=1.2", () => {
			dequal(func(12, {len: 2, def: 1.2}), "12");
			dequal(func(12.345, {len: 2, def: 1.2}), "12.35");
		});
		can("chs=true", () => {
			dequal(func(1012, {chs: true}), "一千零十二");
			dequal(func(1012.34, {len: 2, chs: true}), "一千零十二点三四")
		})
	});
	block("string", () => {
		can("arg=null", () => {
			dequal(func("123"), "123");
			dequal(func("<123'\""), "<123'\"");
		});
		can("html=true", () => {
			dequal(func("<123'\"", true), "&lt;123&apos;&quot;");
		});
	});
	can("null", () => {
		dequal(func(undefined), "null");
		dequal(func(null), "null");
	});
	can("date", () => {
		let now = new Date(2011, 1, 3, 4, 5, 6, 7);

		dequal(func(now), "2011-2-3 04:05:06");
		dequal(func(now, "yy-mm-dd h:n:s.zz"), "11-02-03 4:5:6.07");
		dequal(func(now, "CY年 CM月 M月D T Z"), "辛卯年 庚寅月 正月初一  兔");
	});
	can("array", () => {
		let data = [1.23, 2.2667, "3"],
			opt  = {0: {len: 1}, 1: {len: 2}};

		dequal(func(data, opt), "[1.2,2.27,\"3\"]");
		dequal(func(data, 4), "[\n    1,\n    2,\n    \"3\"\n]");
		dequal(func([20, 192, 168, 0, 200, 30], {buf: true, offset: 1, count: 4}), "C0 A8 00 C8");
	});
	can("object", () => {
		let data, opt;

		data = require("./data.json");
		data = func(data);
		data = target.toJSON(data);



		// data = {a: -1.139646, b: "2"};
		opt = {len: 5};

		console.log(func(data, 4));

		// dequal(func(data), "{\"a\":-1,\"b\":\"2\"}");
		// dequal(func(data, opt), "{\"a\":-1.13965,\"b\":\"2\"}");
		// dequal(func(data, 4), "{\n    \"a\": -1,\n    \"b\": \"2\"\n}");
	});
	can("uint8", () => {
		dequal(func(Buffer.from([20, 192, 168, 0, 200, 30]), {offset: 1, count: 4}), "C0 A8 00 C8");
	})
}

function TODATE (func) {
	let now = new Date(2011, 1, 3, 4, 5, 6, 7);

	can("date", () => {
		dequal(target.toString(func(now)), "2011-2-3 04:05:06");
	});
	can("number", () => {
		dequal(target.toString(func(now.getTime())), "2011-2-3 04:05:06");
	});
	can("string", () => {
		dequal(target.toString(func("2011-02-03T04:05:06.007Z")), "2011-2-3 04:05:06");
		dequal(target.toString(func("2011-2-3 04:05:06")), "2011-2-3 04:05:06");
	});
}

function TOJSON (func) {
	can("arg=null", () => {
		// dequal(jBD.is(func("{\"a\":1,\"b\":\"2\",c:false}"), {a: 1, b: "2", c: false}, false), true);
		let data = "{\"mode\":\"debug\",\"key\":\"eyemove\",\"sys\":{\"host\":\"0.0.0.0\",\"port\":8080,\"max\":10,\"timeout\":60,\"start\":true,\"page\":\"/\",\"api\":\"/api\",\"upload\":\"/upload\"},\"module\":{\"db\":{\"mode\":\"mongo\",\"host\":\"127.0.0.1\",\"port\":27017,\"max\":1,\"uid\":\"\",\"pwd\":\"\",\"name\":\"EYEMOVE\"},\"file\":{\"db\":\"File\",\"timeout\":15},\"act\":{\"system\":{\"db\":\"Account_System\",\"timeout\":45,\"key\":\"eyemove\",\"name\":\"临时管理员\",\"token\":\"account\",\"mode\":{\"account\":\"account\",\"password\":\"password\"},\"account\":[4,4]},\"person\":{\"db\":\"Account_Person\",\"timeout\":45,\"key\":\"eyemove\",\"name\":\"临时用户\",\"token\":\"account\",\"mode\":{\"account\":\"account\",\"password\":\"password\"},\"account\":[11,20]},\"manage\":{\"db\":\"Account_Manage\",\"timeout\":45,\"key\":\"eyemove\",\"name\":\"临时管理\",\"token\":\"account\",\"mode\":{\"account\":\"account\",\"password\":\"password\"},\"account\":[4,4]}},\"person\":{\"db\":\"Person\"}},\"page\":{\"home\":true,\"side\":true,\"search\":true,\"vacate\":true,\"notice\":true,\"motif\":true,\"message_normal\":true,\"message_system\":true,\"message_vacate\":true,\"model\":true,\"model_search\":true,\"model_history\":true},\"api\":{\"Motif\":true,\"Message\":true,\"Vacate\":true,\"Notice\":true}}";

		data = func(data);

		// console.log(target.toString(data, 4));

		// console.log(data);


	});
}

function TOHTML (func) {
	can("arg=null", () => {
		dequal(func("\t<1 tag=\"123\">"), "&nbsp;&nbsp;&lt;1&nbsp;tag=&quot;123&quot;&gt;");
	});
	can("tab=false", () => {
		dequal(func("\t<1 tag=\"123\">", false), "　　&lt;1　tag=&quot;123&quot;&gt;");
	});
	can("rev=true", () => {
		dequal(func("&nbsp;&nbsp;&lt;1&nbsp;tag=&quot;123&quot;&gt;", true, true), "\t<1 tag=\"123\">");
		dequal(func("&nbsp;&nbsp;&lt;1&nbsp;tag=&quot;123&quot;&gt;", false, true), "  <1 tag=\"123\">");
	});
}

function TOMONEY (func) {
	can("整数", () => {
		dequal(func(1034), "壹千零叁拾肆元整");
	});
	can("小数", () => {
		dequal(func(1014.32), "壹千零拾肆元叁角贰分");
		dequal(func(1014.30), "壹千零拾肆元叁角");
		dequal(func(1014.02), "壹千零拾肆元贰分");
	});
}

function TOURL (func) {
	can("url", () => {
		dequal(func("www.baidu.com//1/c/./../a.index"), "http://www.baidu.com/1/a.index");
		dequal(func("tcp://aa:81//1/c/./../a.index"), "tcp://aa:81/1/a.index");
		dequal(func("tcp://aa:80//1/c/./../a.index"), "tcp://aa/1/a.index");
	});
}

function TOUTF8 (func) {

}

function TOHEX (func) {
	can("8bit", () => {
		dequal(func("123", {str: true, encoding: 8}).join(" "), "31 32 33");
	});
	// can("16bit", () => {
	// 	dequal(func("123", {str: true, encoding: 16}).join(" "), "00 31 00 32 00 33");
	// });
	can("32bit", () => {
		dequal(func("123", {str: true, encoding: 32}).join(" "), "00 00 00 31 00 00 00 32 00 00 00 33");
	});
}