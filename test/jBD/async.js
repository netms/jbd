let assert  = require("assert"),
	block   = describe,
	can     = it,
	target,
	fail    = assert.fail,
	equal   = assert.equal,
	nequal  = assert.notEqual,
	dequal  = assert.deepEqual,
	dnequal = assert.notDeepEqual,
	sequal  = assert.strictEqual,
	snequal = assert.notStrictEqual,
	factory = (tag, work, func, arg) => {
		if (!arg) arg = [];
		arg.unshift(func);
		block(tag, () => { work.apply(null, arg); })
	};

global.jBD = require("../../jBD/core.js");
global.jBD.clone(require("../../jBD/class.js"), global.jBD);
target = require("../../jBD/async.js");

// factory("Callback", Callback, target.Callback);
factory("Deferred", Deferred, target.Deferred);

// factory("async", ASYNC, target.async);

function Callback (func) {

}

function Deferred1 (func) {
	var num = 0;

	function LoopFunc (time, cb) {
		setTimeout(() => {
			(async (state) => {
				try {
					state = await cb();
					if (state !== false) LoopFunc(time, cb);
				}
				catch (e) {
					console.log(e);
				}
			})();
		}, time);
	}

	async function AsyncFunc () {
		return func(true, (dtd) => {
			setTimeout(() => {
				// try {
				num++;
				console.log("new: ", num);

				if (num >= 10) dtd.resolve();
				else dtd.reject(0);
				// }
				// catch(e) {
				// 	throw false;
				// dtd.reject(false);
				// console.log("OKK");
				// }
			}, 500);
		});
	}

	LoopFunc(500, AsyncFunc);
}

function Deferred (func) {
	async function A (value) {
		let dtd = func(true);

		dtd.reject(value);

		return dtd.promise();
	}

	(async function () {
		try {
			(async () => {
				await A(0);
			})();
			await A(1);
		}
		catch (e) {
			console.log(e);
		}
	})();
}

function ASYNC (func) {
	let gen, done,
		sleep1 = function (l) {
			console.log("sleep begin");
			console.log("sleep l:", l);
			return new Promise(ok => {
				setTimeout(function () {
					console.log("sleep l=", l);
					ok(l);
				}, l * 1000);
			});
		},
		sleep2 = function* (l) {
			yield sleep1(l);
			return l;
		},
		work   = function* (d1, d2) {
			console.log("\targ:", arguments.length);
			console.log("\targ-a:", this.a);
			console.log("============");
			console.log(yield sleep1(d2++));
			console.log("============");
			console.log(yield* sleep2(d2++));
			console.log("\targ-d2:", d2++);

			return d2;
		};

	can("1", (ok) => {
		console.log(1);
		done = ok;
		func(work, [2, 5], {a: 3})
			.done(d => {
				console.log(d);
				ok();
			})
			.fail(err => { ok(err); });
	});

}